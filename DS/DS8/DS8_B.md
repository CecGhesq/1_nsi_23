# DS 8_B

## Exercice 1 :
Réaliser le QCM sur pronote

## Exercice 2 :
Le fichier [ds_html.html](ds_html_B.html) est à télécharger.  
1. A partir de celui-ci, créer trois fichiers différents séparant les codes html, css et le javascript.  
Les liens entre la page html et la feuille de style ainsi que celle de javascript seront à réaliser en utilisant les balises suivantes :   

```html
// dans le head pour la feuille de style
<link href="style.css" rel="stylesheet" type="text/css"/>

// sous les éléments du body devant interagir
<script src="script.js"></script> 
```
🖐 Mettre les trois fichiers dans un dossier, le zipper en un seul fichier (clic droit de la souris : Compresser) et le déposer sur Pronote

2. Reprendre le fichier script.js afin d'ajouter un événement : lors du survol du texte : "clic clic clic ...", ce texte doit est encadré en noir, d'une largeur de 2px, en trait continu (solid).
Rappels :  
* l'événement survol se code : `mouseover` ;  
* pour modifier la couleur d'une balise, la méthode js est `.style.border`
🖐 Déposer le nouveau fichier `script.js` sur pronote.
