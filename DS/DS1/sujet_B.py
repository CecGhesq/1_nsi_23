#import doctest

#########################################
#    Code à compléter                   #
#                                       #
# en remplaçant 'pass' par votre code   #
#########################################


#### sur 1 pt ##########

def nombre_lettres(mot):
    '''renvoie le nombre de caractères du mot
    Exemple :
    >>> nombre_lettres('montagne')
    8
    '''
    pass

#### sur 2 pt ##########
def indice_espace(chaine):
    '''renvoie l'indice de position de la première rencontre d'un espace dans une chaine de caractères
    Exemples :
    >>> indice_espace('un ballon')
    2
    >>> indice_espace('un ballon et un but')
    2
    '''
    pass

#### sur 2 pt ########## on utilisera la fonction indice_espace précédente
def mot_1(chaine):
    '''renvoie le premier mot de la chaine contenant deux mots séparés par un espace
    Exemples:
    >>> mot_1('courrier electronique')
    'courrier'
    '''
    pass

#### sur 2 pt ########## on utilisera la fonction indice_espace précédente
def mot_2(chaine):
    '''renvoie le deuxième mot de la chaine contenant deux mots séparés par un espace
    Exemples:
    >>> mot_2('produit logiciel')
    'logiciel'
    '''
    pass

#### sur 1,5 pt ##########
def debut_mot(mot, n):
    '''renvoie les n premières lettres du mot
    Exemples :
    >>> debut_mot('spaghetti',4)
    'spag'
    >>> debut_mot('produit', 3)
    'pro'
    >>> debut_mot('neige', 0)
    ''
    '''
    pass


#### sur 1,5 pt ##########
def fin_mot(mot,n):
    '''renvoie les n derniers caractères du mot
    Exemples :
    >>> fin_mot('logiciel',6)
    'giciel'
    '''
    pass

#### sur 3 pt ########## on complètera le code en utilisant certaines des fonctions précédentes    
def mot_valise(chaine, n1,n2):
    '''renvoie le mot valise constitué des n1 premiers caractères du premier mot et
    les n2 derniers caracteres du second mot
    Exemple :
    >>> mot_valise('produit logiciel', 3, 6)
    'progiciel'
    '''
    mot1 = mot_1(chaine)
    mot1_debut = #à completer
    mot2 = #à completer
    mot2_fin = fin_mot(mot2,n2)
    return #à completer


#doctest.testmod()

