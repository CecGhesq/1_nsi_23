#import doctest


#########################################
#    Code à compléter                   #
#                                       #
# en remplaçant 'pass' par votre code   #
#########################################


#### sur 1,5 pt ##########
def coupe_mot(mot, n):
    '''renvoie les n premières lettres du mot
    Exemples :
    >>> coupe_mot('autoroute',4)
    'auto'
    >>> coupe_mot('information', 5)
    'infor'
    >>> coupe_mot('montagne', 0)
    ''
    '''
    pass

#### sur 1,5 pt ##########
def fin_mot(mot,n):
    '''renvoie les n derniers caractères du mot
    Exemples :
    >>> fin_mot('automatique',7)
    'matique'
    '''
    pass

#### sur 1 pt ##########
def nombre_caracteres(mot):
    '''renvoie le nombre de caractères du mot
    Exemple :
    >>> nombre_caracteres('neige')
    5
    '''
    pass


#### sur 2 pt ##########
def indice_espace(chaine):
    '''renvoie l'indice de position de la première rencontre d'un espace dans une chaine de caractères
    Exemples :
    >>> indice_espace('un chien')
    2
    >>> indice_espace('un chien et un chat')
    2
    '''
    pass

#### sur 2 pt ########## on utilisera la fonction indice_espace précédente
def premier_mot(chaine):
    '''renvoie le premier mot de la chaine contenant deux mots séparés par un espace
    Exemples:
    >>> premier_mot('information automatique')
    'information'
    '''
    pass

#### sur 2 pt ########## on utilisera la fonction indice_espace précédente
def deuxieme_mot(chaine):
    '''renvoie le deuxième mot de la chaine contenant deux mots séparés par un espace
    Exemples:
    >>> deuxieme_mot('information automatique')
    'automatique'
    '''
    pass

#### sur 3 pt ########## on complètera le code en utilisant certaines des fonctions précédentes
def mot_valise(chaine, n1,n2):
    '''renvoie le mot valise constitué des n1 premiers caractères du premier mot et
    les n2 derniers caracteres du second mot
    Exemple :
    >>> mot_valise('information automatique', 5, 7)
    'informatique'
    '''
    mot1 = premier_mot(chaine)
    mot1_debut = #à completer
    mot2 = #à completer
    mot2_fin = fin_mot(mot2,n2)
    return #à compléter


#doctest.testmod()
