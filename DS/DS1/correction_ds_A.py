
import doctest

def coupe_mot(mot, n):
    '''renvoie les n premières lettres du mot
    Exemples :
    >>> coupe_mot('autoroute',4)
    'auto'
    >>> coupe_mot('information', 5)
    'infor'
    >>> coupe_mot('montagne', 0)
    ''
    '''
    return mot[:n]

def fin_mot(mot,n):
    '''renvoie les n derniers caractères du mot
    Exemples :
    >>> fin_mot('automatique',7)
    'matique'
    '''
    return mot[len(mot)-n:]

def nombre_caracteres(mot):
    '''renvoie le nombre de caractères du mot
    Exemple :
    >>> nombre_caracteres('neige')
    5
    '''
    return len(mot)

def indice_espace(chaine):
    '''renvoie l'indice de position de la première rencontre d'un espace dans une chaine de caractères
    Exemples :
    >>> indice_espace('un chien')
    2
    >>> indice_espace('un chien et un chat')
    2
    '''
    return chaine.find(' ')

def premier_mot(chaine):
    '''renvoie le premier mot de la chaine contenant deux mots séparés par un espace
    Exemples:
    >>> premier_mot('information automatique')
    'information'
    '''
    indice_esp = indice_espace(chaine)
    return chaine[:indice_esp]

def deuxieme_mot(chaine):
    '''renvoie le deuxième mot de la chaine contenant deux mots séparés par un espace
    Exemples:
    >>> deuxieme_mot('information automatique')
    'automatique'
    '''
    indice_esp = indice_espace(chaine)
    return chaine[indice_esp+1:]
    
def mot_valise(chaine, n1,n2):
    '''renvoie le mot valise constitué des n1 premiers caractères du premier mot et
    les n2 derniers caracteres du second mot
    Exemple :
    >>> mot_valise('information automatique', 5, 7)
    'informatique'
    '''
    mot1 = premier_mot(chaine)
    mot1_debut = coupe_mot(mot1, n1)
    mot2 = deuxieme_mot(chaine)
    mot2_fin = fin_mot(mot2,n2)
    return mot1_debut + mot2_fin


doctest.testmod()