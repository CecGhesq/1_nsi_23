
import doctest

def nombre_lettres(mot):
    '''renvoie le nombre de caractères du mot
    Exemple :
    >>> nombre_lettres('montagne')
    8
    '''
    return len(mot)

def indice_espace(chaine):
    '''renvoie l'indice de position de la première rencontre d'un espace dans une chaine de caractères
    Exemples :
    >>> indice_espace('un ballon')
    2
    >>> indice_espace('un ballon et un but')
    2
    '''
    return chaine.find(' ')

def mot_1(chaine):
    '''renvoie le premier mot de la chaine contenant deux mots séparés par un espace
    Exemples:
    >>> mot_1('courrier electronique')
    'courrier'
    '''
    indice_esp = indice_espace(chaine)
    return chaine[:indice_esp]

def mot_2(chaine):
    '''renvoie le deuxième mot de la chaine contenant deux mots séparés par un espace
    Exemples:
    >>> mot_2('produit logiciel')
    'logiciel'
    '''
    indice_esp = indice_espace(chaine)
    return chaine[indice_esp+1:]

def debut_mot(mot, n):
    '''renvoie les n premières lettres du mot
    Exemples :
    >>> debut_mot('spaghetti',4)
    'spag'
    >>> debut_mot('produit', 3)
    'pro'
    >>> debut_mot('neige', 0)
    ''
    '''
    return mot[:n]

def fin_mot(mot,n):
    '''renvoie les n derniers caractères du mot
    Exemples :
    >>> fin_mot('logiciel',6)
    'giciel'
    '''
    return mot[len(mot)-n:]


    
def mot_valise(chaine, n1,n2):
    '''renvoie le mot valise constitué des n1 premiers caractères du premier mot et
    les n2 derniers caracteres du second mot
    Exemple :
    >>> mot_valise('produit logiciel', 3, 6)
    'progiciel'
    '''
    mot1 = mot_1(chaine)
    mot1_debut = debut_mot(mot1, n1)
    mot2 = mot_2(chaine)
    mot2_fin = fin_mot(mot2,n2)
    return mot1_debut + mot2_fin


doctest.testmod()
