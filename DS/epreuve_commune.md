# Epreuve commune
* __EXERCICE 1 (5 points)__

Écrire en langage Python une fonction recherche prenant comme paramètres une variable _a_ de type numérique (float ou int) et un tableau non vide _tab_ (de type list) et qui renvoie le nombre d'occurrences de _a_ dans _tab_.

On y incluera une docstring précise et une assertion judicieusement choisie.

Exemples d'utilisations de la fonction recherche:

```python
>>> recherche(5, [-2, 3, 4, 8])
0
>>> recherche(5, [-2, 3, 1, 5, 3, 7, 4])
1
>>> recherche(5, [-2, 5, 3, 5, 4, 5])
3 
```

* __EXERCICE 2 (5 points)__

Les résultats d'un vote ayant trois issues possibles 'A', 'B' et 'C' sont stockés dans un tableau.  
Exemple :

```python
urne = ['A', 'A', 'A', 'B', 'C', 'B', 'C', 'B', 'C', 'B']
```

La fonction `depouille` doit permettre de compter le nombre de votes exprimés pour chacune des issues. Elle prend en paramètre un tableau et renvoie le résultat dans un dictionnaire dont les clés sont les noms des issues et les valeurs le nombre de votes en leur faveur.  

La fonction `vainqueur` doit désigner le nom du ou des gagnants.
Elle prend en paramètre un dictionnaire dont la structure est celle du dictionnaire renvoyé par la fonction depouille et renvoie un tableau.
Ce tableau peut donc contenir plusieurs éléments s’il y a des ex-aequo. 


Compléter les fonctions `depouille` et `vainqueur` fournies à la page suivante pour qu’elles renvoient les résultats attendus. 

```python
def depouille(urne):
	resultat = …
	for bulletin in urne:
		if ...:
<<<<<<< HEAD
			resultat[bulletin] = resultat[bulletin] + 1			
        else:
			…
=======
			resultat[bulletin] = resultat[bulletin] + 1	
        else:
			.......
>>>>>>> e629be9b164a173ecee754e7dd807224f3fcfe4a
	return resultat

def vainqueur(election):
	nmax = 0
	for candidat in election:
		if ... > ... :
			nmax = …
	liste_finale = [nom for nom in election if election[nom] == …]
	return …
```

Exemples d’utilisation :

```python
>>> election = depouille(urne)
>>> election
{'A': 3, 'B': 4, 'C': 3}
>>> vainqueur(election)
['B'] 
```

