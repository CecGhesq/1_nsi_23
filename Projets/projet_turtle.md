<center> <h1>Projet Turtle </h1></center>

# Projet
Le graphique proposé plus loin a été produit par Cyril D., élève de seconde (voir le script) dans le cadre d’un devoir maison de mathématiques intitulé “Les mathématiques sont belles ! “.

Votre travail consiste à exploiter le module turtle de python, vous devez :

* vous approprier le code python de Cyril D,
* modifier ce code jusqu’à obtenir un résultat que vous jugerez satisfaisant.

Ce travail ne finit en fait jamais sauf quand vous décidez que vous l’avez fini. Le rendu doit être flatteur, c'est-à-dire que vous devez le trouver beau.

Vous pouvez juste modifier le code, ou y ajouter des éléments, ou le réécrire mais ce n’est pas obligatoire. Il devrait toujours y avoir __au minimum une boucle__.

Votre code doit respecter les trois contraintes suivantes :

* 16 lignes au maximum
* 64 caractères par ligne, espace inclus
* 1024 caractères au total donc car 16 x 64 = 1024

Arriver à 16 lignes et 1024 caractères n’est en aucun cas un objectif.

# Des exemples :
![](./img/im1.jpg)
![](./img/im2.jpg)
![](./img/im3.jpg)
![](./img/im4.jpg)

# Obtenir une image propre

Le travail est à réaliser sur le workshop de la NumWorks.

On se connecte au workshop avec un compte NumWorks, il est gratuit que vous ayez ou pas une NumWorks. ATTENTION le navigateur conseillé est __Chrome__ pour relier sa calculatrice. Lorsque votre compte est créé, sous le badge de votre nom, vous trouverez la rubrique 'Mes scripts' dans laquelle vous pourrez écrire votre code, le tester, l'envoyer vers votre calculatrice.  

![](./img/numworks.jpg)

Le script modèle est [ici](https://my.numworks.com/python/cent20/openbadge)

En cliquant sur ▶ l’image se construit et on peut récupérer une capture d’écran propre en faisant :  clic droit > Enregistrer l’image sous

__ATTENTION, les photos, captures totales de l’écran sont interdites.__

Si vous respectez les consignes, vous obtiendrez une image de 320 x 240 pixels au format .png, elle ne contient aucune bordure. 

![](./img/im5.jpg)

```python
import turtle
list1 = ["red", "orange", "yellow", "green", "black"]
for i in range(125) :
 turtle.color(list1[i%5])
 turtle.pensize(10+2)
 turtle.forward(i)
 turtle.left(59)
 ```



# Déposer votre script et votre photo sur Pronote.


![](./img/tortue008.gif) 
=======
Déposer votre script et votre photo sur Pronote.

