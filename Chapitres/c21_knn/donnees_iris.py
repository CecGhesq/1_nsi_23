donnees_iris = [{'longueur_sepale': 5.1, 'largeur_sepale': 3.5, 'longueur_petale': 1.4, 'largeur_petale': 0.2, 'Nom': 'Setosa'},
                {'longueur_sepale': 4.9, 'largeur_sepale': 3.0, 'longueur_petale': 1.4, 'largeur_petale': 0.2, 'Nom': 'Setosa'},
                {'longueur_sepale': 4.7, 'largeur_sepale': 3.2, 'longueur_petale': 1.3, 'largeur_petale': 0.2, 'Nom': 'Setosa'},
                {'longueur_sepale': 4.6, 'largeur_sepale': 3.1, 'longueur_petale': 1.5, 'largeur_petale': 0.2, 'Nom': 'Setosa'},
                {'longueur_sepale': 5.0, 'largeur_sepale': 3.6, 'longueur_petale': 1.4, 'largeur_petale': 0.2, 'Nom': 'Setosa'},
                {'longueur_sepale': 5.4, 'largeur_sepale': 3.9, 'longueur_petale': 1.7, 'largeur_petale': 0.4, 'Nom': 'Setosa'},
                {'longueur_sepale': 4.6, 'largeur_sepale': 3.4, 'longueur_petale': 1.4, 'largeur_petale': 0.3, 'Nom': 'Setosa'},
                {'longueur_sepale': 7.0, 'largeur_sepale': 3.2, 'longueur_petale': 4.7, 'largeur_petale': 1.4, 'Nom': 'Versicolor'},
                {'longueur_sepale': 6.4, 'largeur_sepale': 3.2, 'longueur_petale': 4.5, 'largeur_petale': 1.5, 'Nom': 'Versicolor'},
                {'longueur_sepale': 6.9, 'largeur_sepale': 3.1, 'longueur_petale': 4.9, 'largeur_petale': 1.5, 'Nom': 'Versicolor'},
                {'longueur_sepale': 5.5, 'largeur_sepale': 2.3, 'longueur_petale': 4.0, 'largeur_petale': 1.3, 'Nom': 'Versicolor'},
                {'longueur_sepale': 6.5, 'largeur_sepale': 2.8, 'longueur_petale': 4.6, 'largeur_petale': 1.5, 'Nom': 'Versicolor'},
                {'longueur_sepale': 5.7, 'largeur_sepale': 2.8, 'longueur_petale': 4.5, 'largeur_petale': 1.3, 'Nom': 'Versicolor'},
                {'longueur_sepale': 6.3, 'largeur_sepale': 3.3, 'longueur_petale': 4.7, 'largeur_petale': 1.6, 'Nom': 'Versicolor'},
                {'longueur_sepale': 6.3, 'largeur_sepale': 3.3, 'longueur_petale': 6.0, 'largeur_petale': 2.5, 'Nom': 'Virginica'},
                {'longueur_sepale': 5.8, 'largeur_sepale': 2.7, 'longueur_petale': 5.1, 'largeur_petale': 1.9, 'Nom': 'Virginica'},
                {'longueur_sepale': 7.1, 'largeur_sepale': 3.0, 'longueur_petale': 5.9, 'largeur_petale': 2.1, 'Nom': 'Virginica'},
                {'longueur_sepale': 6.3, 'largeur_sepale': 2.9, 'longueur_petale': 5.6, 'largeur_petale': 1.8, 'Nom': 'Virginica'},
                {'longueur_sepale': 6.5, 'largeur_sepale': 3.0, 'longueur_petale': 5.8, 'largeur_petale': 2.2, 'Nom': 'Virginica'},
                {'longueur_sepale': 7.6, 'largeur_sepale': 3.0, 'longueur_petale': 6.6, 'largeur_petale': 2.1, 'Nom': 'Virginica'},
                {'longueur_sepale': 4.9, 'largeur_sepale': 2.5, 'longueur_petale': 4.5, 'largeur_petale': 1.7, 'Nom': 'Virginica'}]