---
title: "Exercices algorithmes des plus proches voisins"
subtitle: "Thème 7: Algorithmiques"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
--- 

## Exercice 1 : QCM &#x1F3C6;

__1.__ Comment appelle-t-on une donnée d'entrée pour construire un algorithme knn?

* [ ] `Un élément`
* [ ] `Un point de donnée`
* [ ] `Un jeu d'instructions`
* [ ] `Une donnée`  

__2__ Comment appelle-t-on l'ensemble des données d'entrée qui permettent de construire l'algorithme knn?  

* [ ] `Le jeu d'éléments`
* [ ] `Le jeu de tests`
* [ ] `Les éléments`
* [ ] `Le jeu d'apprentissage`  

__3__ Pourquoi appelle-t-on cet algorithme l'algorithme des plus proches voisins?  

* [ ] `Car on utilise l'étiquette de la classe des plus proches voisins pour faire une prédiction`
* [ ] `Car on demande à nos voisins de quartier de nous aider`
* [ ] `Car à l'origine cet algorithme a été crée lors de la fête des voisins`
* [ ] `Car le nom algorithme glouton avait déjà été utilisé`  

__4__ Nous disposons du code suivant:  

``` python
import math

def calcul_distance(X_abscisses, X_ordonnées, Y_abscisses, Y_ordonnées):
    return math.sqrt((X_abscisses-Y_abscisses)**2+(X_ordonnées-Y_ordonnées)**2)

```  

Que va renvoyer cette fonction?  

* [ ] `La distance de Manathan entre les points de donnée X et Y`
* [ ] `Le module d'un nombre complexe`
* [ ] `La distance Euclidienne entre les points de donnée X et Y`
* [ ] `Les coordonnées du milieu d'un segment`  

## Exercice 2 : Classifier les iris &#x1F3C6; &#x1F3C6;  

On dispose d'une liste de dictionnaires [__donnees_iris__](donnees_iris.py) regroupant les carctéristiques de trois types d'iris (les Sétosas, les Versicolors et les Virginicas).  
Nous avons pour chaque iris mesuré quatre carctéristiques:  

* La longueur des sépales  
* La largeur des sépales  
* La longueur des pétales  
* La largeur des pétales.  

Voici la structure d'un élément de la liste:  
`{'longueur_sepale': 4.6, 'largeur_sepale': 3.1, 'longueur_petale': 1.5, 'largeur_petale': 0.2, 'Nom': 'Setosa'}`  

1. Donner les classes de cet exercice.  

2. Comment appelle-t-on l'ensemble des données de la liste __donnees_iris__?  

3. Créer une fonction __longueur_sepale__ qui prend en paramètre un dictionnaire de même structure que ceux dans __donnees_iris__ et qui renvoie la valeur associée à la clé __longueur_sepale__  

4. Créer quatre autres fonctions __largeur_sepale__, __longueur_petale__, __largeur_petale__ et __nom_iris__ qui prendront toutes le même type de paramètre que la fonction __longueur_sepale__ et qui renverront respectivement la largeur du sépale, la longueur du pétale, la largeur du pétale et le nom de l'iris.  

5. Créer la fonction __distance__ qui prend deux paramètres. Ces paramètres sont des dictionnaires de même type que ceux dans la liste __donnees_iris__.  

   On utilisera la distance Euclidienne qu'on généralisera aux quatres critères des iris. La fonction renverra un tuple comportant la valeur de la distance et le valeur de la clé 'nom' du deuxième paramètre.  

   Ainsi,  
   `distance({'longueur_sepale': 4.6, 'largeur_sepale': 3.4, 'longueur_petale': 1.4, 'largeur_petale': 0.3, 'Nom': 'Setosa'},{'longueur_sepale': 7.0, 'largeur_sepale': 3.2, 'longueur_petale': 4.7, 'largeur_petale': 1.4, 'Nom': 'Versicolor'})`  

   Donnera:  

   `(4.230839160261237, 'Versicolor')`

6. Créer le même type de fonction mais en utilisant la distance de Manathan.  


## Exercice 3 : HARRY POTTER &#x1F3C6; &#x1F3C6; &#x1F3C6;  

Lorsque de jeunes sorciers arrivent à l'école de Poudlard, le Choixpeau magique répartit les élèves dans les différentes maisons (Elles représenteront les classes dans notre exercice):  

* Griffondor  
* Serpentard  
* Serdaigle  
* Poufsouffle  

Pour choisir la maison de chaque sorcier, le Choixpeau magique s'appuie sur quatre critères (Ils représenteront les quatre critères des points de donnée):  

* Leur courage  
* Leur loyauté  
* Leur sagesse  
* Leur malice.  

Nous disposons d'un fichier `choixpeauMagique.csv` qui contient les points de donnée visés par notre expert qui est le ChoixpeauMagique. Ces points de donnée représentent notre __jeu d'apprentissage__.  

A partir de l'ensemble de ces données, nous allons aider Choixpeau magique à trouver la maison de certains jeunes sorciers dont nous avons les caractéristiques suivantes:  

| Nom | Courage | Loyaute | Sagesse | Malice | Maison |  
|:--------:|:--------:|:--------:|:--------:|:--------:|:--------:|  
| Hermione | 8 | 6 | 6 | 6 | ??? |  
| Drago | 6 | 6 | 5 | 8 | ??? |  
| Cédric | 7 | 10 | 5 | 6 | ??? |  

![Choixpeau magique](images/chapeau.png)  

1. Vous allez commencer par recopier la fonction `convertir_csv_dico` qui prend comme paramètre **nom_fichier** qui représente le nom du fichier dont on souhaite extraire les données.

   Cette fonction permet de renvoyer une liste de dictionnaires à partir du fichier __choixpeauMagique.csv__.  

   ``` python  
   import csv
   
   def convertir_csv_dico(nom_fichier):
       """
       Fonction qui récupère les données d'un fichier .csv et qui les classes sous forme d'une liste de dictionnaires.
       : param : nom du fichier qui contient les données et a pour extension .csv
       :parm type : str
       : return : liste de dictionnaires
       : rtype : list(dict)
       """
       file = open(nom_fichier, newline='')
       csv_en_dico = csv.DictReader(file , delimiter = ';')
       liste = []
       for ligne in csv_en_dico :
           liste.append(dict(ligne))
       file.close ()  
       return liste

   ```  

   Verifier que cette fonction qui a pour paramètre le fichier `choixpeauMagique.csv` renvoie:  

   `[{'Nom': 'Adrian', 'Courage': '9', 'Loyaute': '4', 'Sagesse': '7', 'Malice': '10', 'Maison': 'Serpentar'},  
   {'Nom': 'Andrew', 'Courage': '9', 'Loyaute': '3', 'Sagesse': '4', 'Malice': '7', 'Maison': 'Griffondor'},  
   ...`  
   Stocker ces données dans **jeu_d_apprentissage**

2. Vous allez maintenant créer la fonction __calcul_distance__ qui a pour premier paramètre __mystere__ qui est un jeune sorcier dont on ne connait pas la maison et __sorcier__ qui est un jeune sorcier du jeu d'apprentissage. Ces deux paramètres seront des dictionnaires de la forme:  

   `{'Nom': 'Adrian', 'Courage': '9', 'Loyaute': '4', 'Sagesse': '7', 'Malice': '10', 'Maison': 'Serpentar'}`  

   On utilisera pour cette fonction la distance Euclidienne en la généralisant aux quatre paramètres (Courage, Loyaute, Sagesse et Malice). Cette fonction renverra un tuple qui contient dans l'ordre la distance, le nom du sorcier du jeu d'apprentissage et sa maison.  

   Ainsi, `calcul_distance({'Nom': 'Hermione', 'Courage': '8', 'Loyaute': '6', 'Sagesse': '6', 'Malice': '6', 'Maison': '???'},  
   {'Nom': 'Adrian', 'Courage': '9', 'Loyaute': '4', 'Sagesse': '7', 'Malice': '10', 'Maison': 'Serpentar'})`  

   donnera:  

   `(4.69041575982343, 'Adrian', 'Serpentar')`  

3. Créer la fonction __toutes_les_distances__ qui prend en paramètre __sorcier_mystere__ c'est à dire un sorcier dont on ne connait pas la classe (La maison). Le paramètre sera de type dictionnaire comme pour le 2.  

   La fonction __toutes_les_distances__ renverra une liste de tuples (Distance, Nom du sorcier du jeu d'apprentissage, Maison du sorcier) triée par ordre croissant des distances.  

   Ainsi, `toutes_les_distances({'Nom': 'Hermione', 'Courage': '8', 'Loyaute': '6', 'Sagesse': '6', 'Malice': '6', 'Maison': '???'})`  

   Donnera:  

   `[(2.449489742783178, 'Cormac', 'Griffondor'),  
   (3.0, 'Neville', 'Griffondor'),  
   (3.1622776601683795, 'Colin', 'Griffondor'),  
   (3.1622776601683795, 'Dean', 'Griffondor'),  
   (3.3166247903554, 'Milicent', 'Serpentar'),  
   (3.3166247903554, 'Susan', 'Poufsouffle'),  
   ...`  

4. Nous allons enfin créer la fonction __reponse__ qui prend deux paramètres. Le premier est le nombre __k__ qui est un entier qui détermine le nombre de voisins choisis pour réaliser la prédiction. Le second paramètre est le sorcier mystère qui est un dictionnaire comme vu précédement.  

   La fonction renverra un tuple qui comportera en premier la classe du sorcier mystère(sa maison) et en second le pourcentage de voisins parmi les __k__ qui ont cette classe.  

   Ainsi, `reponse(5, {'Nom': 'Hermione', 'Courage': '8', 'Loyaute': '6', 'Sagesse': '6', 'Malice': '6', 'Maison': '???'})`  

   Donnera:  
   `('Griffondor', '80.0%')`  

5. Pour terminer, trouver la maison de chaque sorcier mystère donné dans le tableau.  

## Exercice 4 : Reconnaissance de caractères &#x1F3C6; &#x1F3C6; &#x1F3C6;  

Dans cet exercice nous allons nous intéresser aux algorithmes d'apprentissage supervisé via la bibliothèque __scikit-learn__.  
C'est une bibliothèque spécialisée dans l'apprentissage automatique et elle est actualisée très régulièrement.  
On commencera donc par installer la bibliothèque __sklearn__ via __Thonny__ dans le gestionnaire de plug-ins.  

Nous aurons besoin aussi de la bibliothèque de gestion d'image __PIL__ et d'une bibliothèque de gestion de tableau __numpy__. Si elles ne sont pas installées, on procèdera comme pour __sklearn__.  

Le but de cet exercice est de créer un algorithme __knn__ de reconnaissance de caractères. Pour cela, la bibliothèque __sklearn__ nous fournira un jeu de points de donnée. C'est une base de données représentant des chiffres manuscrits de 0 à 9. Il y a 1797 références ce qui constitue un nombre suffisament important pour réaliser un algorithme d'apprentissage.  

![Chiffres manuscrits](images/mnist.jpeg)

Nous pourrons diviser les points de donnée de sklearn en deux parties:  

* Le jeu d'apprentissage  
* Le jeu de tests.  

1. Dans un premier temps, nous allons importer les bibliothèques:  

   ``` python  
    from PIL import Image
    import numpy as np
    from sklearn.datasets import load_digits
    from sklearn.model_selection import train_test_split
    from sklearn.neighbors import KNeighborsClassifier
   ```  

   * load_digits est la base de données comportant l'ensemble des points de donnée. sklearn.datasets comporte bien d'autres bases de données.  
   * train_test_split nous permettra de scinder la base de données en __jeu d'apprentissage__ et __jeu de tests__.  
   * KNeighborsClassifier est une classe qui va nous permettre de créer un objet algorithme KNN.  

2. Nous allons utiliser des images extérieures à la base de donnée. Ces photos auront toutes pour caractéristiques 8x8 c'est à dire 8 pixels en largueur et 8 pixels en hauteur soit 64 pixels. Elle ont les mêmes dimensions que les photos de la base de donnée de sklearn mais ils faut les stocker sous le même format que celle de la base de donnée. On dispose de la fonction suivante:  

   ``` python  
   def image(titre):
    im = Image.open(titre)
    tableau = np.array(im)
   ```

   * Image.open() est une instruction qui permet d'ouvrir un fichier image. On affectera le contenu à la variable __im__.  
   * La bibliothèque numpy nous permet de créer des tableaux. On pourra ainsi stocker l'ensemble des informations sous forme d'une structure de tableau dans la variable __tableau__ 

   Affichez la variable tableau et vérifiez qu'elle est constituée de huit listes contenant elle-même huit listes de trois éléments.  
   Cela représente les 64 pixels et chaque pixel contient trois valeurs qui correspondent au codage RVB c'est à dire une valeur pour la couleur __rouge__, une valeur pour la couleur __verte__ et une pour le couleur __bleue__.  

   On souhaite en réalité avoir une liste de 64 éléments et chaque élément est l'addition des trois valeurs liées aux couleurs RVB. On donne la fonction suivante:  

   ``` python  
   def image(titre):
    im = Image.open(titre)
    nouvelle_liste = []
    tableau = np.array(im)  
    #Code à compléter  
    return np.array([nouvelle_liste])  
    ```  

   Complétez le code pour que `image('mystere_1.png')`  

   Donne:  
   `array([[ 21,  87, 633, 684, 672,   6,   0,   0,  66, 738, 753, 714, 732,
        177,   0,   0, 714, 735, 435, 720, 741, 174,   0,   0, 663, 312,
        216, 747, 747, 297,   0,   0,   0,  57, 279, 708, 729, 297,  63,
          0,   0,  72, 354, 723, 738,  93,   3,   0,   6,  99, 297, 741,
        750, 150,   0,   0,   0, 123, 339, 705, 678, 252,   0,   0]])`

3. Créez la fonction __algo_knn__ qui prend pour paramètre __image_mystere__. Ce paramètre est une chaîne de caractères comportant le nom de l'image mystère avec son extension.  
   Un jeu d'images mystères se trouve dans le dossier, vous pourrez tester votre code avec ces images puis dans la question suivante, vous pourrez créer vos propres images.  

   Cette fonction fait appel à la programmation objet que nous n'avons pas encore abordée. La structure de cette fonction sera donc complètement donnée. Nous allons juste comprendre chaque ligne de code afin de se familiariser avec la bibliothèque scikit-leran.  

   * Nous allons commencer par créer l'objet __base_de_donnees__ en instanciant la classe __load_digits()__  

   `base_de_donnees = load_digits()`  

   Nous créons un objet nommé __base_de_donnees__ en faisant appel à la classe __load_digits()__.  
   Ainsi, __base_de_donnees__ aura des attributs et méthodes qui lui sont propres.  

   * __base_de_donnees__ est une structure complexe nous allons donc créer une variable __donnees__ qui va contenir les données des 1797 images de la base de données sous forme de tableaux. Nous allons aussi créer une variable __etiquettes__ qui va contenir l'ensemble des noms des images.  

   `donnees = base_de_donnees.data`  
   `etiquettes = base_de_donnees.target` 

   * On scinde la base de données en deux parties (le jeu d'apprentissage et le jeu de tests) avec l'objet  
    __train_test_split__. Cet objet renvoie quatre valeurs (x_entrainement, x_test, y_entrainement, y_test)  
   Cet objet prend trois paramètres qui sont: donnees, etiquettes et test_size qui fixe la proportion des  
   données qui vont être utilisées pour faire les tests. Souvent on prend test_size = 0.25 ce qui équivaut à 25% des données pour les tests.  

   `x_entrainement, x_test, y_entrainement, y_test = train_test_split (donnees, etiquettes , test_size= 0.25)`  

   Maintenant que nous avons récupéré les données et les étiquettes des classes et que nous avons divisé notre base de données en un jeu d'apprentissage et un jeu de tests, il faut créer un objet qui va nous permettre de réaliser l'algorithme KNN.  

   * Nous allons créer l'objet __KNN__ en instanciant la classe __KNeighborsClassifier__. Cette classe prend un paramètre k qui est le nombre de voisins pris en compte pour réaliser la prédiction. Comme nous avons 1797 références, nous fixerons arbitrairement la valeur de k à 200. Vous pourrez changer cette valeur par la suite.  

   `KNN = KNeighborsClassifier(200)`  

   Cet objet KNN possède plusieurs méthodes.  

   * Nous allons utiliser la méthode `.fit()` qui va réaliser la phase d'entrainement. Cette méthode prend deux paramètres x_entrainement et y_entrainement. (x_entrainement correspond aux données de la base de données réservées à l'entrainement et y_entrainement aux étiquettes correspondantes)  

   `KNN.fit(x_entrainement, y_entrainement)`  

   * Nous allons maintenant préparer l'image mystère qu'on souhaite tester en appelant la fonction qu'on a réalisé au 2.  
   `test_image = image(image_mystere)`  

   * On renvoie le résultat avec le niveau de confiance. Pour cela, on utilise:  
     la méthode `.predict()` appliquée à l'objet KNN. Elle prendra pour paramètre __image_externe__. Cela renverra la classe de l'image mystère.  
     `KNN.predict(test_image)`

     La méthode `.score()` appliquée à l'objet KNN. Elle prendra deux paramètres, x_test et y_test. Cela renverra un nombre compris entre 0 et 1 qui fixera le taux de confiance.  
     `KNN.score(x_test, y_test)`

   Vérifier avec les images mystères fournies que votre code donne la prédiction et le taux de confiance.  

   Ainsi, `algo_knn('mystere_0.png')`  

   Donnera:  

   `(array([0]), '89.8%')`  

   array([0]) : signifie chiffre 0  
   89.8% : Taux de confiance de 89.8%  

4. Vous pouvez créer vos propres images et tester.  

   Pour cela, vous devez utiliser un logiciel de dessin comme __GIMP2__.  
   Vous créez une nouvelle image.  
   Vous fixez les dimensions à largeur 8px et hauteur 8px.  
   Vous zoomez pour voir la zone de travail.  
   Vous mettez le fond en noir.  
   Avec la gomme vous tracez le chiffre que vous voulez.  
   Vous exportez sous le format .png  

   Testez dans votre programme.  

