---
title: "Chapitre 21 Algorithmes des plus proches voisins"
subtitle: "Thème 7: Algorithmiques"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
--- 

Nous venons de voir les algorithmes gloutons qui s'efforcent d'apporter la solution optimale au problème posé.  
Les algorithmes des plus proches voisins qu'on appelle aussi algorithmes à base d'apprentissage apportent une réponse plausible mais pas forcément exacte. On parle d'une prédiction.   
On utilise ce type d'algorithme lorsqu'il est difficile d'appliquer des algorithmes traditionnels.  
  
## I Première approche des algorithmes à base d'apprentissage  

Nous avons des données sur les alligators et les crocodiles. Nous dirons que nous avons deux __classes__ d'animaux. Ces données portent sur deux critères:  

- La longueur de leur corps.  
- La taille de leur gueule.  

Ces données ont été analysées par un spécialiste des alligators et crocodiles et il a pu pour chaque couple de données(longueur du corps taille de la gueule) affirmer si l'animal était dans la classe alligator ou dans la classe crocodile.
Ces données seront appelées données __du jeu d'apprentissage__. Chaque animal sera qualifié de __point de donnée__ et le nom de la classe sera appelé __l'étiquette__ de la classe. 

Pour plus de clarté, ces __points de donnée__ ont été portés sur un graphique avec en ordonnée la longueur du corps et en abscisse la taille de la gueule.  

Les points de donnée bleus représentent les alligators, les points de donnée rouges les crocodiles et les points de donnée verts, des animaux dont on a les caractéristiques mais qui n'ont pas été examinés par le spécialiste.  
Ils ne font pas partie du jeu d'apprentissage.

![Graphique regroupant les données des crocodiles et alligators](./images/knn_aligators_analyse.png)

## Analyse du graphique:  

Les points de donnée nous montrent trois zones bien distinstes:  

*  une qui regroupe l'ensemble des crocodiles. (Classe crocodile) 
*  une autre regroupant l'ensemble des alligators. (Classe alligator) 
*  une zone qui est le reste sur laquelle il est difficile de se prononcer.

Le but d'un algorithme des plus proches voisins est de pouvoir, sans l'avis d'un expert, faire une prédiction sur la classe de nouveaux animaux en connaissant uniquement leurs caractéristiques et les points de donnée du jeu d'apprentissage.  

Ici, d'après le graphique, nous pouvons dire que le point de donnée "mystère 1" doit être de la classe alligator, le point de donnée "mystère 2" doit être de la classe crocodile mais il est difficile de se prononcer pour le point de donnée "mystère 3" car il est graphiquement entre les classes alligator et crocodile.  

## Construction d'un algorithme des plus proches voisins.  

a) Les différents types d'algorithmes KNN (k Nearest Neighbors)  

- Celui que nous allons développer dans le cours est l'algorithme KNN avec apprentissage supervisé. Cela reprend notre premier exemple. Nous disposons des points de données du jeu d'apprentissage qui ont été supervisées par un expert. L'expert nous permet donc de trouver les classes et de leur donner une étiquette.  

- Les algorithmes KNN avec un apprentissage non supervisé. Aucun expert ne supervise le travail. Nous pouvons avec les données trouver les classes (même si cela est plus difficile car l'expert ne sera pas présent pour trancher sur les cas limites) mais nous ne pouvons pas attribuer d'étiquette.  

- Les algorithmes KNN avec un apprentissage par renforcement. On part avec un jeu d'apprentissage visé par un expert et ce jeu est renforcé avec de nouvelles données acquises.

b) Règles pour la construction d'un algorithme KNN avec apprentissage supervisé.  

## Comment avons nous réussi à attribuer une classe aux animaux mystères dans l'exemple alligators-crocodiles?  

Nous avons simplement observé graphiquement l'environnement de chaque point de donnée mystère. Si par exemple nous prenons les quatre plus proches voisins des points de donnée mystères. Le point de donnée "mystère 1" est entouré d'animaux de classe alligator. Nous avons donc attribué la classe alligator au point de donnée "mystère 1". De la même manière pour le point de donnée "mystère 2", nous lui avons attribué la classe crocodile. Pour le point de donnée "mystère 3" il nous est impossible de trancher car parmi les quatre voisins il y a autant de points de donnée de classe crocodile que de points de donnée de classe alligator. (Il faudra donc consulter l'expert) 

![Graphique regroupant les données des crocodiles et aligators.](./images/Knn_aligators_kvoisins.png)  

Même si les résultats obtenus ne sont pas forcément exacts, nous allons nous appuyer sur cette idée de plus proches voisins pour construire nos algorithmes.  

__Nous regarderons la classe des plus proches voisins d'un point de donnée mystère. Nous déterminerons la classe majoritaire. Cette classe majoritaire sera la classe du point de donnée mystère.__  

Il nous faudra donc choisir avec __combien de voisins__ nous voulons travailler et calculer __la distance entre le point de donnée mystère et les points de donnée du jeu d'apprentissage__ pour trouver les voisins les plus proches.  

## Comment calculer les distances entre le point de donnée mystère et les points de donnée du jeu d'apprentissage ?  

Nous utiliserons cette année deux méthodes possibles de calcul de distance (Nous choisirons l'une ou l'autre):  

- La méthode de la distance Euclidienne.  

  > Distance = $`\sqrt{(x_{1} - x_{2})^{2} + (y_{1} - y_{2})^{2}}`$


Par exemple: On reprend l'exemple du I.  
Point de donnée mystère 1 : longueur = 4,2 m  et taille de la gueule = 0,30 m  
Point de donnée du jeu d'apprentissage: longueur = 4,8 m et taille de la gueule = 0,08 m  

La distance sera de : Distance = $`\sqrt{(4,2 - 4,8)^{2} + (0,30 - 0,08)^{2}}`$ = 0,64  

Remarque: Si nous avons plus de deux caractéristiques, nous généraliserons la formule ci-dessus.  

- La méthode de la distance de Manathan.  

  >Distance = $`\left | x_{1} - x_{2} \right | + \left | y_{1} - y_{2} \right |`$ 

On reprend l'exemple ci-dessus.  
La distance sera de : Distance = $`\left | 4,2 - 4,8 \right | + \left | 0,30 - 0,08 \right |`$ = 0,82  

Conclusion: Nous utiliserons la distance Euclidienne ou la distance de Manathan pour calculer la distance entre le point de donnée mystère et chaque point de donnée du jeu d'apprentissage.  

## Combien de voisins faut-il choisir ?  

- Nous allons dans un premier temps nous appuyer sur le problème des alligators et des crocodiles. Nous allons faire varier k (nombre de voisins) de 1 à 10 puisque le jeu d'apprentissage contient au maximum 10 éléments. Nous verrons pour les points de donnée mystères qui sont leurs voisins.  

| | Cas mystère 1 ||| Cas mystère 2 ||| Cas mystère 3 ||  
|:--------:|:--------:|:--------:|:---------:|:---------:|:---------:|:---------:|:---------:|:---------:|  
| | Alligator | Crocodile || Alligator | Crocodile || Alligator | Crocodile |  
| k = 1 | 1 | 0 || 0 | 1 || 1 | 0 |  
| k = 2 | 2 | 0 || 0 | 2 || 1 | 1 |  
| k = 3 | 3 | 0 || 0 | 3 || 2 | 1 |  
| k = 4 | 4 | 0 || 0 | 4 || 2 | 2 |  
| k = 5 | 5 | 0 || 0 | 5 || 3 | 2 |  
| k = 6 | 5 | 1 || 1 | 5 || 3 | 3 |  
| k = 7 | 5 | 2 || 2 | 5 || 4 | 3 |  
| k = 8 | 5 | 3 || 3 | 5 || 5 | 3 |  
| k = 9 | 5 | 4 || 4 | 5 || 5 | 4 |  
| k = 10 | 5 | 5 || 5 | 5 || 5 | 5 |  

Nous voyons, pour les points de données mystères 1 et 2, que prendre un maximum de voisins n'est pas la meilleure solution car pour k inférieur ou égal à 5 nous pouvions nous prononcer alors que pour k = 10, nous ne pouvons plus. Nous prenons trop de voisins. On ne peut plus donner une prédiction tranchée.  

Cependant, pour k inférieur ou égal à 5, nous ne pouvons pas nous prononcer pour le point de donnée mystère 3 car ce n'est pas tranché.

Le cas idéal semble être k = 8 car l'écart est significatif pour les trois points de donnée mystères.  
Remarque cependant, nous travaillons avec trop peu de points de donnée dans le jeu d'apprentissage, dans tous les cas il est difficile de trancher. Dans un algorithme KNN, il faut un jeu d'apprentissage important pour avoir des prédictions fiables.  

- Comment choisir la valeur de k lorsque le jeu d'aprentissage comporte par exemple 100 éléments ?  

Nous allons diviser La base de données (Alligators Crocodiles) en deux parties:  

- La première partie servira à réaliser la courbe d'apprentissage c'est à dire qu'ils vont représenter les points de donnée qui vont être utilisés pour mesurer les distances avec les points de donnée mystères. Souvent on prend 75% des points de donnée. Ils représenteront le __jeu d'apprentissage__.

- La deuxième partie (25%) servira à réaliser des tests. Nous testerons ces 25% sur la courbe d'apprentissage en faisant varier __k__ comme ci-dessus. Comme nous connaissons la classe de ces éléments, nous verrons pour chaque élément si la prédiction de notre algorithme KNN a été juste ou non. Nous pourrons choisir la valeur de __k__ pour laquelle il y a eu le plus de prédictions justes.  

## Ce qu'il faut retenir:  

- Il y a plusieurs types d'algorithme d'apprentissage. Mais nous ne travaillerons qu'avec les algorithmes d'apprentissage supervisé.  

- Comment réaliser une prédiction sur un point de donnée mystère ?  
  - Nous calculons la distance entre le point de donnée mystère et les points de donnée du jeu d'apprentissage.  
  - Nous fixons le nombre de voisins pour effectuer la prédiction.  
  - Nous regardons la classe majoritaire des voisins. Elle fixera la classe du point de donnée mystère.  

- Pour évaluer la fiabilité de notre prédiction, nous divisons les point de donnée visés par l'expert en deux catégories:  
  - 75% des points de donnée vont former le jeu d'apprentissage. Ils vont servir à construire la courbe d'apprentissage.  
  - 25% des points de données vont former le jeu de tests. On teste ces points de donnée sur la courbe d'apprentissage. Comme on connait leur classe, on pourra déterminer le taux de fiabilité de notre prédiction.  
