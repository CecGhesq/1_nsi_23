from PIL import Image
import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

def algo_knn(image_mystere):
    base_de_donnees = load_digits()
    donnees = base_de_donnees.data 
    etiquettes = base_de_donnees.target 
    x_entrainement, x_test, y_entrainement, y_test = train_test_split(donnees, etiquettes, test_size=0.25) 
    KNN = KNeighborsClassifier(200) 
    KNN.fit(x_entrainement, y_entrainement)
    test_image = image(image_mystere)    
    return (KNN.predict(test_image), str(round(KNN.score(x_test, y_test)*100,1))+'%')

def image(titre):
    im = Image.open(titre)
    h = []
    tableau = np.array(im)
    for i in range(8):
        for j in range(8):
            h.append(int(tableau[i][j][0])+int(tableau[i][j][1])+int(tableau[i][j][2]))
    return np.array([h])
