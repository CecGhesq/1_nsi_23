---
title: "C14  HTML5 et CSS3"
subtitle: "Thème 4 : Interaction entre l'homme et la machine sur le Web "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
# Présentation de VSCodium

* C’est l’éditeur que nous allons utiliser pour visualiser le code HTML, CSS et JavaScript.
* Pour visualiser du code sur cette éditeur, il suffit d’ouvrir le document dans l’éditeur.
* Pour coder avec cet éditeur, il faut :
    * créer un nouveau fichier
    * renseigner le nom avec l'extension pour le langage ( .html .css .js)
    * l'encodage (UTF-8 par exemple) est écrit en bas de la page et peut êter modifié en cliquant dessus.

# Présentation de HTML 5

## Qu’est-ce que le langage HTML ?

HTML signifie HyperText Markup Langage ce qui se traduit par langage de balisage hypertexte. Le langage HTML est donc un langage de balisage qui structure une page web.

Les éléments HTML rencontrés sont délimités soit par:
* un couple de balise (ouvrantes / fermante):   \<balise >       \</balise>
* une seule balise (autofermante) :   \<balise/>
  
## Structure générale d’une page HTML

Un fichier codé en HTML sera interprété par un navigateur. Il faut donc dans le document HTML préciser plusieurs éléments afin que l’interprétation soit réussie.

* < !DOCTYPE html> : cette balise précise au navigateur que le type du document est HTML.
* L’élément racine html délimité par  les balises \<html> et \</html> qui correspondent au début et la fin du document.
* Puis, les éléments « head » et « body » (tous les deux délimités par un couple de balise) qu’on retrouve à l’intérieur de l’élément html.

## Présentation du « head »

* On y on fixe l’encodage des caractères.
* On y réalise les liens vers les fichiers externes.
  
![](./img/head.png)

| Première ligne | ![](./img/charset.JPG)|
|:----------------:|:--------:|

* C’est une balise meta qui fournit des métadonnées au navigateur.

* C’est une balise auto fermante car elle termine par />.
* Le nom de la balise (meta) est suivi d’un **attribut** (charset) associé à une **valeur** (utf-8).
* Cette ligne d’instruction s’occupe de l’encodage des caractères. Elle s’occupe donc de préciser au navigateur avec quel jeu de caractères il devra réaliser l’affichage. Ici c’est en **UTF-8**.

| Deuxième ligne | ![](./img/name.JPG)|
|:----------------:|:--------:|

C’est une balise qui permet de nommer l’auteur de la page HTML. C’est une métadonnée donc elle n’est pas visible sur la page lors de l’affichage. On peut retrouver cette indication dans les propriétés du document.

| Troisième ligne | ![](./img/title.JPG)|
|:----------------:|:--------:|

Cette ligne permet grâce aux balises ouvrante et fermante de mettre un nom à l’onglet.

_Remarques_ :

* Comme en Python, il est important d’indenter le code pour en faciliter sa lecture. Attention, contrairement à Python elle n’est pas automatique.

* Les phrases apparaissant en vert dans Notepad++ sont des **commentaires** délimités par les caractères
  < !--     et             -->

## Présentation du body

C’est dans cette zone qu’on réalise ce qui va être affiché à l’écran.

* Que se passe-t-il si on met dans le body un texte organisé mais sans balise ?
Si on ne met pas de balise, le texte est affiché sans garder la mise en forme.
* Les différentes balises et leur utilité :

### Balises pour structurer un texte
 (visualiser les fichiers présents dans le dossier Exemples_cours)

|Balise utilisée|Affichage dans le navigateur|
|:-----:|:------:|
|Les éléments h1 à h6 servent pour les titres. Ils ont un style prédéfini qui peut être modifié en CSS|![50% center](./img/titrerz.JPG)|

```html
      <h1>Titre1\</h1> 
      <h2>Titre2\</h2>
      <h3>Titre3\</h3>
```

|Balise utilisée|Affichage dans le navigateur|
|:-----:|:------:|      
|L’élément p permet de structurer un texte en paragraphe:   |Aucun changement visuel ; Un retour chariot qui sépare ce qui est contenu entre les balises et le reste du texte.|

```html
<p>La balise p permet de donner une structure de paragraphe à l’ensemble d’un texte.</p>
```

|Balise utilisée|Affichage dans le navigateur|
|:---:|:----:|
|La balise \<br/>|Elle provoque un retour chariot|
|Les listes non ordonnées  ul et li ; elles permettent de lister du texte sans le numéroter|![uli](./img/ulli.JPG)|

```html
<ul>
    <li> Première étape.</li>
    <li> Deuxième étape.</li>
    <li> Troisième étape.</li>
</ul>
```

**Réaliser l’exercice n°1**: Mettre en forme un texte. Utiliser le fichier exercice1 en utilisant uniquement les balises du tableau ci-dessus. Une photo « correction_exercice_1 » vous donne un visuel de ce que vous devez réaliser.

### Balise pour mettre des images sur une page html: 
La balise img est une balise auto fermante. L’attribut src permet de donner le chemin relatif vers l’image.

```html
<img src = "photo.jpg"/>
```

* Ne pas oublier l’extension.
* Le chemin relatif se fait ici par rapport au fichier HTML.

### Balise pour créer des lien hypertextes

* La balise a est utilisée pour créer des liens vers d’autres pages web ou pour se rendre rapidement à d’autres endroits de la page :
  
```html
<a href = "lien vers une page web">lien</a>
```

Si le lien est actif, il apparait en bleu et il est souligné.

### Les balises div : Balises génériques

* L’élément div est utilisé pour partitionner une page web. 
C’est un élément générique sans signification particulière
	
```html
<div>Première page HTML</div>
```

* Si rien n’est spécifié dans le CSS, on ne voit qu’un retour chariot.

**Réaliser l’exercice n°2** : Vous allez structurer une page web en utilisant le matériel proposé et en respectant le visuel.

# Le langage CSS

## Qu’est-ce que le CSS ?

Le langage CSS vient en complément du langage HTML. Le langage HTML structure la page web alors que le langage CSS met en forme le document.

## Comment lier les documents HTML et CSS ?
Il faut ajouter un lien dans le head du fichier HTML.

```html
<link rel="stylesheet" href="style.css" />
```

Dans cette balise, la valeur associée à l’attribut **href** permet d’indiquer le chemin vers le fichier externe codé en CSS. On n’oubliera pas de mettre  l’extension **.css** après le nom du fichier. Le chemin est un chemin relatif depuis le fichier HTML.

## Comment appliquer un style à un élément présent dans le code HTML ?

* Il est possible d’appliquer le style directement à un élément en utilisant un sélecteur correspondant à son nom.
Par exemple : div, p, ul …
Remarque : Le problème de cette méthode est que si on applique un style à une div par exemple, l’ensemble des div du document HTML auront le même style.

* La deuxième méthode est de mettre un nouvel attribut dans les balises : class
Par exemple :

```html
<div class = "test"></div>
```

Toutes les balises qui auront le nom de classe "test" quelle que soit la balise pourront avoir un style identique.

* La dernière méthode est d’ajouter un attribut id 
Par exemple :

```html
<div id = "test"></div>
```

La différence avec la classe, c’est que l’**id** est unique. Le style ne s’appliquera qu’à cette balise.

## comment appliquer un style à une balise?

Pour appliquer un style à une ou plusieurs éléments, on sélectionne les sélectionne soit 

* par leur nom :
On ouvre une accolade { , on donne la propriété CSS (ici color) et on lui affecte une valeur. On peut définir plusieurs couples propriétés / valeurs entre les accolades. Il faut toujours terminer une ligne par un point-virgule ; et refermer l’accolade } à la fin.

```css
p
{
    color:red;
}
```

* par leur identificant (id) :
le sélecteur est alors constitué du caractère # suivi de la valeur de l’attribut id

```css
#menu
{
    background-color:: #AACC08 ;
}
```

* par leur classe :
le sélecteur est alors constitué du caractère . suivi de la valeur de l’attribut class

```css
.generale
{
    font-size : 2em ;
}
```

## Propriétés de base en CSS

|Propriétés|Valeurs|
|---|:---:|
|**width** : Largeur d’un élément|px (pixel) ou vw(pourcentage de la largeur de la fenêtre)|
|**height**: Hauteur d’un élément(Valeur défini que pour certaines balises)|px (pixel) ou vh(pourcentage de la hauteur de la fenêtre)|
|**margin**: marge extérieure de l’élément|px (pixel) vh ou vw|
|**padding** : marge intérieure|px (pixel) vh ou vw|
|**background-color** : couleur de fond|: #CCAABB 
|**color**: couleur de la police| #CCAABB|
|**opacity**: opacité d’un élément|Valeur comprise entre 0 et 1.|
|**font-size** : Taille de la police|em px (vh et vw sont possibles)|

**Concernant les couleurs** :
* Couleur exprimée en hexadécimale :  Les deux premiers symboles après # représente la valeur du rouge, le deux suivants la valeur du vert et les deux derniers celle du bleu.
  
* Couleur exprimée en décimale : rgba(255, 255, 255, 0.5)
Fonction qui prend comme premier paramètre la valeur du rouge (0 à 255) comme deuxième paramètre la valeur du vert, comme troisième paramètre la valeur du bleu et le dernier paramètre la transparence.


**Réaliser l’exercice 3**:
* Observez les structures des documents HTML et CSS sur l’OCEAN.
* Modifiez les valeurs dans le fichier CSS et observez les effets.
* Remarque : les phrases placées entre les symboles /* et */ sont des commentaires

*Flex-box* : Voir les sites suivants pour comprendre l’utilisation des flex-box : 
[https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox)
[https://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html](https://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html)


# Travail à la maison : 

* Vous choisissez un pays que vous présentez sur une page web.
* La structure HTML est imposée par le document HTML fourni (Vous devez le compléter)
* Le document CSS est déjà lié au document HTML.
* Vous devez vous inspirer du document océan pour créer vos pages HTML et CSS mais votre version doit être différente.
