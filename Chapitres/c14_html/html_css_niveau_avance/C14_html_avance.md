---
title: "Chapitre 14 Projet HTML CSS "
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---  

Votre travail se déroule en deux temps :
-	En classe, vous allez réaliser un projet où vous allez réaliser la présentation d’un film  
-	A la maison, vous devez préparer une page web présentant trois films. 

Travail en classe : 2 heures.
-	Vous choisissez votre film.  
-	La structure HTML est imposée par le document HTML fourni (Vous devez le compléter.).  
-	Le document CSS est déjà lié au document HTML.
-	Le document CSS ne comporte que les instructions sur la balise body. Vous devez le compléter pour avoir un document web proche visuellement de la photo fournie.  

Aides : 
Propriétés de base en CSS :  (liste non exhaustive qui peut être complétée)  
|Propriétés|Valeurs possibles|
|:---:|:---:|
|__width__: Largeur d’un élément|px (pixel) ; vw(pourcentage de la largeur de la fenêtre)|
|__height__: Hauteur d’un élément(Valeur définie que pour certaines balises)|px (pixel) ; vh(pourcentage de la hauteur de la fenêtre)|
|__margin__: marge extérieure de l’élément|px (pixel) ; vh ou vw |
|__padding__ : marge intérieure|px (pixel) ; vh ou vw|
|__background-color__ : couleur de fond|__Couleur exprimée en hexadécimale__ #CCAABB Les deux premiers symboles après # représente la valeur du rouge, le deux suivants la valeur du vert et les deux derniers celle du bleu. __Couleur exprimée en décimale___ : rgba(255, 255, 255, 0.5) Fonction qui prend comme premier paramètre la valeur du rouge (0 à 255) comme deuxième paramètre la valeur du vert, comme troisième paramètre la valeur du bleu et le dernier paramètre la transparence.|
|__color__: couleur de la police	|Couleur exprimée en hexadécimale : #CCAABB|
|__opacity__: opacité d’un élément|Valeur comprise entre 0 et 1.|
|__font-size__ : Taille de la police|em ; px (vh et vw sont possibles)|  

__Flex-box__ : Voir les sites suivants pour comprendre l’utilisation des flex-box.
https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox
https://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html


Travail à la maison : 
-	Vous choisissez trois films que vous présentez sur une page web.
-	La structure HTML est imposée par le document HTML fourni (Vous devez le compléter.). 
-	Le document CSS est déjà lié au document HTML.
-	Vous devez vous inspirer de la présentation du document correction présenté en classe.
-	La présentation des trois films doit tenir sur une page web sans scroll.


Aides :
Transitions
https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Transitions/Utiliser_transitions_CSS

Site généraliste en CSS3
http://www.css3create.com/

