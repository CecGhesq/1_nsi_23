---
title: "Chapitre 11 Spécification et mise au point de programmes "
subtitle: "Thème 6: Langage et programmation"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

La **spécification**  d'une fonction consiste à décrire , à lister de manière explicite les exigences  de ce que doit faire cette fonction. 
Nous allons voir comment spécifier et mettre au point son programme pour être le plus rigoureux possible.

# Documenter son programme
Il est fortement conseillé de donner une documentation ( description) à chaque fonction :

```python
def f(x) :
    """ retourne le carré de x.
    f est la fonction carré
    : x : (int) 
    @ return (int) 
    """
    return x*x
```

Cela peut aider l'utilisateur :

```python
>>>help(f)
Help on function f in module __main__:

f(x)
    retourne le carré de x.
    f est la fonction carré
    x : (int) 
    @ return (int)
```

La documentation (on dit docstring) est un attribut attaché à l'objet fonction :

```python
>>> f.__doc__
' retourne le carré de x.\n    f est la fonction carré\n    : x : (int) \n    @ return (int) \n    '
```

***La documentation ne remplace pas le commentaire***. Il faut cependant s'abstenir de commenter à outrance : le plus souvent des noms de variables ou de fonctions bien choisis remplacent avantageusement des commentaires. Ainsi on préférera nommer une somme gagnée *gain* plutôt que *x*.

__📢 Définition :__
>La documentation doit contenir :    
    * une phrase qui indique à quoi sert la fonction  
    * d'autres phrases s'il existe des contraintes sur les paramètres en entrée (comme strictement positifs,...)  
    * les types des arguments d'entrée  
    * la description du (ou des) argument(s) éventuel(s) de sortie ainsi que son (leur) type(s)

Par exemple : 

```python
from math import sqrt
def racine(x) : 
    """Calcule la racine carrée d'un nombre, renvoie le résultat
    : x : un nombre positif ou nul (entier, flottant)
    @ return resultat : un flottant
    """
    resultat = sqrt(x)
    return resultat
```

La deuxième ligne du docstring précédent est prénommée `précondition`  : elle décrit la condition d'utilisation de la fonction (CU).
La troisième ligne est nommée `postcondition` sur le résultat

# Programmation défensive

## Assert
Si on veut éviter qu'une utilisation  d'une fonction donne un résultat incohérent, qui risquerait de déclencher une erreur plus tard, on peut __interrompre__ le programme dès lors que la fonction reçoit une information non valide :

```python
def indice_maximum_tableau(t):
    """Renvoie l'indice du maximum du tableau t.
    CU : Le tableau t est supposé non vide
    : t : (list)
    """
    assert len(t) > 0,"  le tableau est vide"
    m=0
    for i in range(len t(t)):
        if t[i]>t[m] :
            m = i
    return m
```

Si la précondition sur le tableau n'est pas remplie , la fonction `assert` combine **le test d'une condition** et **l'interruption du programme** avec un message dans le cas où cette condition n'est pas vérifiée.  
__📢 Syntaxe :__

> La syntaxe est donc :  
**assert condition , "commentaire"**

On voit ainsi que si la fonction appelle un tableau vide, le test échoue et le programme est interrompu :

```python
>>> indice_maximum_tableau([])
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "C:\Users\***\cours.py", line 10, in indice_maximum_tableau
    assert len(t) > 0,"  le tableau est vide"
AssertionError:   le tableau est vide
```

On appelle ceci la **programmation défensive**.

## La valeur None

__📢 None :__
>Une autre façon d'être défensif consiste, plutôt que d'échouer, à renvoyer une valeur qui ne peut être confondue avec un résultat valide.

```python
def indice_maximum_tableau(t):
    """Renvoie l'indice du maximum du tableau t.
    ou None s'il est vide"""
    if len(t) == 0:
        return None
    .........
```

Cette méthode permet d'appeler la fonction sans précaution particulière et de tester avant de l'utiliser.

```python
r = indice_maximum_tableau(t)
if r != None:
print("le maximum est " , t[r])
```

# Tests et mise au point

Pour détecter les erreurs, on peut réaliser des `tests` pour vérifier les  préconditions, postconditions, ces dernières étant les premières lignes des docstrings renseignant sur le rôle de la fonction.  

## Assert et test d'égalité

Plutôt que d'effectuer les tests dans le boucle interactive de Python, une  solution consiste à les inclure dans le même fichier que le programme avec la construction `assert` et un test :  

```python
def produit(a,b):
    """Produit de a par b
    entrée : a, b nombres
    sortie : nombre
    """
    return a*b

assert produit(3,2) == 6 , "le produit est validé"
assert produit( 2.3,3.1) == 7.13 , "le produit est validé"
```

Si le test échoue, il faut rectifier le programme. Une fois l'erreur corrigée, il convient de relancer TOUS les tests, car on peut avoir introduit une autre erreur.

## Doctests et module doctest

On peut si on le désire, inclure des tests dans la documentations (on parle de *doctest*)

```python
def produit(a,b):
    """
    Produit de a par b
    Exemple:
    >>> produit(3,2)
    6
    >>> produit( 2.3,3.1)
    7.13
    """
    return a*b

import doctest
doctest.testmod()
```

Si on exécute ce programme, `testmod` teste la fonction et comme il ne détecte rien d'anormal, on a l'impression qu'il ne s'est rien passé de plus que d'habitude.
En revanche, si les résultats annoncés dans le doctest ne correspondent pas à ceux qui sont retournés par la fonction on obtient un affichage du type ( on a annoncé pr exemple que le produit de 3 par 2 doit être égal à 7):

```python 
File "cours.py", line 32, in __main__.produit
Failed example:
    produit(3,2)
Expected:
    7
Got:
    6
1 items had failures:
   1 of   2 in __main__.produit
***Test Failed*** 1 failures.
```

## Qualité des tests

Il est parfois difficile de choisir le nombre de tests. A partir de combien est-on efficace ?
Voici des usages généraux sur ceux-ci :

* si la spécification du programme mentionne plusieurs cas, chacun de ces cas doit faire l'objet d'un test.  
* si une fonction renvoie une valeur booléenne, essayer d'avoir des tests impliquant chacun des deux résultats possibles.  
* si le programme s'applique à un tableau, il faut inclure un test couvrant le cas du tableau vide.  
* si le programme s'applique à un nombre, il peut être utile de tester ce nombre pour des valeurs >0, <0 ou =0.  
* si le programme fait intervenir un nombre appartenant à un certain intervalle, il peut être utile d'inclure des tests sur les limites de l'intervalle (pour un tableau, les indices 0 et maximal par exemple).  


# Variables locales et globales

## Variables locales 

Dans les erreurs communes lors de l'écriture d'un programme, la gestion des erreurs locales et globales est classique.
  **Une variable locale** est une variable créée au sein d'une fonction , elle **ne sera pas accessible dans le programme principal**.

```python
def ajoute_1(x):
    resultat = x + 1 #resultat est une variable locale
    return resultat
```

*resultat* est une variable locale ; son appel a l'extérieur de la fonction renvoie un message d'erreur:

```python
>>> ajoute_1(6)
7
>>> resultat
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
NameError: name 'resultat' is not defined
```

## Variable globale  

**Une variable globale** est une variable créée dans le programme principal (c'est à dire à la racine du fichier .py créé), **elle sera accessible partout** : dans les fonctions et dans le programme principal.

```python
y=2
def f(x):
    return y+x # on se sert de y dans la fonction
```

*y* est une variable globale car déclarée dans le programme principal. *y* est visible dans le programme principal, y compris dans la fonction f. 

Ainsi, même si dans une fonction on nomme la variable locale du même nom, elle conservera sa valeur : 

```python
y=6
def multiplie_2(valeur):
    y = 2 * valeur
    return y
```

```python
>>> y
6
>>> multiplie_2(8)
16
>>> y
6
``` 

Pendant l'exécution de multiplie_2(8), il existe simultanément deux variables  *y* : la variable définie à l'extérieur de la fonction et la variable locale  à la fonction. Pendant l'exécution de la fonction, **seule la variable locale est accessible**. On dit qu'elle masque la première variable.
Ainsi de la même façon, lorsqu'une valeur est passée en **argument**, elle est reçue comme une **variable locale** à la fonction, dont la durée de vie est exactement l'exécution de la fonction.

Dans certaines situations, il serait utile de pouvoir modifier la valeur d’une variable globale depuis une fonction, notamment dans le cas où une fonction se sert d’une variable globale et la manipule.

Cela est possible en Python. Pour faire cela, il suffit d’utiliser le mot clef __global__ devant le nom d’une variable globale utilisée localement afin d’indiquer à Python qu’on souhaite bien modifier le contenu de la variable globale et non pas créer une variable locale de même nom.
```python
x = 10
def portee():
    global x
    x= 5
>>> portee()
>>> print(x)
5
```

# Notion d'invariant de boucle
On qualifie un algorithme de **correct** s'il fait ce qu'on attend de lui : c'est à dire qu'il renvoie le résultat  escompté.

Ainsi un *invariant de boucle* sert, entre autres, à prouver la validité d'un algorithme comportant une ou plusieurs boucles `for` ou `while`.
Un invariant de boucle est une proposition qui : 
* est vraie avant d'entrer dans la boucle ( **initialisation**)
* reste vraie après une itération, si elle était vraie avant ( **conservation**)
* donne le résultat attendu en fin de boucle (**terminaison**)


# Conclusion 

On retiendra la nécessité des docstrings et la possibilité d'y insérer des tests afin de vérifier le programme.
On retiendra l'utilisation d' `assert` dans différents cas:  

* au sein de la fonction pour vérifier des préconditions
* au sein du programme, en dehors des fonctions,  pour éviter les erreurs

Le nombre de tests et leur qualité sont importants pour la mise au point d'un programme. Le succès d'un jeu de tests ne garantit pas pour autant un programme sans erreur.

Si, pour un algorithme donné, il existe un invariant, c'est à dire une hypothèse H qui vérifie l'**initiation**, la **conservation**, et la **terminaison**, alors on dit que l'algorithme est valide.

Sources :  
* NSI T. Balabonski S. Conchon JC. Filliâtre K. Nguyen  
* Le petit Python R Gomez  
* NSI C. Canu  
* https://www.pierre-giraud.com/python-apprendre-programmer-cours/portee-variable  

