---
title: "TP : Mise au point d'un programme "
subtitle: "Chapitre 11 Spécification et mise au point de programmes"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

# Exercice 1:
Donner une chaine de documentation à la fonction **division_euclidienne(a,b)** définie ci-dessous:


```python
def division_euclidienne(a,b): 
    q = 0
    r = a
    while r>= b :
        q = q + 1
        r = r - b
    return q,r

>>> division_euclidienne(23,4)
(5, 3)
```

# Exercice 2 :

1. Donner une chaîne de documentation pour une fonction **puissance(x,n)** qui calcule x à la puissance n pour deux entiers x et n.

```python
def puissance(x,n):
    valeur = 1
    for i in range(n):
        valeur = valeur*x
    return valeur
```

2. A l'aide d' `assert` réaliser des tests sur les préconditions au sein de la fonction.

```python
>>> puissance(3,4)
81
```

# Exercice 3 
Pour la fonction suivante, lui donner un meilleur nom, une chaîne de documentation, des tests en utilisant le module doctest et assert.

```python
def f(t):
    s = 0
    for i in range(len(t)):
        s = s + t[i]
    return s
```

```python
>>> f([1,3,6,8])
18
```

# Exercice 4
On prétend que la fonction suivante teste l'appartenance de la valeur v au tableau t.

```python
def appartient(v,t):
    i = 0
    while i < len(t)- 1 and t[i] !=v:
        i = i + 1
    return i < len(t)        
```

```python
>>> appartient(4, [1,3,4,8])
True
```

1. Réaliser la chaîne de documentation
2. Donner des tests pour cette fonction et montrer en particulier un test montrant qu'elle est incorrecte.
