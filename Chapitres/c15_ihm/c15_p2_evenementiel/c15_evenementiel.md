---
title: "Chapitre 15 : Interface Homme-Machine (IHM) sur le Web "
subtitle: "PARTIE 2 :  Programmation évenementielle"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Jusqu'à présent nous avons rencontré différents paradigmes de programmation (des manières de programmer) :

* __la programmation impérative__ : dans laquelle des séquences d'instructions s'enchainent
* __la programmation fonctionnelle__ : basée sur les fonctions
* __la programmation logique__ : basée sur des opérateurs et des règles de logique

Python est un langage multi-paradigmes car on a pu utilisé ces derniers en les combinant.
Un autre paradigme couramment utilisé est la __programmation évenementielle__ que nous allons présenter en utilisant le langage Javascript.

# Evénements

Javascript est très souvent utilisé sur la machine du client web (la vôtre, _à distinguer de celle du serveur_) pour rendre la page interactive en réponse aux actions de l'utilisateur :

* un clic sur un élément
* un survol d'un élément
* un appui sur une touche du clavier etc...

> 📢 A chaque __évenement__ est associé un __gestionnaire d'événement__ : en général une fonction qui sera exécutée lorsque l'événement sera déclenché.

Regardons un exemple simple : suivez lien pour accéder à JS Fiddle (un service en ligne permettant de tester des extraits de code HTML, CSS et JS)

https://urlz.fr/fgPT

Remarques :

- le code HTML se limite au contenu de l'élément body : ici un élément `button`
- les liens vers les fichiers css et javascript sont gérés par l'interface : ils n'apparaissent pas

Intéressons-nous au code Javascript :

```javascript
let btn = document.querySelector('button');  
// Permet de récupérer l'élément button présent sur la page 

btn.addEventListener('click', popup);  
// met en place une fonction à appeler à chaque clic sur l'élément 

function popup(){   
	window.alert('vous avez cliqué sur le bouton ! ')
}
```

La mise en place d'événement est basée sur la stratégie suivante :

* on affecte à une variable un élément de la page
* on abonne une fonction à un élement pour un événement donné
* la fonction sera exécutée lorsque l'événement se produira sur l'élément ciblé.

Le contenu de la fonction est bien évidemment adaptable : regardez le deuxième exemple :
https://urlz.fr/fgPY


```javascript
function modif_div(){
	let div = document.querySelector('div');
    div.style.backgroundColor = 'blue';
    /* remarque les propriétés composées comme background-color s'écrivent comme backgroundColor en javascript : on parle de syntaxe Camel case*/
}
```

La fonction `modif_div` permet ici d'agir sur les propriétés de style de l'élément `div`

Il existe bien d'autres événements comme :

* `dblclick` : un double-clic
* `mouseover` : un survol
* `keypress` : un appui sur une touche
* `load` : un chargement (celui de la page par exemple)
* etc...

# Séparation des codes HTML, CSS et Javascript.

Il est préférable de faire une sépration claire des codes HTML, CSS et Javascript.
Comme on l'a précédemment vu : les scripts au langage JAvascript auront une extension de type `.js` .
Il suffit alors d'appeler le script juste avant la balise body fermante comme dans l'exemple ci-après :

```html
<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8"/>
    <title>exemple</title>
</head>
<body>
    <button>Cliquez-ici</button>
    <script src="script_exemple.js"></script>
</body>
</html>
```






