---
title: "Chapitre 19 Algorithmes Gloutons "
subtitle: "Thème 7: Algorithmiques"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---


# Problème d'optimisation.  

## Exemples de problèmes concrets.  

* Exemple du voyageur de commerce:  

C’est sous forme de jeu que William Rowan Hamilton a posé pour la première fois ce problème, dès 1859. Sous sa forme la plus classique, son énoncé est le suivant :  

« Un voyageur de commerce doit visiter une et une seule fois un nombre fini de villes et revenir à son point d’origine. Trouvez l’ordre de visite des villes qui minimise la distance totale parcourue par le voyageur »  

L'optimisation se fait ici sur un critère précis: __La distance parcourue doit être la plus courte.__  

Applications  

 Optimisation de parcours de distribution de marchandises ou de personnes.  
 Optimisation des parcours des machines dans une entreprise.  
 Optimisation d'un planning.  
 Optimisation des tracés électroniques sur une plaque de silice.  

* Exemple du rendu de monnaie:  

Dans ce type de problème, on dispose d'un système monétaire fixé par exemple en France, nous disposons des pièces suivantes 1, 2, 5 euros et des billets 5, 10, 20, 50, 100 et 200 euros.  
Nous faisons un achat dans un magasin par exemple de 38 euros et nous donnons un billet de 50 euros pour régler cette somme.  
Le principe de ce problème est que le vendeur doit nous rendre 12 euros en utilisant le moins de pièces ou billets.  

L'optimisation se fait ici sur un critère précis: __Le nombre de pièces ou billets rendus doit être le plus petit possible.__  

Application:  

 Optimisation d'une caisse d'un commerce mais aussi des distributeurs automatiques.  

* Exemple du remplissage du sac à dos:  

Un des énoncés possibles pour ce problème est le suivant:  
« Étant donnés plusieurs objets possédant chacun une masse et un prix et étant donnée une masse maximale supportée pour le sac, quels objets faut-il mettre dans le sac de manière à maximiser la valeur totale sans dépasser la masse maximale autorisée par le sac ? ».  

L'optimisation se fait ici sur : __La valeur totale du sac doit être la plus grande possible.__ (Une contrainte: ne pas dépasser la masse maximale.)  

Applications:  

 Dans les systèmes financiers où il faut maximiser les gains avec la contrainte d'une somme au départ en investissant sur des produits financiers.  
 Dans la découpe en industrie où il faut découper un maximum de pièces sur une plaque de surface donnée en minimisant les pertes.  
 Dans le chargement de cargaisons dans un avion, un bateau, une valise ...  

## Comment résoudre ces problèmes ?  

* La première information importante est que tous ces problèmes possèdent une solution optimale. Donc, ils sont solubles.  
* La première idée serait de faire une résolution __naïve__ qui consiste à étudier tous les cas possibles et de prendre, en fonction des critères imposés, la solution optimale.  

Prenons l'exemple du voyageur de commerce qu'on personnalise à notre situation.  
Un bus part de Gondecourt et doit réaliser un circuit passant une fois et une seule par les villes suivantes Allennes-les-marais, Carnin et Chemy avant de revenir à Gondecourt.  
On dispose du tableau de données représentant la distance en kilomètres entre les villes :  

|   |Gondecourt| Allènes-les-marais | Carnin | Chemy |  
|:----------:|:----------:|:----------:|:----------:|:----------:|  
|Gondecourt| X | 2,8 | 3,6 | 1,8 |  
| Allennes-les-marais| 2,8 | X | 2,5 | 4,1 |  
|Carnin| 3,6 | 2,5 | X | 4,4 |  
|Chemy| 1,8 | 4,1 | 4,4 | X |  

__Nous voudrions trouver le trajet qui est le plus court possible.__  

On dénombre les trajets:  

| | | | | | |  
|:----------:|:----------:|:----------:|:----------:|:----------:|:----------:|  
| Gond | Allennes-les-marais | Carnin | Chemy | Gond | 2,8 + 2,5 + 4,4 + 1,8 = 11,5 |  
| Gond | Allennes-les-marais | Chemy | Carnin | Gond | 2,8 + 4,1 + 4,4 + 3,6 = 14,9 |  
| Gond | Chemy | Allennes-les-marais | Carnin | Gond | 1,8 + 4,1 + 2,5 + 3,6 = 12 |  
| Gond | Chemy | Carnin | Allennes-les-marais | Gond | 1.8 + 4,4 + 2,5 + 2,8 = 11,5 |  
| Gond | Carnin | Allennes-les-marais | Chemy | Gond | 3,6 + 2,5 + 4,1 + 1,8 = 12 |  
| Gond | Carnin | Chemy | Allennes-les-marais | Gond | 3,6 + 4,4 + 4,1 + 2,8 = 14,9 |  

Nous pouvons observer que les parcours 4, 5 et 6 sont identiques aux parcours 1,2 et 3. On ne prendra donc pour notre étude que les parcours 1, 2 et 3.  
D'après les calculs, nous observons que le parcours 1 est le plus court.  

La résolution de ce problème est donc simple car il y a peu de villes dans le trajet et donc peu de possibilités de trajets.  
Une résolution naïve qui dénombre l'ensemble des possibilités est donc possible.  

* Que se passe t-il si le nombre de villes dans le parcours augmente ?  

En mathématique, on montre que le nombre de trajets possibles se calcule avec la formule:  

nombre de trajets = $`\frac{(nombre de villes-1)!}{2}`$  

(En ne passant qu'une fois dans chaque ville)  

Exemple:  
Nous avions 4 villes (Gondecourt, Allennes-les-marais, Chemy et Carnin) pour trouver le nombre de trajets, il faut faire (4-1)! / 2  
Le "!" veut dire factoriel soit 3! = 3x2x1 = 6  
nombre de trajets = 6 / 2 = 3 C'est ce que nous avions.  

Que se passe-t-il si nous avons un nombre plus grand de villes ?  

* Pour 7 villes : nombre de trajets = (6x5x4x3x2) / 2 = 360  

* Pour 11 villes : nombre de trajets = (10x9x8x7x6x5x4x3x2) / 2 = 1 814 400  

* Pour 16 villes : nombre de trajets = (15x14x13x12x11x10x9x8x7x6x5x4x3x2) / 2 = $`6.5*10^{11`}`$  

On voit donc que le nombre de trajets augmentent très rapidement et la résolution naïve devient impossible même avec l'aide d'ordinateurs.  

# L'algorithme glouton.  

## Quand utiliser un algorithme glouton ?  

Les algorithmes gloutons sont utilisés pour répondre à des problèmes d'optimisation lorsque la résolution naïve n'est plus possible. Ces algorithmes vont donc avoir pour objectif de trouver une bonne solution sans que celle ci soit la meilleure possible.

Nous appliquerons les algorithmes gloutons dans les cas suivants:  

* un problème possédant un grand nombre de solutions.  
* Il est possible d'attribuer une valeur à chaque solution.  
* On recherche une solution qui soit bonne. Dans certains cas elle sera la meilleure.  

## Qu'est ce que l'algorithme glouton ?  

__Un algorithme glouton est un algorithme qui suit le principe de faire, étape par étape, un choix local qu'on suppose optimal, dans l'espoir d'obtenir un résultat optimum global. Le choix local est définitif et identique à chaque étape.__  

Remarque: Comme le choix local n'aboutit pas forcement à la meilleure solution globale, on parle d'heuristique gloutonne.  

### Retour sur l'exemple du voyageur de commerce.  

Dans l'exemple du voyageur de commerce, on ne peut plus appliquer la résolution naïve lorsque le nombre de villes devient trop important. On ne peut donc pas avoir une vision globale du problème. On choisit donc d'appliquer une règle entre chaque changement de ville. Comme on cherche le trajet le plus court, on choisira par exemple localement la distance la plus courte entre chaque ville.

|   |Gondecourt| Allennes-les-marais | Carnin | Chemy |  
|:----------:|:----------:|:----------:|:----------:|:----------:|  
|Gondecourt| X | 2,8 | 3,6 | 1,8 |  
| Allennes-les-marais| 2,8 | X | 2,5 | 4,1 |  
|Carnin| 3,6 | 2,5 | X | 4,4 |  
|Chemy| 1,8 | 4,1 | 4,4 | X |  

Si on reprend le tableau de données, on voit que partant de Gondecourt, on a Chemy (1,8 distance la plus courte depuis Gondecourt), puis partant de Chemy, on a Allennes-les-marais (4,1 plus courte hormis Gondecourt) puis Carnin (2,5) pour revenir à Gondecourt (3,6).  
Au total, le parcours sera de 12 km.  

On voit que notre choix local qui est d'aller, à chaque fois, à la ville la plus proche ne conduit pas à la solution globale optimale. Le choix d'aller à chaque étape à la ville la plus proche est donc une heuristique gloutonne.

## Le rendu de monnaie.  

On choisit le système monétaire euro sans les centimes soit 1, 2, 5, 10, 20, 50, 100 et 200. On ne fera pas de différence entre les pièces et les billets. (Toutes ces valeurs seront supposées être des pièces pour faciliter les explications.)  
Soit "Jean" un client qui achète un article pour 38 euros. Il donne à la caissière une pièce de 50 euros.  
__On veut que la caissière rende le moins de pièces possible au client.__

On décompose localement le problème comme pour le voyageur de commerce et on choisit donc comme règle locale de donner à chaque fois une pièce qui a la plus grande valeur possible mais qui est inférieure ou égale à la valeur à rendre.  
Par exemple:  

* Première étape, la caissière doit rendre 12 euros, elle choisit donc de rendre une pièce de 10 euros car c'est la valeur la plus grande en dessous de 12.  
* Deuxième étape, la caissière doit rendre 2 euros, elle choisit donc de rendre une pièce de 2 euros.  

Au total, la caissière aura rendu deux pièces.  

Pour cet exemple du rendu de monnaie, dans le système monétaire de l'euro, l'algorithme glouton fournira toujours la solution globale optimale.  

Remarque: Si on change de système monétaire, l'algorithme glouton ne conduira pas forcément vers la solution globale optimale.  
Le système 1, 6, 10 ne conduira pas à la solution optimale si on doit rendre 12. On choisira la valeur la plus grande 10 puis 2x1 alors que la solution globale optimale est 2x6.  

# Ce qu'il faut retenir:  

* On choisit d'utiliser un algorithme glouton dans des problèmes d'optimisation lorsque le dénombrement des possibilités conduit à des calculs trop importants.  
* On change d'approche. On ne voit plus le problème dans sa globalité. On recherche une solution locale qui nous semble optimale. Cette solution sera appliquée à chaque étape.  
* Comme la solution locale ne conduit pas systématiquement à une solution globale __optimale__, on parle d'heuristique gloutonne.  

