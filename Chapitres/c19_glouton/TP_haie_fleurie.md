---
title: "Chapitre Algorithmes Gloutons "
subtitle: "Exercice"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

# Contexte
Une collectivité d'agglomérations souhaite reboiser à l'aide d'arbustes fleuris des surfaces vides pour améliorer l'assimilation du dioxyde de carbone dans les communes. Lors des discussions du choix des arbustes, une contrainte s'est imposée : il faut mélanger des __arbustes persistants et des arbustes à feuilles caduques__ pour que cela soit joli toute l'année.  
Enfin la collectivité souhaite engager le __moins d'argent possible__ dans ce reboisement.  

![](./img/cotinus.jpg) ![](./img/photinia.jpg) ![](./img/ceanothe.jpg) ![](./img/oranger.jpg)  

Un pépiniériste propose son catalogue d'arbustes :  

```python
Arbustes= [ {"nom": "Berberis thunbergii 'Atropurpurea'", "feuillage" :"caduque" , "couleur" : "pourpre","hauteur" :1.8 , "largeur" : 1.2, "prix":13.95 , "rusticité" :-15 } ,
            {"nom" : "Cotinus coggygria varié", "feuillage" :"caduque" ,"couleur" :"pourpre" ,"hauteur" :3 , "largeur" : 2.0, "prix":21.95, "rusticité" : -15 } ,
            {"nom" : "Leucothoe axillaris 'Twisting Red'", "feuillage" :"persistant" , "couleur" :"panaché" ,"hauteur" :0.8 , "largeur" : 0.8,"prix": 26.99, "rusticité" :-15 } ,
            {"nom" : "Noisetier 'Maxima Purpurea'", "feuillage" :"caduque" , "couleur" :"pourpre" ,"hauteur" :3.5 , "largeur" : 1.2,"prix":24.99 , "rusticité" : -15} ,
            {"nom" : "Physocarpe opulifolius Diabolo", "feuillage" :"caduque" , "couleur" :"pourpre" ,"hauteur" :1.5 , "largeur" : 1.5,"prix": 22.4, "rusticité" :-20 } ,
            {"nom" : "Spirée nipponica Snowmound", "feuillage" :"caduque" , "couleur" :"vert" ,"hauteur" :2.0, "largeur" : 1.1,"prix": 20.5, "rusticité" :-20 } ,
            {"nom" : "Deutzia gracilis", "feuillage" :"caduque" , "couleur" :"vert"  ,"hauteur" :0.7 , "largeur" : 0.7,"prix":17.99 , "rusticité" : -15} ,
            {"nom" : "Weigela x ' Kosteriana Variegata '", "feuillage" :"caduque" , "couleur" :"vert"  ,"hauteur" :1.5 , "largeur" : 1.5, "prix":15.99 , "rusticité" :-15 } ,
            {"nom" : "Viburnum plicatum", "feuillage" :"caduque" , "couleur" :"vert"  ,"hauteur" :3.0 , "largeur" : 1.5,"prix":19.95 , "rusticité" :-15 } ,
            {"nom" : "Photinia x fraseri 'Pink Marble'", "feuillage" :"persistant" , "couleur" :"panaché"  ,"hauteur" :2.0 , "largeur" : 1.5,"prix":21.99 , "rusticité" :-15 } ,
            {"nom" : "Oranger du Mexique ternata Sundance® 'Lich'", "feuillage" :"persistant" , "couleur" :"vert" ,"hauteur" :2.0 , "largeur" : 1.0,"prix":23.8 , "rusticité" :-15 } ,
            {"nom" : "Callistemon Laevis", "feuillage" :"persistant" , "couleur" :"vert" ,"hauteur" :2.0 , "largeur" : 1.5,"prix":23.99 , "rusticité" :-5 } ,
            {"nom" : "Bambou sacré domestica Obsessed® 'Seika' ", "feuillage" :"persistant" , "couleur" :"panaché" ,"hauteur" :1.8 , "largeur" : 1.0,"prix":26.20 , "rusticité" :-5 } ,
            {"nom" : "Hibiscus syriacus ", "feuillage" :"caduque" , "couleur" :"vert" ,"hauteur" :2.5 , "largeur" : 2.0,"prix":19.95 , "rusticité" :-15 } ,
            {"nom" : "Buddleia Davidii", "feuillage" :"caduque" , "couleur" :"vert" ,"hauteur" :1.8 , "largeur" : 1.2,"prix":9.95 , "rusticité" :-20 } ,
            {"nom" : "Abelia x 'Edward Goucher' ", "feuillage" :"caduque" , "couleur" :"vert" ,"hauteur" :1.3 , "largeur" : 0.9,"prix":15.95 , "rusticité" :-12 } ,
            {"nom" : "Gardenia Kleims Hardy ", "feuillage" :"caduque" , "couleur" :"vert" ,"hauteur" :1.0 , "largeur" : 1.0,"prix":22.95 , "rusticité" :-5 } ,
            {"nom" : "Weigela florida Alexandra ", "feuillage" :"caduque" , "couleur" :"pourpre" ,"hauteur" :2.0, "largeur" : 1.0,"prix":22.20 , "rusticité" :-20 } ,
            {"nom" : "Ceanothus thyrsiflorus ' Skylark ' ", "feuillage" :"caduque" , "couleur" :"pourpre" ,"hauteur" :1.5 , "largeur" : 1.5,"prix":13.90 , "rusticité" :-10 }
         
]
```
Comment choisir les arbustes ?

# Prédicats de classement

La liste des arbustes est  une liste de dictionnaires ayant les mémes clés :  
* nom  (type str)
* feuillage (type str)
* couleur  (type str)
* prix  (type float)
* rusticité (type int)

Ecrire en python des prédicats s'appliquant à un arbuste ( donc à un dictionnaire) pour tester si celui-ci :  
* est_vert(arbuste)
* est_pourpre(arbuste)
* est_caduque(arbuste)
* est_persistant(arbuste)
* est_rustique(arbuste, température) avec l'argument température signifiant la température minimale admissible pour le lieu de la collectivité

# Sélections 
La collectivité souhaite sélectionner un nombre d'arbustes ayant les __critères des prédicats précédents__.
Ecrire ainsi en python par exemple __selection_vert(nombre, liste_etude,l)__ où l'argument `nombre` correspond au nombre d'arbustes différents souhaités, `liste_etude` sera la liste des arbustes disponibles et `l` la liste des arbustes selectionnée. Celle-ci peut être initiée à vide au départ et ne devra pas déjà contenir l'arbuste si il a été sélectinné auparavant.  

On écrira ainsi ( en utilsant les prédicats précédents!) :  

* selection_vert(nombre, liste_etude,l)  
* selection_pourpre(nombre, liste_etude,l) 
* selection_persistant(nombre, liste_etude,l)
* selection_caduque(nombre, liste_etude,l)

On écrire enfin __selection_rusticité(list_arb, temperature)__ permettant de sélectionner une liste d'arbustes dont la température minimale est donnée.  

# Tri
Afin de trier une liste selon une catégrie, on utilisera la fonction `sorted()` : La fonction sorted() est une fonction en python qui renvoie une liste triée. _Elle prend en premier paramètre le nom de la liste à trier, en deuxième paramètre une clé. Cette clé aura pour affectation une fonction anonyme "lambda" qui permettra de fixer par rapport à quoi nous souhaitons trier la liste. Enfin, le troisième paramètre "reverse" est par défaut à False. Si on préfère trier par ordre décroissant, vous mettrez reverse = True._

```python
sorted(liste , key = lambda element : element[categorie] )
```

Ecrire donc une fonction de tri nommée __tri(liste_arb,categorie, reverse)__ renvoyant la liste triée selon la catégorie( clé de dictionnaire ici) choisie.

```python
>>> tri(Arbustes,"prix", False)
[{'nom': 'Buddleia Davidii', 'feuillage': 'caduque', 'couleur': 'vert', 'prix': 9.95, 'rusticité': -20}, {'nom': "Ceanothus thyrsiflorus ' Skylark ' ", 'feuil..........
```

# Retour des résultats

La collectivité aimerait obtenir une liste d'arbustes avec uniquement le prix total de la sélection.
* Ecrire une fonction __donne_liste_arbres()__ qui renvoie une liste contenant le nom des arbustes uniquement  
* Ecrire une fonction __prix_selection(liste)__ qui renvoie le prix total de la sélection.

# Faire son choix !

La collectivité d'agglomérations demande un devis pour 6 arbustes différents en tout. Ce lot de base sera multiplié pour réaliser des longueurs de haies différentes.

## Choix 1

Un devis est demandé pour 2 arbustes persistants, 2 arbustes à feuillage vert et 2 à feuillages pourpre. Les six arbustes doivent être différents et au prix minimum.
Ecrire la fonction __choix_1(list_arb)__ renvoyant un tuple contenant la liste d'arbres sélectionné ainsi que le prix.

```python
>>> choix_1(Arbustes)
(99.54, ["Photinia x fraseri 'Pink Marble'", "Oranger du Mexique ternata Sundance® 'Lich'", "Ceanothus thyrsiflorus ' Skylark ' ", "Berberis thunbergii 'Atropurpurea'", 'Buddleia Davidii', "Abelia x 'Edward Goucher' "])
```

Modifier l'ordre des critères entre persistant feuillage vert ou pourpre : la réponse est-elle identique?

## Choix 2

Les agglomérations sont situées dans le Nord de la France : un autre devis est demandé pour s'assurer que les arbustes supportant -15°C seront sélectionnés. 
Ecrire cette fonction __choix_2(list_arb)__. 

```python
>>> choix_2(Arbustes, -15)
(107.63, ["Photinia x fraseri 'Pink Marble'", "Oranger du Mexique ternata Sundance® 'Lich'", "Berberis thunbergii 'Atropurpurea'", 'Cotinus coggygria varié', 'Buddleia Davidii', "Weigela x ' Kosteriana Variegata '"])
```


