---
title: "Chapitre 16_2 : Reseaux "
subtitle : "TP1 :  Simulations Réseaux et tranmission des données "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n°1: Découverte de Filius  

[Filius](https://www.lernsoftware-filius.de/Herunterladen) est un logiciel pédagogique pour aider à la compréhension et à l’apprentissage des concepts des réseaux informatiques.  

![Image logiciel Filius](./fig/filius1.png)  

On y trouve :  

* _une zone centrale blanche_ où l’on construira le réseau 
* _sur la gauche_  : les différents composants du réseau 
* _en haut_ : des boutons pour charger ou enregistrer un fichier, des boutons de commande pour la simulation (le bouton play) ou la construction d’un réseau (le marteau) et enfin un glisseur pour contrôler la vitesse des simulations.

__1) On commence par créer un réseau élémentaire avec deux ordinateurs de bureau reliés par un cable__

* Cliquez/ glissez pour déposer un premier ordinateur dans la zone centrale
* Faites un double click sur lui pour observer en bas ses caractéristiques réseau
* Affectez l'adresse IP __192.168.0.10__ au premier ordinateur (en changeant éventuellement la valeur dans `Adresse IP`) puis cochez la case `Utiliser l'adresse IP comme nom`
* Faites la même chose avec un autre ordinateur d'IP: __192.168.0.11__
* Reliez-les par l'intermédiaire d'un cable

__2)__ Appuyez sur le bouton "play". Vous passez en mode simulation.  

* Double-cliquer sur l'ordinateur `192.168.0.10 `
* Cliquer sur l'icône `Installation des logiciels`
* Cliquer sur `ligne de commandes` et la fleche verte gauche pour mettre l'instruction dans `installés`.
* Cliquer sur `Appliquer les modifications`.
* Exécuter alors l'application `ligne de commandes` en double cliquant dessus.

Une invite de commandes vous permet d'utiliser quelques commandes sur cet ordinateur du réseau.
A tout moment vous pouver utiliser la commande 
```bash
help
```
pour accéder aux commandes disponibles

__3)__ Testons la connexion avec l'autre ordinateur

```bash
ping 192.168.0.11
```
La commande `ping` permet d'envoyer des paquets de données vers la machine dont on indique l'IP.

* Combien de paquets ont été envoyés?  ............. 
 
On voit la caractéristique `ttl` qui veut dire __Time To Live__  :ce nombre décroit de une unité à chaque fois qu'il rencontre un routeur. Le TTL initial est fixé pour tous à 64.  

* Combien de routeurs a rencontré la paquet n°1 ?  ...................
* Sachant que lorsque TTL atteint la valeur de 0, le paquet est détruit. Combien de sauts peut faire le paquet?  .....

# Exercice n°2: Construction d'un réseau local.  

A l'aide du logiciel Filius, construire un réseau local comprenant trois ordinateurs, chacun relié à  un switch par un câble.  

* Affecter des adresses IP différentes à chaque ordinateur. (192.168.0.10 à 192.168.0.12)
  
* Passer en mode simulation

__1)__ Observer le réseau avant communication.  

* Cliquer sur le switch: Une boite de dialogue apparait avec la table de routage.

  --> Qu'observez-vous? .................................................  
  
* Cliquer bouton droit sur l'ordinateur d'adresse IP 192.168.0.11  

  --> Qu'observez-vous dans les échanges de données? .............

__2)__ A partir du poste d'IP 192.168.0.11, vous allez envoyer un ping sur les deux autres ordinateurs.  

- Qu'observez-vous dans la __table MAC__ du switch ? ........................................................
  
- Qu'observez-vous dans la table d'échanges de données de l'ordinateur d'IP 192.168.0.11.  
.................................................  

__3)__ Dans la table d'échanges de données, cliquer sur une requête de protocole ARP.  

- En lisant les commentaires, que pouvez-vous dire de l'intérêt de ce type de requête?  
.............................................

__4)__ En regardant l'ensemble des requêtes de protocole ICMP, on observe les requêtes effectuées par 192.168.0.10 et d'autres.

- Comment appelle-t-on ce type de transfert de données? .......................................................
- Comprenez-vous pourquoi on appelle cela un ping? ............................................ 
- Comment évolue le TTL des requêtes? Est-ce normal?..........................................................  


# Exercice n°3: Manipulation de l'invite de commandes de votre PC

Aller dans un terminal :

__1)__ Commençons par analyser la table ARP de votre ordinateur. 

```bash
arp -a
```

Qu'observez-vous? ................................................................  

__2)__ Nous allons envoyer un ping sur le site `facebook.fr `. Attention dans Linux il envoie indéfiniment donc Ctrl + C pour arreter ou l'option -c : ping -c 5 site

- Ecrivez la ligne de commande correspondante : .............................. 
  
- Quelle est l'adresse IP de Facebook? ........................
  
- Combien de routeurs ont été traversés? ........................ 

__3)__ On peut vérifier la dernière information en suivant les chemins des paquets de données à l'aide de ce [site web](https://gsuite.tools/fr/traceroute) par exemple.
Tester et visualiser le chemin pris par votre paquet.
Chaque routeur représente un point de connexion entre deux réseaux, au travers duquel le paquet de données a été transmis.

# Exercice n°4: Manipulation d'un réseau avec routeurs.  

Téléchargez le fichier [`reseau-de-reseaux.fls`](./reseau-de-reseaux.fls) est ouvrez-le depuis Filius.  

__1)__ Passer en mode simulation. Analyser la table ARP de l'ordinateur 192.168.0.10 et la table MAC du switch 0.  
  
* Par qui ces requêtes ont-elles été émises ? ............................................
* Vérifiez cette information sur les autres réseaux locaux.  

__2)__ Vous allez maintenant lancer un ping depuis l'ordinateur 192.168.0.11 vers un ordinateur d'un autre réseau local. (192.168.1.11 par exemple)  

* Observez vous un découpage en paquets? ...................
* Si oui combien? ..........................
* Observez le Time To Live des paquets? ................................



