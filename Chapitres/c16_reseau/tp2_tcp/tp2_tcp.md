---
title: "Chapitre 16_2 : Reseaux "
subtitle : "TP2 : Protocole TCP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


Dans ce TP nous allons utiliser FILIUS pour simuler les requêtes client-serveur et examiner les paquets TCP transmis

# Construction de l'Architecture client-serveur

__1)__ Commencer par ajouter :

* un ordinateur d'adresse IP 192.168.1.10 nommé `client`
* un ordinateur d'adresse IP 192.168.1.11 nommé `serveur`
* un Switch reliant les deux

__2)__ Passer en mode simulation :

* Installer l'application `Serveur web` sur le serveur puis lancez-le (Double clic puis Démarrer)
* Installer l'application `Navigateur web` sur le client puis lancez-le et indiquez l'IP du serveur pour vous y connecter

# Analyse des échanges de données

Visualiser sur l'écran du serveur les requêtes et les réponses associées :

__3)__ Combien observez vous de requetes et de réponses (justifiez), quelles sont les méthodes utilisées ?


__4)__ Affichez les échanges de données sur l'ordinateur client

__a-__ A quoi correspond les 2 premières ?  Justifier leur intérêt


__b-__ Expliquez les trois paquets TCP suivants et notamment justifier précisément tous  les numéros de séquence transmis:


__5)__ Observez la première requête GET du client : double cliquez dessus ( du haut vers le bas vous observerez l'encapsulation des données)

![exemple encapsulation](./fig/encapsulation.jpg)

__a-__ Combien de couches d'encapsulation sont ici décrites ?


Le modèle OSI décrit en cours est un modèle théorique dans la pratique, la conseption des réseaux s'appuie sur un modèle plus simple le __modèle TCP/IP__ [^1]

![modèles OSI et TCP/IP](./fig/modele-osi-vs-tcp.png)

__b-__ Dans quelle couche se situe les données de la requête HTTP ? 


__c-__ Quel est le port utilisé pour la communication par le navigateur (dans l'entête de quel paquet se situe cette information, citez également la couche concernée)?


__d-__ Dans l'entête de quel paquet, les adresses réseaux du client et du serveur ( citez également la couche concernée)


__e-__ Quelles données sont rajoutées à l'entête de la trame véhiculant ces informations ?


__6)__ Comparez ce paquet au paquet TCP suivant. Expliquer leur point commun et leur différence


# Segmentation des données transmises

On s'intéresse maintenant à l'envoi de la réponse du serveur concernant l'image. Elle commence normalement par ce paquet :

![reponse serveur image](./fig/reponse_image.jpg)

__7)__ En combien de paquets TCP a été découpée cette image ?


__8)__ Expliquez précisément l'alternance des valeurs des sequences SEQ envoyées par le serveur et des ACK envoyées par le client.

__9)__ Expliquer ce qui se passe pour la fermeture de la connexion TCP


[^1]: https://linux-note.com/modele-osi-et-tcpip/