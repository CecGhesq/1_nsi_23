---
title: "Chapitre 16 : Réseaux : relation client serveur"
subtitle : "Cours : Construction d'un formulaire"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Les formulaires sont essentiels dans les sites web car ils permettent d'obtenir des données sur l'utilisateur du site et donc de personnaliser les pages par la suite. En entreprise par exemple, cela permet d'établir une base de données client ou fournisseur.  

Pour des raisons pratiques, vous copierez le dossier ["formulaire_lycee_eleve"](./Formulaire_lycee_eleve.7z) sur votre session de travail. Dézipper le fichier.  
Les fichiers HTML que vous devez compléter se trouvent dans le dossier "views". Ils ont une extension ".ejs" car c'est le moteur de template que nous utilisons pour le serveur créé avec nodeJS.  
Vous pourrez programmer vos pages en HTML et juste enregistrer. Vous devrez veiller à ce que l'extension soit toujours ".ejs".  

# Construction d'un formulaire.

* Vous avez un fichier HTML pré-rempli. Pour construire votre formulaire, vous devrez remplacer les zones de commentaires par les balises utiles. [index.ejs](./Formulaire_lycee_eleve/views/index.ejs)  

* Vous avez un fichier CSS lié au fichier HTML du formulaire. Il est rempli, vous pouvez le modifier pour personnaliser votre travail si vous le souhaitez lorsque l'ensemble du travail de la séance sera accompli. [style.css](./Formulaire_lycee_eleve/public/css/style.css)   

# Formulaire en HTML

__1)__ Utilisez les balises ci-dessous pour completer le fichier index.ejs afin d'obtenir un formulaire similaire à celui ci-dessous.  

- Dans la div qui porte l'id "titre", vous mettrez "Premier Formulaire" suivi de votre nom. (Cela permettra de reconnaitre votre formulaire.)  
- La methode sera de type get.  
- La redirection se fera vers "/index1.ejs".  

![Page web du formulaire](./fig/formulaire.png)  

## Balise form:  

| Balise | Attributs | Description |  
|:-----------:|:------------------------:|:----------------------------:|  
| __`<form>`__ |             | Balise qui crée un formulaire. On utilisera la balise __`</form>`__ pour clore le formulaire. |  
|              | action = "adresse page web" | L'attribut action permet de fixer la redirection lors de la validation du formulaire. |  
|              | method = "get ou post" | La méthode fixe le type de requête HTTP. Chaque type de requête a ses avantages et ses inconvénients. |  


## Champ texte:  

| Balise | Attributs | Description |  
|:-----------:|:------------------------:|:----------------------------:|  
| __`<label>`__ |             | Balise qui permet de mettre une étiquette à un champ texte. (ou autre) |
|               | for = "nom" | Attribut qui permet de lier de manière formelle la balise label avec la balise input. |  
| __`<input>`__ | type = "text" | Attribut qui définit le type de la balise input. Ici ce sera une balise de champ texte. |  
|           | name = "nom"  | Donne un nom à la balise. Cela permet de relier de façon formelle la balise input avec la balise label. |  
|           | id = "identifiant" | Attribut qui donne un identifiant unique à la balise input. Cela permettra de faire des liens avec les fichiers CSS et javascript. |  
|           | value = "valeur" | Fixe une valeur à la balise input. Le texte entre guillements sera visible. |  

Exemple:  
```html
    <label for = "prenom"> Prénom: </label>  
    <input id="prenom" name ="prenom" type="text" value="prénom"/>  
```  

Cela donne ceci lorsque le code HTML est interprété par un navigateur:   

Prénom :  ![](./fig/input.jpg)  


## Bouton radio:  

| Balise | Attributs | Description |  
|:-----------:|:------------------------:|:----------------------------:|
| __`<input>`__ | type = "radio" | Attribut qui définit le type de la balise input. Ici ce sera une balise de type bouton radio. |  

Exemple:  
``` 
    <label for = "pere"> Père :</label>  
    <input id="pere" name ="pere" type="radio" value="pere"/>  
```  
Cela donne ceci lorsque le code HTML est interprété par un navigateur:  
Père :  ![](./fig/pere.jpg)  

## Liste déroulante:  

| Balise | Attributs | Description |  
|:-----------:|:------------------------:|:----------------------------:|  
| __`<select>`__ |             | Balise qui permet de créer une liste déroulante. On couple cette balise avec la balise option. |  
| __`<option>`__ |             | On doit définir autant de balises option qu'il y a de choix dans la liste déroulante. |  

Exemple:  
``` 
    <label for = "annee"> Année </label>  
    <select id = "annee" name ="annee" value="2018">
        <option>2019</option>
        <option>2020</option>
    </select>  
``` 
Donne: ![](./fig/annee.jpg)  
  

__2)__ Ouvrir le fichier index1.ejs et compléter le fichier afin d'obtenir un formulaire identique au précédent.  

- Dans la div qui porte l'id "titre", vous mettrez "Deuxième Formulaire" suivi de votre nom. (Cela permettra de reconnaitre votre formulaire.)  
- La methode sera de type post pour ce formulaire.  
- La redirection se fera vers "/index2.ejs".  

Maintenant, vous devez avoir trois pages HTML (avec l'extension .ejs):  
- index.ejs  
- index1.ejs  
- index2.ejs.  

# Communication entre ordinateurs: modèle client-serveur.  

## Le modèle OSI.  

Nous n'allons pas détailler le modèle OSI (Open Systems Interconnections) dans ce chapitre mais juste commenter l'illustration ci-dessous afin de comprendre où se situe le modèle client-serveur dans la communication entre ordinateurs.  

![Modèle OSI](./fig/osi.png)  

- Le modèle OSI est un __modèle théorique__ qui représente la communication entre ordinateurs.  
- Il est composé de 7 couches. Chaque couche a un rôle précis dans la communication.  

Le modèle client-serveur que nous allons étudier dans ce chapitre se situe au niveau de la couche 7: Application.  

Pour information, l'adressage IP se situe dans la couche 3 (Réseau), le protocole TCP ainsi que l'attribution des ports se situe dans la couche 4 (Transport) et l'interprétation du langage HTML se situe dans la couche 6 (Présentation).

## Le protocole http ou https  

- Les échanges de données entre ordinateurs ont été conçus suivant le modèle client-serveur car dans la plupart des cas, un ordinateur (client) souhaite avoir des ressources situées sur le disque dur d'un autre ordinateur (serveur). (La situation n'est pas symétrique donc on différencie le rôle de chaque ordinateur.)  

- Comme dans tout échange, il faut fixer des règles de communication, c'est le rôle du protocole http.  

- Le protocole http (Hyper Text Transfert Protocol) est un ensemble de règles qui permet au client d'effectuer des __requêtes__ au serveur web. En retour le serveur web va envoyer une réponse en relation avec la requête.  

Par exemple, lorsque vous tapez "https://www.google.fr", vous utilisez le protocole https (version sécurisée de http), vous allez sur le web (www) et vous faites une requête au serveur qui porte le nom de domaine "google". (Ce nom de domaine correspond en réalité à une adresse IP : serveur DNS). En retour, le serveur qui porte le nom de domaine "google" repond en envoyant un fichier HTML. (Accompagner d'un fichier CSS et autres) 

## Les méthodes de requête associées au protocole http.  

Les méthodes de requêtes indiquent les actions que l'on souhaite réaliser sur la ressource indiquée. Nous allons détailler deux méthodes de requêtes:  

- La méthode GET: C'est la méthode la plus courante pour demander une ressource. Elle est sans effet sur la ressource. Les requêtes GET doivent uniquement être utilisées afin de récupérer des données. (Récupérer un fichier HTML, un fichier CSS, une image ...)  

- La méthode POST: Cette méthode est utilisée pour soumettre des données en vue d'un traitement (côté serveur). Typiquement c'est la méthode employée lorsque l'on envoie au serveur les données issues d'un formulaire.  

## Utilisation de vos formulaires pour comprendre ces méthodes:  

**1) Connaitre son adresse IP** : Démarrer un terminal et taper la commande `ìp address`  

**2) Demarrer le serveur web:**  

- Vous allez ouvrir visual studio codium et faire glisser le dossier `Formulaire_lycee_eleve`.
- A l'aide du raccourci clavier `CTRL + J` vous ouvrez un terminal  
- Vous tapez la commande "npm run start"   

Normalement, vous allez voir ceci:  

![Invite de commande](./fig/invitecommande.png)  

**3) Réaliser une requête sur le serveur web de votre voisin.**  

- Demander à votre voisin (ou autre) l'adresse IP de son ordinateur. Elle figure sur le bureau de son ordinateur. (Par exemple 192.168.1.16)  

- Ouvrir le navigateur Mozilla Firefox.

- Dans la barre de navigation, tapez l'adresse IP de votre voisin suivie de ":3000" c'est le port d'écoute qu'on utilisera ici.  
Par exemple 192.168.1.16:3000.  

Vous devriez voir le formulaire créé par la personne que vous avez choisi.  

- Tapez ensuite sur la touche F12 afin d'ouvrir le "Débogueur" de Mozilla.  

- Appuyer sur l'onglet "Réseau" puis sur le chronomètre.  

- Vous verrez des graphiques apparaitre, cliquer sur le premier graphique.  

- Vous observez les requêtes des images. Pour voir l'ensemble des requêtes, il faut sélectionner l'onglet "tout".  

Vous devriez voir ceci apparaitre en bas de votre page web:  

![Débogueur](./fig/debogueur.png)  

## Analyse des requêtes http.  

* Quelle est la méthode utilisée pour chaque requête? Est-ce judicieux d'utiliser cette méthode?  

---------------------------------------------------------------------------------------------  

Voici un tableau récapitulant les états des reponses des requêtes (Cette liste reprend les principales réponses du serveur lorsqu'il reçoit une requête du client):  
| Code | Message | Signification |  
|:------------:|:------------:|:-----------:|
| 200 | OK | Requête traitée avec succès |
| 304 | Not modified | Document non modifié depuis la dernière requête. ( La ressource avait déjà été mise dans le cache)|  
| 404 | Not found | Ressource non trouvée. |  

* Quels sont les états des reponses aux requêtes réalisé sur le serveur web de votre voisin?  

---------------------------------------------------------------------------------------------  

* Quel est le domaine sollicité?  

---------------------------------------------------------------------------------------------  

**Vous allez maintenant compléter le "Premier formulaire" puis vous appuyez sur le bouton suivant.**  

* Regardez dans la barre de navigation. Qu'observez vous dans l'URL?  

---------------------------------------------------------------------------------------------  

* Dans le débogueur cliquez sur la première requête http en partant du haut et regardez à droite dans "En têtes".  
    * Quelle est l'URL de la requête?  

    * Quelle est la méthode de la requête?  

    * Expliquez en quoi la méthode de requête n'est pas bonne pour un formulaire?  

Si vous voulez observez ce qui a été envoyé du côté serveur, il faut regarder dans l'invite de commandes de votre voisin.  

**Vous allez maintenant compléter le "Deuxième formulaire" puis vous appuyez sur suivant.**  

* Regardez dans la barre de navigation. Comparer l'URL de la méthode POST avec celle de la méthode GET?  

---------------------------------------------------------------------------------------------  

* Dans le débogueur cliquez sur la première requête http en partant du haut et regardez à droite dans "En têtes".  
    * Quelle est l'URL de la requête?  

    * Quelle est la méthode de la requête?  

    * Expliquez pourquoi la méthode POST est recommandée pour l'envoi d'un formulaire?  


* Cliquez sur l'onglet "paramètres". Que constatez-vous?  
  
---------------------------------------------------------------------------------------------  

* Peut-on dire que les données sont sécurisées?  

---------------------------------------------------------------------------------------------  

## Analyse des requêtes https.  

Le protocole https a la même fonction que le protocole http mais les données lors des transferts sont cryptées.  
Pour vous en assurer, loggez vous sur votre compte ENT en laissant le débogueur Mozilla ouvert.  
Normalement, vous allez avoir une requête POST liée au formulaire de connexion. Cliquez sur cette requête puis observez les paramètres.  

* Que constatez vous?  

---------------------------------------------------------------------------------------------  


**En résumé:**  

- "http" est un protocole lié à la septième couche du modèle OSI. Il définit les règles de communication entre les ordinateurs au niveau d'une relation client-serveur.  
  
- Pour cela, il s'appuie sur des méthodes de requêtes. La méthode GET est utilisée principalement pour accéder à des données sans les modifier alors que la méthode POST est utilisée pour envoyer des données via un formulaire.  

- Si un formulaire est envoyé vers un serveur, avec la méthode GET les données se trouveront dans l'URL alors qu'avec la méthode POST, elles seront dans le corps de la requête.  
  
- Pour les deux méthodes de requête, les données ne sont pas sécurisées, il faut utiliser le protocole https pour crypter les données transférées.  
  
