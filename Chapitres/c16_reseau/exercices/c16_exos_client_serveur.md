---
title: "Chapitre 16 : Reseaux"
subtitle : "Partie 1 - Exercices :  Relation client/serveur sur le Web"
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---

# Exercice 1 : QCM &#x1F3C6;

__Question 1__   Quelle est la machine qui va exécuter le script suivant inclus dans le code HTML d'une page?

```javascript
document.querySelector('p').addEventListener('click', survol);
function survol(){
    this.style.color="red";
}
```

* [ ] La machine de l'utilisateur ou du serveur selon qui est la plus disponible   
* [ ] La serveur web sur lequel est stocké la page html
* [ ] La machine de l'utilisateur sur laquelle s'exécute le navigateur web
* [ ] La machine de l'utilisateur ou du serveur selon la confidentialité des données à traiter  
   

__Question 2__ Quelle méthode d’envoi des paramètres est-il préférable d’utiliser, pour un formulaire d’une page web, destiné à demander à l’utilisateur un mot de passe pour se connecter (le protocole utilisé est HTTPS) ?

* [ ] La méthode CRYPT
* [ ] La méthode GET
* [ ] La méthode POST
* [ ] La méthode PASSWORD

__Question 3__ Un élément form d’une page html contient un élément button de type submit. Un clic sur ce bouton : 

* [ ] Ne sera possible que si le serveur fonctionne correctement.
* [ ] Envoie les données du formulaire vers la page définie par l'attribut method de l'élément form.
* [ ] Envoie les données du formulaire vers la page définie par l'attribut action de l'élément form.
* [ ] Ne fait rien si un événement click n'a pas été mis en place.

__Question 4__ Quel est l'intrus dans cette liste de solutions logicielles employées dans les serveurs: 

* [ ] Apache
* [ ] Nginx
* [ ] Linux
* [ ] Node.js

__Question 5__ Que peut-on déduire de la lecture de cet URL ? 
https://fr.wikipedia.org/w/index.php?search=web

* [ ] le serveur traite les données en javascript.
* [ ] La connexion n'est pas sécurisée.
* [ ] Le serveur est situé en France.
* [ ] le serveur n'a pas reçu une requête avec une méthode POST.


# Exercice 2 : Interpréter du code HTML &#x1F3C6;

Voici le code html [èxtrait d'une page du site https://developer.mozilla.org](https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/Evolution_of_HTTP)

```html
<h2 id="Linvention_du_World_Wide_Web">L'invention du World Wide Web</h2>

<p>En 1989, alors qu'il travaillait au CERN, Tim Berners-Lee proposa la création d'un système hypertexte sur internet. Initialement nommé <em>Mesh, </em>il prit le nom de World Wide Web lors de sa mise en place en 1990. Bâti sur les protocoles existants TCP et IP, il consistait en quatre éléments de base :</p>

<ul>
 <li>Un format textuel pour représenter les documents hypertextes, l'<em><a href="/fr/docs/Web/HTML">HyperText Markup Language</a></em> (HTML).</li>
 <li>Un protocole simple pour échanger ces documents, l'<em>HyperText Transfer Protocol </em>(HTTP).</li>
 <li>Un logiciel client pour exposer (et modifier) ces documents, le premier navigateur web nommé <em>WorldWideWeb</em>.</li>
 <li>Un serveur pour garantir l'accès au document, version initiale du <em>http</em>.</li>
</ul>
```

__1)__ De combien de liens hypertexte ce code est-il composé ?    
__2)__ A quoi sert la première ligne ?  
__3)__ Combien d'éléments html on été utilisés ?  


# Exercice 3 : Evenements et formulaire  &#x1F3C6;

On considère le code suivant :

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
	</head>

	<body>
        <h1 id="titre"> Formulaire </h1>
	    <form action = "/action.php" name="formulaire" id="formulaire" method = "post">
			<label for = "classe"> Classe: </label>
				<select id="classe" name = "classe" value="1.1">
					<option value='1.1'>1.1</option>
					<option value='1.2'>1.2</option>
					<option value='1.3'>1.3</option>
				</select>
			
			<br/>
			<input id="bouton" onclick="change_titre()" value="Valider"/>
		</form>
        <script>  
            function change_titre(){
                let h1 = document.getElementById("titre");
                h1.textContent = "Formulaire validé";
            }
        </script>
	</body>
</html>
```

__1)__ Décrire la page web fabriquée par ce code.    
__2)__ Quel événement est mis en place ici ? Sur quel élément ? Quel est son effet ?  
__3)__ La façon de mettre en place cet événement en javascript est-elle recommandée? Quels modifications proposeriez-vous (aucun code n'est demandé) ?  
__4)__ Le formulaire ne pourra pas être traité par le serveur, pourquoi ?



# Exercice 4: Requête HTTP  &#x1F3C6;

Ci-dessous une requête et sa réponse associée

```
GET /static/img/header-background.png HTTP/1.1
Host: developer.cdn.mozilla.net
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate, br
Referer: https://developer.mozilla.org/fr/docs/Glossary/Simple_header

200 OK
Age: 9578461
Cache-Control: public, max-age=315360000
Connection: keep-alive
Content-Length: 3077
Content-Type: image/png
Date: Thu, 31 Mar 2016 13:34:46 GMT
Last-Modified: Wed, 21 Oct 2015 18:27:50 GMT
Server: Apache

(contenu)
```

__1)__ Sur quel type de contenu porte la requête ? Justifier.  
__2)__ Quelle est la méthode utilisée pour cette requête ? Est-ce une méthode adaptée ?
__3)__ La requête a-t-elle aboutie ? Justifier.      
__4)__ Quel est le système d'exploitation utilisé par la machine client ?    
__5)__ Quel est le type de solutions logicielles utilisé par le serveur ?  
__6)__ Le protocole est-il sécurisé ? Justifier 






