---
title: "Chapitre 16 : Réseaux "
subtitle: "PARTIE 2 - Cours: Transmission de données dans un réseau."
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


> 📢 Nous parlons d'un __réseau__ lorsque au minimum deux ordinateurs peuvent échanger directement des informations. (Soit par un réseau cablé/fibré soit par un réseau wifi/lifi)  

# Le réseau local.  

## Eléments du réseau local 
  
Nous allons travailler sur une image d'un réseau local d'une habitation afin de trouver les différents éléments du réseau et leur fonction.  

![Réseau local](./fig/image_reseau_local.jpg)  

* Le __routeur__: Chez nous, le routeur est assimilé à la "box". Cet appareil a pour but de relier votre réseau local au monde extérieur c'est à dire au réseau Internet.  
Il partage une même connexion internet entre différents éléments du réseau local. Il établit donc des "routes" entre les éléments dans une maison ou entreprise et le monde extérieur.  

* Le __commutateur réseau__ (_ou_ __switch__): C'est un boitier comportant plusieurs ports ethernet. Il permet de relier en réseau local plusieurs éléments (ordinateurs ou autres).  
Le switch permet avant tout de répartir l’information de manière « intelligente » au sein du réseau local. Il reçoit des informations d'un émetteur et les envoie vers un destinaire précis. Il réalise cela grâce à une table: la __table MAC__.  

* Les __cables ethernet__: Le débit maximal est de 10 Gbit/s et la fréquence supportée peut aller jusqu'à 250 MHz.  

* Les __cartes réseaux__: les éléments qui sont connectés au switch ou au routeur possède tous une carte réseau. C'est le composant qui permet la communication avec le réseau.
  
## Définitions

> 📢 Tous les appareils connectés possède une adresse physique unique au monde: c'est une adresse __MAC__ (_Media Acces Control_) stocké dans leur carte réseau. 


> 📢Dans un réseau local chaque périphériphérique connecté posséde en plus un numéro d'identification attribué de façon permanente ou provisoire : c'est l'adresse __IP__ (_Internet Protocol_).

## Fonctionnement d'un réseau local  

Si on suppose un réseau local composé de plusieurs ordinateurs et d'un switch. Lorsque l'ensemble des cables sont branchés entre ces ordinateurs et le switch, on dit que le réseau physique est opérationnel.  
Les ordinateurs ne peuvent cependant pas encore communiquer entre eux car ils ne se "voient" pas.  

Dans la mémoire de l'ordinateur (ou élément du réseau), sera stockée une __table ARP__. 

> 📢 La table __ARP__(_Adress Resolution Protocol_) fait la correspondance entre les adresses IP du réseau local et les adresses MAC. 

Pour parvenir à ce résultat, les ordinateurs envoient des requêtes à l'ensemble du réseau local. En fonction des réponses, ils construisent progressivement leur table ARP. 

Exemple:  

![Table ARP ordinateur ](./fig/tablearp.png)  

* Pour le switch, il va de la même manière observer les flux et affecter à ces ports les adresses MAC des éléments de son réseau local. 
   
Exemple:  

![Table ARP switch ](./fig/tablemac.jpg)  

Lorsque les liens entre les ports, les adresses IP et les adresses MAC sont réalisées, les ordinateurs peuvent communiquer entre eux. (la construction des tables se fait progressivement.)  

# Internet un réseau de dimension mondiale.  

[Lien vers la vidéo TCP/IP](https://www.youtube.com/watch?v=AYdF7b3nMto)  

## Qu'est ce que le réseau internet?  

![Représentation réseau internet](./fig/internet.jpg)  

On pourrait voir internet comme un ensemble de routeurs reliés entre eux qui permettent de connecter les réseaux locaux. Les liaisons entre les routeurs sont nombreuses ce qui permet d'avoir de multiples chemins pour aller d'un réseau local A à un réseau local B.  

Ce réseau Internet étant très complexe, il est necessaire d'imposer un adressage précis et un protocole de transport.  

## L’ADRESSE IP  

* Comme nous l'avons vu, chaque ordinateur possède une adresse MAC unique délivrée par le fabriquant de l'ordinateur.  

* Dans le réseau local, une adresse IP locale est attribuée de manière dynamique par le serveur DHCP de la box (routeur qui va permettre la liaison avec internet.) Elle commence souvent par 192.168.....  

* Lorsque vous vous connectez à internet via un ordinateur, un smartphone..., vous demandez à votre FAI une adresse IP publique. Cette adresse IP est souvent dynamique, c'est à dire qu'elle change à chaque connexion. Elle est composée de quatre nombres séparés par un point. Par exemple 205.58.45.65 dans un adressage IPV4 c'est à dire sur quatre octet.  

![Adresse IPV4](./fig/adressip.png)  

_📝 Application_

* Retrouver la première valeur 172 en binaire.  
..........................................................................

* Combien y a-t-il d'adresses possibles sachant que chaque nombre est codé sur un octet?  
............................................................................

* Vous pouvez alors faire des requêtes vers des adresses IP sur le net.  

## Nom de domaine.  

La notion de nom de domaine est étroitement liée aux adresses IP. En effet, retenir une suite de 4 nombres ne convient pas et limite donc les recherches des internautes. Pour palier à cela, on a crée le système de nom de domaine DNS qui affecte un nom à une adresse IP. Ainsi, google.fr est le nom de domaine associé à l'adresse IP 216.58.206.227  

En utilisant le site www.mon-ip.net, trouver l'adresse IP publique attribuée à l'établissement Marguerite de Flandre.  
.................................................................

## Le protocole TCP / IP:  

* Le protocole TCP (Transmission Control Protocol) est un ensemble de règles qui fixe les conditions de transport des informations. Le protocle TCP est souvent constitué de trois phases:  
  - Etablissement d'une connexion: Avant d'envoyer des informations ou de demander des informations, une synchronisation entre l'émetteur et le recepteur est assurée via un échange préalable.  
  - Transfert de données: Pendant cette phase, plusieurs processus sont en action. Tout d'abord, si le paquet d'envoi est trop gros, il est fractionné et les paquets sont numérotés. Les paquets sont envoyés et un accusé de réception est attendu pour chaque paquet envoyé. Si un accusé de réception n'est pas reçu, le paquet est réexpédié.
  - La rupture de la liaison: Lorsque l'ensemble des transferts sont effectués, on procède à l'arrêt de la connexion.  

* Encapsulation des données:  

Comme nous l'avons vu au chapitre précédant, on peut utiliser le modèle théorique des couches (OSI) pour représenter la communication entre deux noeuds d'un réseau.  

![Encapsulation](./fig/encapsulation.png)  

Dans cette photo, on part du haut c'est à dire des données (DATAS). Lorsqu'on passe à la couche juste en dessous, un en-tête est placé devant les DATAS afin d'expliquer avec quelle application les datas pourront être lues. Nous avons ensuite un en-tête TCP qui va reprendre la structure que nous avons vu pour le protocole à savoir : Connexion, transfert et rupture. Enfin, l'en-tête IP qui contient entre autre l'adresse IP du destinataire et de l'émeteur.  

Ainsi lorsqu'un échange de données est réalisé, on part des DATAS, on ralise un ensemble d'encapsulation puis les données sont envoyées. Lorsqu'elles sont reçus, le récepteur désencapusle les données. Pour le récepteur, on part cette fois ci du bas de la photos pour aller vers le haut.  

## Récupération de paquets perdus: Protocole de bit alterné.  

* Nous avons vu que le protocole TCP propose un mécanisme d'accusé de réception afin de s'assurer qu'un paquet est bien arrivé à destination. On parle plus généralement de __processus d'acquittement__. Ces processus d'acquittement permettent de détecter les pertes de paquets au sein d'un réseau, l'idée étant qu'en cas de perte, l'émetteur du paquet renvoie le paquet perdu au destinataire. Nous allons ici étudier un protocole simple de récupération de perte de paquet : le protocole de bit alterné.  

* Le principe de ce protocole est simple, considérons 2 ordinateurs en réseau : un ordinateur A qui sera l'émetteur des trames et un ordinateur B qui sera le destinataire des trames. Au moment d'émettre une trame, A va ajouter à cette trame un bit (1 ou 0) appelé drapeau (flag en anglais). B va envoyer un accusé de réception (acknowledge en anglais souvent noté ACK) à destination de A dès qu'il a reçu une trame en provenance de A. À cet accusé de réception on associe aussi un bit drapeau (1 ou 0).  

* La règle est relativement simple : la première trame envoyée par A aura pour drapeau 0, dès cette trame reçue par B, ce dernier va envoyer un accusé de réception avec le drapeau 1 (ce 1 signifie "la prochaine trame que A va m'envoyer devra avoir son drapeau à 1"). Dès que A reçoit l'accusé de réception avec le drapeau à 1, il envoie la 2e trame avec un drapeau à 1, et ainsi de suite...  

![Protocole bit alterné](./fig/protocolebitalterne.png)  

* Le système de drapeau est complété avec un système d'horloge côté émetteur. Un "chronomètre" est déclenché à chaque envoi de trame, si au bout d'un certain temps, l'émetteur n'a pas reçu un acquittement correct (avec le bon drapeau en accusé de reception), la trame précédemment envoyée par l'émetteur est considérée comme perdue et est de nouveau envoyée.


__Pour revoir tout cela autrement : __ [ici](https://info.blaisepascal.fr/couche-transport/)