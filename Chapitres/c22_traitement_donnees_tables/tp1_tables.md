---
title: "Chapitre 20  : Traitement des données en table"
subtitle: "TP n°1 - Fichiers CSV et Recherche dans une table"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : Bon de commande &#x1F3C6;

(_Extrait de Numérique et sciences informatiques aux éditions Ellipses_)

Imaginons un bon de commande représenté par une table de données dans laquelle chaque ligne correspond à un produit commandé et contient quatre attributs : la référence du produit et sa désignation(deux chaînes de caractères), le prix unitaire(un nombre décimal), la quantité commandée (un nombre entier). Voici un exemple.

|réf.|désignation|prix|qté|
|:---:|:---:|:---:|:---:|
|18635|lot crayons HB|2,30|1|
|15223|stylo rouge|1,50|3|
|20112|cahier petits carreaux|3,50|2|

__1)__ Construire une table sous forme de liste de dictionnaires en Python représentant ce bon de commande.

__2)__ Ecrire un prédicat `verifie_quantites` qui analyse un bon de commande et renvoie `True` si pour chaque produit commandé la quantité est bien strictement positive (`False` sinon)

__3)__ Ecrire une fonction `nombre_produit` qui renvoie le nombre total de produits demandés dans un bon de commande donné en argument (en ne comptant que les quantités positives)


# Exercice  : QCM &#x1F3C6;&#x1F3C6;

(_Extraits des QCM diffusables pour les E3C_)

__1.__ On considère la table suivante :

```python
t = [ {'type': 'marteau', 'prix': 17, 'quantité': 32},
{'type': 'scie', 'prix': 24, 'quantité': 3},
{'type': 'tournevis', 'prix': 8, 'quantité': 45} ]
```

Quelle expression permet d'obtenir la quantité de scies ?

* [ ] `t[2]['quantité']`
* [ ] `t[1]['quantité']`
* [ ] `t['quantité'][1]`
* [ ] `t['scies']['quantité']`

__2.__ On souhaite construire une table de 4 lignes de 3 éléments que l’on va remplir de 0. Quelle syntaxe Python
utilisera-t-on ?


* [ ] `[ [ 0 ] * 3 for i in range (4) ]`
* [ ] `for i in range (4) [ 0 ] * 3`
* [ ] `[ 0 ] * 3 for i in range (4)`
* [ ] `[ for i in range (4) [ 0 ] * 3 ]`


__3.__ On considère l’extraction suivante d'une base de données des départements français. Cette extraction a ensuite été sauvegardée dans un fichier texte.

```
"1","01","Ain","AIN","ain","A500"
"2","02","Aisne","AISNE","aisne","A250"
"3","03","Allier","ALLIER","allier","A460"
"4","04","Alpes-de-Haute-Provence","ALPES-DE-HAUTE-PROVENCE","alpes-de-hauteprovence","A412316152"
"5","05","Hautes-Alpes","HAUTES-ALPES","hautes-alpes","H32412"
```

On considère le code suivant :

```python
import csv
with open('departements.csv', newline='') as monFichier:
    lesLignes = csv.reader(monFichier)
    for uneLigne in lesLignes:
        print(uneLigne[3])
```

Que va produire l'exécution de ce code ?


* [ ] L'affichage de la troisième colonne à savoir le nom du département avec une majuscule initiale
* [ ] L'affichage de tout le contenu du fichier
* [ ] L'affichage du nombre total de départements figurant dans le fichier
* [ ] L'affichage de la quatrième colonne, à savoir le nom du département tout en majuscules    


__4.__ On définit :

```python
stock = [ {'nom': 'flageolets', 'quantité': 50, 'prix': 5.68},
        {'nom': 'caviar', 'quantité': 0, 'prix': 99.99},
        .........
        .........
        {'nom': 'biscuits', 'quantité': 100, 'prix': 7.71} ]
```

Quelle expression permet d'obtenir la liste des noms des produits effectivement présents dans le stock (c'est-à dire ceux dont la quantité n'est pas nulle) ?

* [ ] `['nom' for p in stock if 'quantité' != 0]`
* [ ] `[p for p in stock if p['quantité'] != 0]`
* [ ] `[p['nom'] for p in stock if 'quantité' != 0]`
* [ ] `[p['nom'] for p in stock if p['quantité'] != 0]`


# Exercice 3 : Aeroports du monde &#x1F3C6;&#x1F3C6;

Pour cette activité nous allons utiliser un __notebook jupyter__ et le module `folium`  
Si ce n'est déjà fait : installer les modules __jupyter__ puis __folium__ (via Thonny : `Outils\Gérer les paquets`)

__1)__ Récupérez tout d'abord l'archive suivante : [notebook_aeroport.zip](./notebook_aeroport.zip) et enregistrez-la. Puis, décompressez-la impérativement dans un dossier de votre choix (__notez bien l'emplacement__ et surtout la lettre associée au disque s'il y en a une __C__, __D__ ...).

__2)__ Dans Thonny, ouvrez une __invite de commandes__ : `Outils\Ouvrir la console du système` (_Open system shell_)

Un terminal doit alors s'ouvrir (_si ce n'est pas le cas, enregistrez le script courant sous un nom quelconque puis réouvrez le terminal_)

![Invite de commandes](./fig/cmd.jpg)  

Comme pour le Shell de Python, le terminal attend des instructions qui seront validées par l'appui sur la touche `Entrée`.
Le prompt de python `>>>` est remplacé par le chemin menant au répertoire courant. 

__3)__ 

* Placez vous tout d'abord à la racine du disque où vous avez enregistré le notebook 

Sous windows (si le disque se nomme C)

```bash
c:
```

Sous MacOS ou Linux 

```bash
cd /
```

* Puis lancez l'application jupyter

```bash
jupyter notebook
```

Après quelques instants permettant l'initialisation d'un serveur local jupyter sur votre machine une page web s'affichera ressemblant à ceci 

![Serveur jupyter ](./fig/serveur_jupyter.jpg)  

Retrouvez le fichier `airports.ipynb` présent dans le dossier créé lors de la décompression de l'archive en naviguant dans l'arborescence et exécutez-le en cliquant dessus.

---

--> Si vous ne parvenez pas à lancer le notebook malgré ces explications : utilisez le lien suivant 

[![Notebook Dichotomie](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=notebook_aeroport%2Fairports.ipynb)  

_le temps de chargement peut être parfois être  de 1 à 2 min maximum ....._

---


#  Fonctionnement du notebook

Le carnet est constitué de cellules :

- Certaines contiennent du texte mis en forme, des images, des animations.. 

![cellule texte ](./fig/text_cell.jpg)  

- D'autres sont utilisées comme blocs-notes pour répondre aux questions

![cellule reponse ](./fig/response_cell.jpg)  

- Enfin, les cellules précédées de `Entrée [ ]` sont des cellules exécutables

![cellule python ](./fig/python_cell.jpg) 

  - Toutes les cellules devront être exécuter une par une (et chronologiquement) : sélectionnez la cellule puis appuyez simultanément sur `Ctrl + Entrée`  ou sur cliquer sur le bouton `Exécuter`

-   Lorsque le code est exécuté, l'ordre dans lequel les cellules ont été evaluées apparait entre  `[ ]`

![cellule python entrée ](./fig/python_cell_entree.jpg) 

_Attention, parfois vous aurez à réexécuter des cellules précédentes pour mettre à jour votre code !_

---

SOURCES : 

* Le notebook a été réalisé en grande partie à partir de https://github.com/nsi-acot/continuite_pedagogique_premiere/tree/master/algorithmique/dichotomie
