---
title: "Chapitre 21  : Traitement des données en table "
subtitle: "PARTIE 1 : Recherche dans une table - Cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Vous avez déjà toutes et tous utilisés un tableur comme libreOffice Calc. Ce logiciel permet de manipuler des informations organisées appelées tables. Nous allons aborder la structure de ces tables et l'exploitation de leur contenu.

# Représentation des tables  

## Définition

> 📢 Une table est un ensemble de données organisées sous forme d'un tableau où chaque __ligne__ correspond à un __enregistrement__.  
> Dans chaque __colonne__ on trouve une valeur correspondant à un __attribut__ (appelé aussi __descripteur__)

_Par exemple :_ 

|insee |nom| surface|CODE POSTAL|
|:----:|:----:| :----:| :----:|
|59005|	Allennes-les-Marais|	5529290.793	|59251|
|59009|	Villeneuve-d'Ascq	|27589625.1	|59650|
|59011|	Annœullin|	8964079.542	|59112|
|59013|	Anstaing|	2311779.151	|59152|
|59017|	Armentières	|6294040.451|	59280|

Le tableau ci-dessus est une toute petite partie des données extraites de la table nommée "Limites administratives des communes de la MEL" accessible sur le site https://opendata.lillemetropole.fr

On constate la présence de 4 attributs nommés  : `insee`, `nom`, `surface`, `CODE POSTAL`  et de 6 enregistrements correspondant à 5 communes différentes (_le premier enregistrement étant la ligne qui contient le nom des attributs_)

## Le format CSV

> 📢 Le format __CSV__ (_Comma-separated values_ c'est à dire _valeurs séparées par des virgules_) est un format texte permettant de représenter les données structurées. Chaque ligne de ce fichier texte correpond à un enregistrement. Chaque attribut est séparé par un délimiteur qui est (_malgré le nom du format_) très souvent le point-virgule `;`

Pour échanger ces données on utilise trés frequemment ce format de fichier ouvert.

_Pour la table précédente voilà le contenu du fichier csv correspondant_  [mel_communes.csv](./mel_communes.csv) : 

```
insee;nom;surface;CODE POSTAL
59005;Allennes-les-Marais;5529290.793;59251
59009;Villeneuve-d'Ascq;27589625.1;59650
59011;Annœullin;8964079.542;59112
59013;Anstaing;2311779.151;59152
59017;Armentières;6294040.451;59280
```

# Importation des données

Pour pouvoir manipuler des données volumineuses à l'aide du langage Python il conviendra de les importer dans une structure de données adaptée : nous étudierons deux possiblités.

## Utilisation d'un tableau doublement indexé (liste de listes)

```python
import csv 

with open('mel_communes.csv', encoding='utf-8', newline='') as fichier:
    listecommunes = list(csv.reader(fichier, delimiter=';'))
```

Le code précédent  :

* importe le module `csv` natif dans Python
* ouvre le fichier `mel_communes.csv` à l'aide de la syntaxe `with...` en  précisant l'encodage
* utilise la fonction `reader` prenant pour argument le fichier précédent et la nature du délimiteur csv
* l'objet renvoyé par la fonction précédente est finalement convertit en tableau à l'aide de `list` 

On obtient ainsi un tableau de tableau :

```python
>>> listecommunes

[['insee', 'nom', 'surface', 'CODE POSTAL'], 
['59005', 'Allennes-les-Marais', '5529290.793', '59251'], 
['59009', "Villeneuve-d'Ascq", '27589625.1', '59650'], 
['59011', 'Annœullin', '8964079.542', '59112'], 
['59013', 'Anstaing', '2311779.151', '59152'], 
['59017', 'Armentières', '6294040.451', '59280']]
```

et on peut accéder à n'importe quel enregistrement à l'aide de son indice dans le tableau, par exemple :

```python
>>> listecommunes[1]
['59005', 'Allennes-les-Marais', '5529290.793', '59251']
```

L'inconvénient de ce type de structures est que le lien entre la description des attributs (la première ligne de la table) et chaque enregistrement n'est plus explicite. On accéde aux attributs par des indices.   
Par exemple le code postal de la ville Annoeullin dans la table serait obtenu ainsi :

```python
>>> listecommunes[3][3]
'59112'
```

L'idée est donc plutôt d'utiliser pour chaque enregistrement un dictionnaire.

## Utilisation d'un tableau de dictionnaires

```python
import csv 

listecommunes = []
with open('mel_communes.csv', encoding='utf-8', newline='') as fichier:
    reader = csv.DictReader(fichier, delimiter=';')
    for ligne in reader:
        listecommunes.append(dict(ligne))
```

La différence principale de ce nouveau code est l'utilisation de la fonction `DictReader` du module csv. Elle génére un objet semblable à une collection de dictionnaires que l'on peut parcourir : on construit ainsi progressivement la table `listecommunes` .
Ici, la première ligne du fichier devra nécessairement contenir les clés des dictionnaires.

Cela permet de construire une nouvelle liste :

```python
>>> listecommunes

[{'insee': '59005', 'nom': 'Allennes-les-Marais', 'surface': '5529290.793', 'CODE POSTAL': '59251'}, 
{'insee': '59009', 'nom': "Villeneuve-d'Ascq", 'surface': '27589625.1', 'CODE POSTAL': '59650'}, 
{'insee': '59011', 'nom': 'Annœullin', 'surface': '8964079.542', 'CODE POSTAL': '59112'}, 
{'insee': '59013', 'nom': 'Anstaing', 'surface': '2311779.151', 'CODE POSTAL': '59152'},
{'insee': '59017', 'nom': 'Armentières', 'surface': '6294040.451', 'CODE POSTAL': '59280'}]
```

Cette fois-ci la structure de données construite est une liste de dictionnaires plus simple à utiliser. On accédera directement à la valeur d'un attribut en utilisant le nom de la clé associée. 


```python
>>> listecommunes[2]['CODE POSTAL']
'59112'
```

Mais attention, l'indexation est légérement modifiée car la première ligne du fichier contenant les noms des champs n'est plus présente à l'indice 0.  


# Recherche dans une table

> 📢 Une fois l'ensemble de données chargées, il devient possible de les exploiter en extrayant une partie des données, en testant la présence de certaines d'entre elles, en faisant des statistiques.... Ces opérations sont appelées __requêtes__


## Recherche en fonction d'une clé

Les méthodes de recherche déjà évoquées précedemment cette année sont appliquables aux tables.

Par exemple recherchons la présence d'une ville donnée dans la table précédente en définissant un prédicat renvoyant `True` si elle est trouvée, `False` sinon.

```python
def appartient_mel(listecommunes, ville):
    for commune in listecommunes:
        if commune['nom'] == ville :
            return True
    return False
```

```python
>>> appartient_mel(listecommunes, 'Paris')
False
```

## Agrégation des données

Les opérations d'agrégation des données de plusieurs lignes permettent de produire un résultat et en particulier une statistique sur ces données.

Par exemple combien de communes ont une surface inférieure à $`7000000 m^{2}`$  (soit $`7 km^{2}`$ )
Cela revient à un comptage d'occurences :

```python
def surface_max(listecommunes, surface):
    n = 0
    for commune in listecommunes:
        if float(commune['surface']) < 7000000 :
            n = n + 1
    return n
```

On retrouve bien le fait que 3 communes dans la table possédent cette caractéristique

```python
>>> surface_max(listecommunes, 7000000)
3
```

## Sélection (de lignes ou de colonnes)

On peut également souhaiter construire une nouvelle table en extrayant de la table d'origine les données vérifiant une certaine condition.

Par exemple, quelles sont les noms des communes dont la surface est inférieure à $`7000000 m^{2}`$ ?

```python
>>> [commune['nom'] for commune in listecommunes if float(commune['surface']) < 7000000]
['Allennes-les-Marais', 'Anstaing', 'Armentières']
```

Ici on a bien sélectionné les données dans la colonne 'nom' correspondant au critére énoncé.

Les utilisations sont très variées et ceci avec des données de taille encore plus importante que l'exemple proposé dans ce cours.
