s1 = 'L\'apostrophe est noté : \''
s2 = 'Si on utilise des "guillemets" : attention ! ' 
s3 = 'Le chemin d\'accès au fichier est C:\\Utilisateurs\\Public\\NTUSER.DAT"'


date = '05/06/1976'
jour = date[0:2]
mois = date[3:5]
annee = date[6:]

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

### EXERCICE 6
def firstname_format(prenom):
    '''renvoie la mise en forme du prenom
    '''
    maj= prenom[0].upper()
    reste = prenom[1:].lower()
    return maj + reste
    
def firstname_format_bis(prenom):
    '''renvoie le prenom composé mis en forme
    '''
    indice_tiret = prenom.find('-')
    prenom1 = prenom[:indice_tiret]
    prenom2 = prenom[indice_tiret+1:]
    return firstname_format(prenom1)+'-'+firstname_format(prenom2)
    
    
    
    
def lastname_format(nom):
    '''renvoie la mise en forme du nom
    '''
    return nom.upper()
def find_firstname(nom_complet):
    '''renvoie le prénom
    '''
    indice_separation = nom_complet.find(' ')
    return nom_complet[:indice_separation]

def find_lastname(nom_complet):
    '''renvoie que le nom de famille
    '''
    indice_separation = nom_complet.find(' ')
    return nom_complet[indice_separation+1:]

def name_format(nom_complet):
    '''renvoie le nom complet formaté
    '''
    prenom = find_firstname(nom_complet)
    nom = find_lastname(nom_complet)
    return firstname_format(prenom) + ' ' + lastname_format(nom)


def name_format_bis(nom_complet):
    '''renvoie le nom complet formaté
    '''
    if nom_complet.find('-') ==-1 : # si pas de tiret
        return name_format(nom_complet)
    else :
        prenom = find_firstname(nom_complet)
        nom = find_lastname(nom_complet)
        return firstname_format_bis(prenom) + ' ' + lastname_format(nom)