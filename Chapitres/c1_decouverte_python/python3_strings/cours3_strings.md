---
title: "Chapitre 1 Découverte Python"
subtitle: "Partie 3 - Cours : Introduction aux chaînes des caractères"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---


# Quelques généralités

Une chaîne de caractères est délimitée en Python par une paire d'apostrophes __'__ (_single quotes_) ou de guillemets __"__ (_double quotes_).  
On peut toujours vérifier le type de la donnée avec la fonction `type`.  

```python
>>> type('informatique')
<class 'str'>
```

```python
>>> type("informatique")
<class 'str'>
```
Cette fonction retourne alors la valeur `<class 'str'>` : __str__ provient du mot anglais string (chaîne)

_Remarque_ : on pourrait très bien délimiter une chaîne à l'aide de triples apostrophes `'''` ou de triples guillemets `"""`.  
 On les utilise en général pour les docstring mais également lorsque la chaîne est très longue et qu'il est nécessaire de passer à la ligne.

Un caractère peut correspondre à une lettre, un chiffre, un espace, un symbole de ponctuation...

```python
>>> type("!")
<class 'str'>
```
```python
>>> type("2")
<class 'str'>
```
```python
>>> type("a")
<class 'str'>
```
```python
>>> type(" ")
<class 'str'>
```

Certains caractères peuvent poser problème : c'est notamment le cas des apostrophes ou des guillemets comme par exemple dans la phrase `l'herbe n'est pas toujours verte`.  
Les apostrophes étant également les délimiteurs de chaîne, leur nombre deviendra impair si on l'utilise.

```python
>>> chaine = 'l'herbe n'est pas toujours verte'
   File "<pyshell>", line 1
     chaine =l'herbe n'est pas toujours verte
                     ^
 SyntaxError: invalid syntax
```

Deux solutions sont envisageables : 
* Soit on utilise l'autre délimiteur (ici les guillemets) pour encapsuler la chaîne
```python
>>> chaine = "l'herbe n'est pas toujours verte"
>>> chaine
"l'herbe n'est pas toujours verte"
```

* Soit on utilise un __caractère d'échappement__  `\` afin que Python n'interpréte pas l'apostrophe ou le guillemet comme étant un délimiteur.  
On écrit alors `\'` ou `\"` suivant le cas

```python
>>> chaine = 'l\'herbe n\'est pas toujours verte'
>>> chaine
"l'herbe n'est pas toujours verte"
```

# Opérations

## Concaténation
On peut assembler des chaînes de caractères à l'aide d'une opération nommée __concaténation__ en utilisant l'opérateur `+`

```python
>>> s_1 ='Numerique'
    s_2 ='et'
    s_3 ='sciences'
    s_4 = 'informatiques'
    chaine = s_1 + s_2 + s_3 + s_4
>>> chaine
'Numeriqueetsciencesinformatiques'
```
On constate bien dans l'exemple précédent que l'`espace` est un caractère à part entière : il faudra y penser systématiquement!

## Répétition

L' opérateur `*` permet de répéter un nombre entier de fois une séquences de caractères:

```python
>>> s = 'bla '
>>> 5 * s
'bla bla bla bla bla '
```

## Longueur de la chaine

Une des fonctions natives en Python permet d'obtenir la longueur totale d'une chaîne passée en paramètre, il s'agit de `len`.

On peut le vérifier à l'aide de la documentation
```python
>>> help(len)
 Help on built-in function len in module builtins:

 len(obj, /)
     Return the number of items in a container.
```
Cette longueur correspond bien au nombre de caractères présents dans la chaîne.

```python
>>> len("Python")
6
```

Attention le caractère spécial `\` utilisé comme symbole d'échapemment ne compte pas.

```python
>>> len('l\'ordinateur')
12
```

## Conversions de types

Il est possible de convertir un nombre en chaîne de caractères ou l'inverse.

```python
>>> nombre = 12
>>> chaine = str(nombre)
>>> chaine
'12'
```
On constate que les variables `nombre` et `chaine` sont de deux types différents.

```python
>>> type(nombre)
<class 'int'>
```

```python
>>> type(chaine)
<class 'string'>
```

* La méthode `str` permet de créer une chaîne de caractères à partir d'un paramètre n'en étant pas une (_un nombre par exemple_).
  
* De manière équivalente, la fonction `int` retourne un nombre entier à partir d'une chaîne de caractères fournie en paramètre


```python
>>> int('12')
12
```

# Indexation

## Indice de position.

La séquence de caractères présents dans une chaîne est indexée. 
Cela signifie qu'à chaque caractère est associé est __indice de position__ (_nombre entier_).  
* Le premier caractère de la séquence posséde l'indice 0
* Pour une chaîne de n caractères les indices varient donc de __0__ à __n-1__

On accéde à un caractère donné d'une chaine à l'aide de la syntaxe suivante : `chaine[indice]`.  

   
_Exemple d'indexation d'une chaine :_ [^1]

![indexation chaine](./fig/indexation.png)

```python
>>> s = 'Rayons X'
>>> s[0]
'R'
```

```python
>>> s[5]
's'
```


## Extraction

Il est possible d'extraire une sous-chaîne de caractères en utilisant les indices de position.  
Python permet de réaliser des tranches de caractères ("_slices_") en utilisant la syntaxe `chaine[i:j]` où `i` et `j` représentent les indices de position du début et de la fin de la tranche à extraire.

```python
>>> s = "Algorithme"
>>> s[0:3]
"Alg"
```

> Attention, l'intervalle d'indices de position extrait est __[i,j[__ c'est à dire que la caractère d'indice `j` est exclu de la tranche.

```python
>>> s[0:1]
"A"
```

L'un ou l'autre des indices est optionnel, dans ce cas toute la partie à droite ou à gauche de la chaîne sera extraite.

```python
>>> s[2:]
'gorithme'
```

```python
>>> s[:5]
'Algor'
```

# Quelques méthodes associées.

Par-delà les opérations de bases présentées, il existent un très grand nombre de __méthodes__ : c'est à dire de fonctions spécifiques au type de données concernées (ici le type _string_).  
Notre objectif n'est pas de toutes les présenter ici, leur syntaxe sera cependant commune :  
`chaine.methode(paramétres_éventuels)`  


## Minuscules et majuscules

* La méthode `lower()` renvoie une copie de la chaîne de caractères dans laquelle tous les caractères capitalisables (principalement les lettres) sont convertis en minuscules

```python
>>> s = "InTERnet"
>>> s.lower()
"internet"
```

La chaîne originale est inchangée, on peut le vérifier : 

```python
>>> s
"InTERnet"
```

* La méthode `upper()` est analogue dans son fonctionnement mais les caractères capitalisables sont convertis en majuscules

```python
>>> s.upper()
"INTERNET"
```

## Recherche d'un caractère

La méthode `find()` donne l'indice de la première position dans la chaîne de la sous_chaîne passée en paramètre.

Nous souhaitons par exemple rechercher la sous-chaîne `"de"` dans la chaîne suivante `"Un ordinateur est un système de traitement de l'information programmable"`

```python
>>> s = "Un ordinateur est un système de traitement de l'information programmable"
>>> s.find("de")
29
```

La recherche est sensible à la casse

```python
>>> s.find("un")
18
```

La sous-chaîne peut être composée d'un seul caractère

```python
>>> s.find("U")
0
```

Si le caractère n'est pas présent, la valeur de retour est __-1__

```python
>>> s.find("w")
-1
```
_Remarques_ : d'autres paramètres peuvent être passés à cette méthode pour affiner la recherche (vous pouvez consulter la documentation associée)

---

_Sources_

[^1]: [Cours Laurent Pointal](https://perso.limsi.fr/pointal/python:courspython3) 

