---
title: "TP : Les fonctions"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

---

DEFINITIONS, APPELS ET DOCUMENTATION

---


# Exercice n°1 : Fonctions natives et documentation &#x1F3C6;

__1)__ Faites apparaitre la documentation de la fonction `pow()`  
    __a-__ A quoi sert-elle ? Testez-la avec une exemple de votre choix pour le vérifier.  
    __b-__ Combien de paramètres au minimum doivent être utilisés ?  

__2)__ Faites apparaitre la documentation de la fonction `abs()`  
    __a-__ A quoi sert-elle ? Testez-la avec une exemple de votre choix pour le vérifier.  
    __b-__ Combien de paramètre(s) doivent être utilisés ?


# Exercice n°2 : Quelques fonctions personnalisées &#x1F3C6;

Pour cet exercice, créez préalablement un script nommé `fonctions_persos.py`.
Pour chacun des cas suivants vous définirez correctement une fonction en n'oubliant pas de :
* choisir un nombre de paramètres adaptés
* nommer les fonctions et les paramètres de manière non ambigüe
* de créer une docstring associée et de tester.

__1)__ Définir une fonction retournant le carré d'un nombre  
__2)__ Définir une fonction retournant le perimètre d'un rectangle  
__3)__ Définir une fonction retournant la moyenne de 3 notes  
__4)__ 🥇Définir une fonction retournant la moyenne pondérée de 2 notes


# Exercice n°3 : IMC &#x1F3C6;

__1)__ Créer un nouveau script nommé `imc.py`.   

__2)__ En reprenant la définition vue lors de la séance précédente indiquez combien de paramètres sont nécessaires à la définition d'une fonction permettant de calculer l'IMC.

__3)__ Créez la fonction `calcul_imc` correspondante en choisissant des noms de paramètres explicites.  
Vous n'oublierez pas de renseigner la docstring

__4)__ Après avoir sauvegardé votre script, testez-la fonction avec les exemples suivants :

```python
>>> calcul_imc(95, 1.81)
28.997893837184456
>>> calcul_imc(140, 2.04)
33.64090734332949
```
__5)__ On souhaite améliorer cette fonction en donnant une valeur arrondie au dixième du résultat.  
Ecrivez une fonction `calcul_imc_bis` en utilisant la fonction `round` vue en cours ( reprenez la documentation associée si nécessaire)

```python
>>> calcul_imc_bis(95, 1.81)
29.0
>>> calcul_imc_bis(140, 2.04)
33.6
```

# Exercice n°4 : année-lumière &#x1F3C6;&#x1F3C6;
> (source de l'exercice : [cours info L1S1 FIL](http://www.fil.univ-lille1.fr/~L1S1Info/Doc/New/exercices2_fr.html#Exercice-1-(ann%C3%A9e-lumi%C3%A8re)))

L'__année-lumière__ (_al_) est définie comme la distance parcourue par un photon (ou plus simplement la lumière) dans le vide, en une année julienne (soit 365,25 jours, ou 31 557 600 secondes).  

![Quelques distances en al](./fig/Light-year-scale-Bob-King.jpg)[^1]

__1)__ Sachant que la vitesse de la lumière dans le vide est une constante fixée à 299 792 458 m/s, calculez à combien de kilomètres correspondent une année-lumière à l'aide d'instructions Python.

__2)__ Écrivez une fonction qui convertit une distance exprimée en années-lumière en une distance exprimée en kilomètres.

__3)__ Sachant que Proxima Centauri, l'étoile la plus proche du Système solaire, se trouve à 4,22 années-lumière de la Terre, à combien de km de la Terre cette étoile se trouve-t'elle ?

---

UTILISATION DU MODULE MATH

---

# Exercice n°5 : Théorème de Pythagore &#x1F3C6;&#x1F3C6;
  
__1)__ Définissez la fonction `hypotenuse`.
Elle acceptera deux paramètres notés `a` et `b` correspondant aux longueurs des 2 plus petits côtés d'un triangle rectangle (exprimées avec la même unité).  
La valeur de retour de cette fonction sera la longueur de l'hypoténuse.

__2)__ Enregistrez le script sous le nom `hypotenuse.py`

__3)__ Vérifiez votre fonction à l'aide des exemples suivants: 

```python
>>> hypotenuse(2, 3)
3.605551275463989
>>> hypotenuse(5, 2)
5.385164807134504
```

__4)__ Le module math possède une fonction nommée `hypot`.  
Importez-la, puis recherchez dans sa documentation son mode d'utilisation.  
Que constatez-vous ?

# Exercice n°6 : Autour du cercle &#x1F3C6;&#x1F3C6;

__1)__ Créer un nouveau script nommé `cercle.py`.   
Importez-y `pi` provenant de la bibliotheque `math`.

Vous pouvez évaluez sa valeur : 
```python
>>> pi
3.141592653589793
```

__2)__ Définissez la fonction `perimetre`
Elle acceptera un seul paramètre noté `rayon` correspondant au rayon d'un cercle.  
La valeur de retour de cette fonction sera le périmètre de ce cercle.

Vérifiez votre fonction à l'aide des exemples suivants: 

```python
>>> perimetre(2)
12.566370614359172
>>> perimetre(100)
628.3185307179587
>>> 
```

__3)__ Toujours dans le même script, définissez la fonction `aire`. 
Elle acceptera un seul paramètre noté `rayon`.  
La valeur de retour l'aire du disque correspondant.

Vérifiez votre fonction à l'aide des exemples suivants:
```python
>>> aire(1)
3.141592653589793
>>> aire(5)
78.53981633974483
>>> 
```

__4)__ 🥇 Après avoir sauvegardé votre script précédent, fermez le fichier puis  ouvrez une nouvelle fenêtre et enregistrez un nouveau script pour l'instant vierge sous le nom `test_module.py` __dans le même dossier__ que le script précédent

`cercle.py` peut  être utilisé comme un module indépendant.
Essayez d'importer le module __cercle__ dans votre nouveau script et utilisez les fonctions précédemment définies.


# Exercice n°7 :  Des fonctions en série &#x1F3C6; &#x1F3C6;

Une fonction peut appeler d'autres fonctions :  
__1)__ Définir une fonction `cube` retournant le cube d'un nombre  

__2)__   
__a-__ Définir une fonction `volume_sphere` retournant le volume d'une sphère  
__b-__ Modifiez cette fonction en utilisant la fonction `cube` precedemment définie

---

UTILISATION DU MODULE RANDOM

---

# Exercice n°8 : Lancer de dés &#x1F3C6;

__1)__ Beaucoup de jeu de rôles (JDR) utilisent des dés spéciaux avec un nombre de faces adapté. 

![Dé 20 faces](./fig/20-sided_dice_250.jpg)[^2]


Créez une fonction `dice_20` sans paramètre simulant le lancer d'un dé à 20 faces (le nombre le plus petit étant `1` et les valeurs allant de un en un )

_remarque_ : lors de la définition d'une fonction sans paramètre les parenthèses sont tout de même obligatoires:
```python
def fonction_sans_parametre():
......
```

__2)__ Créez une fonction `dices_roll` (sans paramètre également) simulant le lancer simultané de 3 dés 20.  
Cette fonction retourne uniquement la somme du lancer : __vous utiliserez bien évidemment la fonction précédente__.  


# Exercice n° 9 : QCM
Indiquez à chaque fois __la bonne réponse__ (l'utilisation de l'ordinateur est interdit):

__Question 1__  
On considère la fonction suivante :
```python
def fonction_mystere(a, b):
    return a // b
```
Quelle est la valeur de retour pour `fonction_mystere(4, 2)`?  
* [ ] 0    
* [ ] 2.0   
* [ ] 4  
* [ ] 2  


__Question 2__  
On considère la fonction suivante :
```python
def fonction_mystere_bis(a):
    b = 3
    return b + a / 3
```
Quelle est la valeur de retour pour `fonction_mystere_bis(9)`?  
* [ ] 4   
* [ ] 4.0   
* [ ] 6.0  
* [ ] 6  


__Question 3__  
On considère le code suivant :
```python
from math import sqrt

def pythagore(a, b):
    """ Renvoie la longueur de l'hypoténuse"""
    return nom_fonction(a * a + b * b)
```

Par quoi remplacer `nom_fonction` ?  
* [ ] math.sqrt    
* [ ] sqrt   
* [ ] sqrt.math  
* [ ] math  


__Question 4__  
On considère la fonction `f()` suivante:
```python
def f(a, b, c, d):
    a = b
    c = d
    return b ** b + c * d
```
Quelle est la valeur de retour pour `f(3, 2, 1, 4)`?  
* [ ] 20    
* [ ] 28   
* [ ] 8  
* [ ] 9  


---
_Sources_

[^1]: [Light year by Bob King](http://www.skyandtelescope.com/observing/how-to-see-the-farthest-thing-you-can-see090920150909/)

[^2]: [20-sided dice, Photo by Fantasy on the English Wikipedia project](https://commons.wikimedia.org/wiki/File:20-sided_dice_250.jpg?uselang=fr)