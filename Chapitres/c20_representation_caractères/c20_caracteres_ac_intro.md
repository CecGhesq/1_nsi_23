---
title: "Chapitre 20 : Représentation des caractères en machine "
subtitle: " Activité : codage des caractères "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

_« Le codage des caractères est une convention qui permet, à travers un codage connu de tous, de transmettre de l’information textuelle, là où aucun support ne permet l’écriture scripturale. Cela consiste à représenter chaque caractère, par un motif visuel, un motif sonore, ou une séquence abstraite. Les techniques des ordinateurs reposent sur l’association d’un caractère à un nombre. »_
(source Wikipedia)

# Le Code ASCII

> 📢 L'American Standard Code for Information Interchange (Code américain normalisé pour l'échange d'information), plus connu sous l'acronyme __ASCII__, est une norme informatique de codage de caractères apparue dans les années 1960 permettant de représenter les caractères utilisés notamment en latin et en anglais.

On fournit le fichier [ASCIItable1972.pdf](./files/ASCIItable1972.pdf)   tiré d’un manuel d’imprimante de 1972.

![codage lettre A](./fig/lettre_A.jpg)

__1)__ Combien de bits sont utilisés pour encoder un caractère en ASCII ?   
__2)__ Combien de caractères sont ainsi encodables ?  

Dans la pratique les ordinateurs traitent les bits par 8 (octet). __Un bit toujours égal à 0 est ajouté en huitième position des codes précédents__ (la première dans le sens de lecture) ce qui donne par exemple pour le caractère `A` :   __0__`1000001`

__3)__ Décoder le message suivant :

```
01001100 01100101 00100000 01100010 01101001 01101110 01100001 01101001 01110010 01100101 00101100 00100000 01100011 00100111 01100101 01110011 01110100 00100000 01101001 01101110 01101000 01110101 01101101 01100001 01101001 01101110 00101110
```

__4)__ Pour plus de facilité, le codage des caractères peut être donné en base 10 (décimal) ou en base 16 (hexadécimal).

__a-__ Retrouver (et justifier) par le calcul le codage décimal du caractère `@`

__b-__ Quel caractère correspond au codage hexadécimal `3D`? (Justifier par le calcul)

__c-__ Observez la table ASCII fournie :    [bases_2_8_10_ASCII.pdf](./files/bases_2_8_10_ASCII.pdf) et vérifier vos conversions

---

Certains caractères ne sont pas imprimables : en effet plusieurs codes utilisés auparavant permettaient de signaler le début ou la fin de la transmission par exemple. A l’heure actuelle ils n'ont plus leur utilité dans les textes codés sur supports numériques. Il en reste cependant....

__5)__ Enregistrez le fichier [`appli3.txt`](./files/appli3.txt) puis ouvrez-le à l’aide de  __Notepad++__ (ou _Atom_, ou _VisualStudio_ suivant votre préférence), voici son contenu : 

![appli3.txt sur Notepad++](./fig/appli3_contenu.jpg)

__6)__ Trouver à l’aide de l’Explorateur de votre système d'exploitation (clic droit Propriétés sous Windows) la taille exacte occupée (en octets) par ce fichier.

![Propriétés du fichier](./fig/proprietes.jpg)

Justifier cette taille précisément.

__7)__ Rajoutez un saut de ligne juste avant le mot « peut » de manière à obtenir :

Un saut de ligne peut se coder.

![appli3.txt avec saut de ligne](./fig/appli3_saut_de_ligne.jpg)

Enregistrez le fichier modifié.  
__Quelle est la nouvelle taille ?__

__8)__ Dans le menu Affichage de Notepad++ sélectionnez : Symboles spéciaux > Afficher tous les caractères 

__a-__	Deux caractères de contrôle s’affichent lesquels ?  
__b-__	Quels sont leurs codes décimaux ?

__9)__ Recherchez l'origine de ces caractères (en fait ce sont des acronymes de mots anglais...) __et__ la manière dont sont codés les sauts de ligne sous 3 types de systèmes d'Exploitation (Windows, MacOS, Linux) 

# Les extensions de l'ASCII

L'ASCII est simple est ne comporte que le strict nécessaire pour l'anglais. D'autres pays l'ont réutilisé pour le compléter avec les caratères utilisés dans d'autres langues. Après une période où chaque pays utilisait sa propre norme (la compatibilité était parfois compliquée !) sont nées les normes ISO-8859 pour les langues latines.

## La norme ISO-8859-1

En ASCII seuls 7 bits sont réellement utilisés comme on l'a vu, on peut donc utiliser le 8ème bit pour augmenter le nombre de caractères encodés 

__10)__ Combien de caractères supplémentaires peuvent ainsi être ajoutés ?

Les caractères supplémentaires ne sont pas forcément les mêmes pour toutes les langues, ont ainsi été créés plusieurs dérivés de la norme __ISO-8859-n__ ou n est le numéro de la partie de la norme.
A l'heure actuelle il existe 16 parties : de ISO-8859-1 (la plus largement utilisée : Allemand, anglais, espagnol, français...) à ISO-8859-16 (albanais, croate, ...)

La norme __ISO-8859-1__ est aussi appelée __Latin-1__. Voici la table d'encodage correspondante:

![Encodage Latin-1[^1] ](./fig/latin1.jpg)

Dans cette table les valeurs sont exprimées en hexadécimal (on lit d'abord l'en-tête de la ligne puis celle de la colonne).
Par exemple le caractère `J` est codé $`4A_{16}`$

__11)__ Est-ce le même code en ASCII ?  

__12)__ La table ASCII est-elle compatible avec la table Latin-1 ? (le vérifier pour plusieurs caractères en donnant des exemples)  

__13)__ Quels caractères très utilisés en français sont rajoutés par cet encodage ?  

__14)__ On s'intéresse plus particuliérement au caractère spécial `NBSP`.  
__a-__ Quel est son encodage décimal ?  
__b-__ Cherchez son utilité en donnant un exemple.


## Encodage par défaut sous Windows

Au début des années 90, Microsoft développe un codage nommé __Windows-1252__ (aussi connu sous le nom de _CP1252_ ou encore _ANSI_).

![Encodage Windows1252 [^2] ](./fig/windows1252.jpg)

__15)__ Cherchez le caractère `=` dans le tableau ci-dessus.

__a-__ Retrouvez sa valeur en décimal  
__b-__ Est-ce le même code en ASCII ?  
__c-__ Vérifiez pour d'autres caractères. Ces normes d'encodage sont-elles compatibles ?    
__d-__ Et avec le Latin-1 ? Donner des exemples le justifiant et expliquant les différences.  

![Caractères spéciaux sous Windows[^3] ](./fig/windows_caracteres.jpg)

__16)__ Essayez dans un traitement de texte d'insérer par la méthode précédemment décrite le caractère `Æ`. Quel combinaison de touches faut-il utiliser ? 

Remarques: 

* sur Windows :  il faut maintenir la touche_ Alt_ puis taper le code avec **le pavé numérique** ; si vous n'en avez pas sur un ordinateur portable il sera peut être nécessaire d'associer _Alt+Fn+code numérique_.
  
* sur Linux : la touche _compose_ doit être utilisée ; le plus souvent elle est associée à _Alt_ ou _Win_ mais peut être aussi choisie par l'utilisateur
* sur Mac-OS : Touche Option (Alt)


## Compatibilité des normes ISO-8859-n ?

Ouvrez le fichier [`index1.html`](./files/index1.html) avec Firefox et visualisez le code source.

__17)__ Quel encodage a été utilisé pour ce fichier ? Précisez où se situe l'information dans le code HTML

Votre navigateur est à priori en détection automatique d'encodage, c'est à dire qu'il utilise les informations fournies par le code HTML pour utiliser la bonne table d'encodage.

__18)__ Modifiez l'encodage du navigateur `Affichage/Encodage` du texte ( utiliser `Alt `pour faire apparaître la barre d'outils )  et choisissez __Europe centrale (ISO)__.  
__a-__ Qu'observez vous ?    
__b-__ Recherchez à quelle norme ISO-8859 cet encodage correspond ?    
__c-__ Les normes ISO-8859 sont-elles compatibles entre-elles ? Donner plusieurs éléments de réponse.(Il faudra notamment comparer les tables d'encodages)  


# Vers l’Unicode

Les diverses extensions du code ASCII présentent l’inconvénient d’être incompatibles entre elles, et le plus souvent d’être spécialisées pour un jeu de caractères lié à une langue. Comment alors coder dans un même document des textes rédigés avec des alphabets aussi divers que les alphabets latin, cyrillique, grec, arabe, ... ? [^4]

Le mot `paix` de par le monde  :
 
* arabe : السلم 	
* français : paix 	
* grec : ειρήνη
* hébreu : שלום 	
* japonais : へいわ 	
* russe : мир
* symbole : ☮ 	
* tchèque : mír 	
* thai : ความสงบสุข

C’est pourquoi Unicode a vu le jour au début des années 1990.  
Unicode dans sa dernière version 12.0 (Mars 2019) permet d’encoder près de 200 000 caractères tous identifiés par un point de code de forme `U+XXXXXX`  (où __XXXXXX__ représente un code hexadécimal plus ou moins long).            

https://vimeo.com/48858289


__19)__	Ouvrez LibreOffice Writer (_ou un Traitement de texte équivalent_). 

__a-__ Dans le menu `Insertion> Caractères spéciaux`, trouvez le point de code du symbole __♫__ placé dans le sous-ensemble Symboles divers  
__b-__ Retrouvez le code décimal équivalent (En justifiant)  
__c-__ Ce code décimal existe-t-il dans la norme ISO-8859? Justifier

__20)__ Toujours en utilisant le même logiciel trouver les caractères correspondant au sous-ensemble Latin-1.

__a-__ Donnez les points de code du premier et du dernier caractère de ce sous ensemble (Ils sont placés dans l'ordre : lecture de gauche à droite et de haut en bas)  

__b-__ Les valeurs décimales sont-elles compatibles avec celles de la norme ISO-8859-1 ?

Unicode se contente de recenser, nommer les caractères et leur attribuer un numéro. Mais il ne dit pas comment ils doivent être codés en informatique.

Plusieurs codages des caractères Unicode existent UTF-32, UTF-16 et le plus célébre UTF-8

Dans __UTF-8__  chaque caractère peut être codé sur 8, 16, 24 ou 32 bits 

__21)__ Combien d'octets peuvent être utilisés par un seul caractère en __UTF-8__ ?

__22)__ On lit dans une table __UTF-8__ que le caractère `é` est codé sur deux octets par les valeurs hexadécimales __C3 A9__.
Expliquez alors l'image suivante en utilisant des tables d'encodage précédentes . 

![Martine](./fig/martine.jpg)

__23)__ Le caractère `€` sur 3 octets par les valeurs hexadécimales __E2 82 AC__.

Soit un fichier contenant le texte `"J'écris € en UTF-8"` codé en UTF-8. Quel sera le texte affiché si un logiciel décode ce texte en supposant que le codage utilisé est __Latin-1__ ?


[^1]: https://fr.wikipedia.org/wiki/ISO/CEI_8859-1

[^2]: https://fr.wikipedia.org/wiki/Windows-1252

[^3]: https://zestedesavoir.com/tutoriels/pdf/1114/comprendre-les-encodages.pdf

[^4]: https://www.fil.univ-lille1.fr/~L1S1Info/Doc/HTML/seq7_codage_caracteres.html