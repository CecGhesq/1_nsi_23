---
title: "Chapitre 20 : Représentations des caractères en machine "
subtitle: " Cours "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# ASCII[^1]

__ASCII__ (_American Standard Code for Information Interchange_) est la première norme largement utilisée pour encoder des caractères.  Comme son nom l'indique cette norme est américaine et elle n'inclut donc que les lettres latines non accentuées (en plus des chiffres, opérateurs mathématiques, caractères de ponctuation ou de délimitation et certains caractères spéciaux).  


|     | `0`     | `1`   | `2`   | `3`   | `4`   | `5`   | `6`   | `7`   | `8`   | `9`  | `A`   | `B`   | `C`  | `D`  | `E`  | `F`  |  
|-----|---------|-------|-------|-------|-------|-------|-------|-------|-------|------|-------|-------|------|------|------|------|  
| `0` | `NUL`   | `SOH` | `STX` | `ETX` | `EOT` | `ENQ` | `ACK` | `BEL` | `BS`  | `HT` | `LF`  | `VT`  | `FF` | `CR` | `SO` | `SI` |  
| `1` | `DLE`   | `DC1` | `DC2` | `DC3` | `DC4` | `NAK` | `SYN` | `ETB` | `CAN` | `EM` | `SUB` | `ESC` | `FS` | `GS` | `RS` | `US` |  
| `2` | `ESP`   | `!`   | `"`   | `#`   | `$`   | `%`   | `&`   | `'`   | `(`   | `)`  | `*`   | `+`   | `,`  | `-`  | `.`  | `/`  |  
| `3` | `0`     | `1`   | `2`   | `3`   | `4`   | `5`   | `6`   | `7`   | `8`   | `9`  | `:`   | `;`   | `<`  | `=`  | `>`  | `?`  |  
| `4` | `@`     | `A`   | `B`   | `C`   | `D`   | `E`   | `F`   | `G`   | `H`   | `I`  | `J`   | `K`   | `L`  | `M`  | `N`  | `O`  |  
| `5` | `P`     | `Q`   | `R`   | `S`   | `T`   | `U`   | `V`   | `W`   | `X`   | `Y`  | `Z`   | `[`   | `\`  | `]`  | `^`  | `_`  |  
| `6` | \` | `a`   | `b`   | `c`   | `d`   | `e`   | `f`   | `g`   | `h`   | `i`  | `j`   | `k`   | `l`  | `m`  | `n`  | `o`  |  
| `7` | `p`     | `q`   | `r`   | `s`   | `t`   | `u`   | `v`   | `w`   | `x`   | `y`  | `z`   | `{`   | `|`  | `}`  | `~`  | `DEL`|  


> 📢 128 caractères composent la table ASCII, ce qui permet de les représenter sur 7 bits (en pratique plutôt 8 bits afin d'occuper un octet complet).
> Les 32 premiers caractères sont appelés __caractères de contrôle__ : ce sont des caratères non imprimables. 

# ISO-8859-n[^1]

Par la suite d'autres encodages ont vu le jour afin de pallier les limites de l'ASCII.  L'ISO-8859-1 (aussi appelé _Latin-1_), pour l'Europe occidentale, a vu le jour en 1986.  Celui-ci comble les manques pour la plupart des langues d'Europe occidentale.  Pour le français il manque cependant le `œ`, le `Œ` et le `Ÿ` et, bien entendu, le symbole `€`.

Voici [la table des caractères ISO-8859-1](http://std.dkuug.dk/jtc1/sc2/wg3/docs/n411.pdf) :

![ISO-8859-1](./fig/latin1.jpg)


D'autres encodages suivant cette norme ont été créés prenant chacun en charge des ensembles de langues différents.

> 📢 __Les encodages suivant la norme ISO-8859 utilisent 8 bits__, les 128 premières valeurs de l'ISO-8859-n sont compatibles avec l'ASCII. Par contre, les différents encodages ISO-8859-n ne sont pas compatibles entre eux notamment en ce qui concerne les 128 derniers caractères.


# Unicode et codage UTF-8[^1]

À nouveau le codage ISO-8859-1 (et les autres codages de la famille ISO-8859) présentent des limites.  Dans les années 1990, le projet Unicode de codage unifié de tous les alphabets est né. 

> 📢 `Unicode` est un standard qui associe à n'importe quel caractère de n'importe quel système d'écriture un nom et un identifiant numérique appelé __point de code__ de la forme __U+xxxx__ (_xxxx est un hexadécimal de 4 à 6 chiffres_).

Différents codages sont utilisés pour représenter des caractères Unicode (UTF-8, UTF-16, UTF-32) utilisant plus ou moins de bits. Ici nous nous concentrons sur l'UTF-8.

__Le codage UTF-8 est un codage de longueur variable__ :

* Certains caractères sont codés sur un seul octet, ce sont les 128 caractères du codage ASCII.  
* Les autres caractères peuvent être codés sur 2, 3 ou 4 octets.  

Ainsi l'UTF-8 permet en théorie de représenter $`2^{21} = 2\,097\,152`$ caractères différents, en réalité un peu moins.   
Il y a actuellement environ une centaine de milliers de caractères Unicode (incluant les [caractères des langues vivantes ou mortes](https://unicode.org/cldr/charts/latest/supplemental/languages_and_scripts.html) et également de [nombreux emojis indispensables](https://unicode.org/emoji/charts-12.0/full-emoji-list.html)😇)  

Les caractères en UTF-8 doivent avoir une forme particulière décrite dans la table ci-dessous :

| Nbre octets codant | Format de la représentation binaire   |
|--------------------|---------------------------------------|
| 1                  | `0xxxxxxx`                            |
| 2                  | `110xxxxx 10xxxxxx`                   |
| 3                  | `1110xxxx 10xxxxxx 10xxxxxx`          |
| 4                  | `11110xxx 10xxxxxx 10xxxxxx 10xxxxxx` |

Par exemple[^2] :

![Quelques caratères codés en UTF-8 ](./fig/ex_utf8.jpg)

> 📢 L'encodage UTF-8 est lui aussi compatible avec l'ASCII. En revanche les encodages de la norme ISO-8859 et UTF-8 sont incompatibles entre eux.
> Pour les langues utilisant beaucoup de caractères UTF-8 nécessite moins d'octets que UTF-16 ou UTF-32.


[^1]: DIU Enseigner l'informatique (Université de Lille) https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/blob/master/bloc1/representations_donnees/readme.md  

[^2]: http://www.dg77.net/tekno/xhtml/codage.htm

