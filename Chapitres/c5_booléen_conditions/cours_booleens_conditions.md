---
title: "Chapitre 5 : Opérateurs booléens et structures conditionnelles"  
subtitle: "Cours"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

Les variables booléennes déjà rencontrées précedemment sont utilisées pour représenter des valeurs de vérité (vrai ou faux) de __propositions__ ( c'est à dire un énoncé formé d'assemblage de symboles et de mots) : c'est la base du raisonnement logique.  

Ces propositions peuvent être combinées à l'aide de __connecteurs__ formant ainsi des __expressions logiques__. 

# OPERATIONS LOGIQUES

## La négation : NON 

__📢 Définition :__    

> La __négation__ d'une proposition __NON P__ est VRAI si et seulement si P est FAUX.  
 Plusieurs connecteurs peuvent être utilisés pour la représenter : __not__, $`\lnot`$, $`\bf^{-}`$   

__📢 la table de vérité :__  

> Une __table de vérité__ est un tableau représentant dans les colonnes de gauche les valeurs de vérités des propositions et dans la colonne de droite la valeur de vérité de l'expression logique.  


Table de vérité de la __négation__:  

|P|$`\lnot P`$|    
|:---:|:---:|                 
|FAUX|__VRAI__|    
|VRAI|__FAUX__|     

## La conjonction : ET

__📢 Définition :__ 

> La __conjonction__ de deux propositions __P ET Q__ est VRAI si et seulement si P est VRAI et Q est VRAI.  
Plusieurs connecteurs peuvent être utilisés pour la représenter : __and__, $`\bf \land`$, __.__   


Table de vérité de la __conjonction__:  

|P|Q|$`P \land Q`$|    
|:---:|:---:|:---:|        
|FAUX|FAUX|__FAUX__|       
|FAUX|VRAI|__FAUX__|   
|VRAI|FAUX|__FAUX__|    
|VRAI|VRAI|__VRAI__|  


## La disjonction : OU

__📢 Définition :__ 

> La __disjonction__ de deux propositions __P OU Q__ est vraie lorsqu'au moins une des propositions est vraie, et est fausse quand les deux sont simultanément fausses.  
Plusieurs connecteurs peuvent être utilisés pour la représenter : __or__ , __$`\bf \lor`$__, __+__

Table de vérité de la __disjonction__:  

|P|Q|$`P \lor Q`$|    
|:---:|:---:|:---:|        
|FAUX|FAUX|__FAUX__|       
|FAUX|VRAI|__VRAI__|   
|VRAI|FAUX|__VRAI__|    
|VRAI|VRAI|__VRAI__|    
  


## Expressions booléennes

En Python les booléens sont associées à trois __opérations booléennes__ correspondant aux opérateurs précédents.  

| Opérateurs logiques |Opérateurs utilisés en Python |   
|:---:|:---:|        
|NON|`not`|      
|ET|`and`|   
|OU|`or`|  


Ces opérateurs permettent de combiner plusieurs tests de comparaisons.

```python
>>> age = 20
>>> inscrit_liste_electorale = True
>>> age >= 18 and inscrit_liste_electorale 
 True
```
__📢 Définition :__  

>Il existe des priorités lors de l'emploi de ces opérateurs : `not` est le plus prioritaire, puis vient `and` et enfin `or`.  

Les expressions booléennes permettent, nous allons le voir de contrôler le flux d'exécution d'un programme afin d'aiguiller son déroulement en fonction des conditions rencontrées.

# STRUCTURES CONDITIONNELLES

## L'instruction SI 

```python
>>> m = 10
>>> if m >= 10:
        print('Vous avez la moyenne')

Vous avez la moyenne
```
En tapant ce code remarquez l'indentation automatiquement au début de la troisième ligne.  
Comme après l'entête d'une fonction ou d'une boucle tant que, la suite du code représente un bloc indenté correspondant ici aux instructions à réaliser __si__ l'expression booléenne est évaluée à True.  
* On peut bien sûr ajouter autant d'instructions que l'on souhaite.  
* Pour sortir du bloc, il suffit de revenir 4 espaces plus à gauche.

```python
>>> m = 10
>>> if m >= 10:
        print('Vous avez la moyenne')
        print('Bravo')
    print('Votre note est : ' + str(m))

Vous avez la moyenne
Bravo
Votre note est 10
```
On en déduit qu'après l'évaluation de la condition associé à l'instruction __if__ et l'exécution éventuelle du bloc de code, le flux normal du programme reprend.  
On peut tester un cas où cette condition n'est pas vérifiée :

```python
>>> m = 9
>>> if m >= 10:
        print('Vous avez la moyenne')
        print('Bravo')
    print('Votre note est : ', m)

Votre note est 9
```

On peut résumer ainsi l'utilisation de cette instruction conditionnelle :

```
if <condition>:
    <instructions>
<suite du programme>
```
Il est également possible de représenter ce déroulé par un __algorigramme__  :

![flux if](./fig/flow_if.png)  


## L'instruction SI SINON

Soit le code suivant :

```python
>>> langage = input('Quel langage est utilisé pour ce programme ? ')
>>> if langage.upper() == 'PYTHON':
        print('Bonne réponse')
    else:
        print('Mauvaise réponse')
```

> 💾 La fonction native `input()` permet d'afficher une chaîne de caractères passée en paramètre (_souvent utilisé pour afficher une question_) puis d'interrompre le programme en attendant que l'utilisateur saisisse des données (_une réponse à la question_).    
Les caractères saisis seront convertis en une __chaîne__ qui sera la valeur de retour de la fonction.  
 Très  souvent la syntaxe privilégiée pour utiliser cette fonction sera de la forme :       `reponse = input('question')`

Dans le programme précédent la réponse donnée par l'utilisateur est sauvegardée dans la variable langage.  

On reconnait l'instruction if qui est cette fois suivie d'un nouveau bloc de code utilisant le mot clé `else` (__sinon__).   
Après ce mot clé aucune condition n'est à fournir en effet seules deux cas sont envisageables :
- la condition  initiale est respectée
- la condition initiale n'est pas respectée

On peut schématiser la situation ainsi :

```
if <condition>:
    <instructions_1>
else:
    <instructions_2>
<suite du programme>
```

![flux if else](./fig/flow_if_else.png)  


## L'instruction SI SINON-SI

Une dernière structure est envisageable, celle où il est important de procéder à des vérifications successives parmis lesquelles au maximum un seul bloc d'instructions  sera traité
```
if <condition_1>:
    <instructions_1>
elif <condition_2>:
    <instructions_2>
<suite du programme>
```

Par exemple :
```python
>>> a = 3
>>> if a == 1:
        print(' a vaut 1')
    elif a == 2:
        print('a vaut 2')
    print('fin du programme')

fin du programme
```

L'algorigramme correspondant :

![flux if elif](./fig/flow_elif.png)  

Toutes les structures conditionnelles précédentes peuvent bien évidemment être combinées entre elles et répétées ce qui permettra des combinaisons très variées dans les isntructions à réaliser par le programme.


```python
>>> a = 3
>>> if a == 1:
        print(' a vaut 1')
    elif a == 2 :
        print('a vaut 2')
    else:
        print('a ne vaut ni 1 ni 2')

a ne vaut ni 1 ni 2
```




