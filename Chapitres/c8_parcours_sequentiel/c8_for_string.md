---
title: "Chapitre 8 : parcours des chaînes"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Une chaîne de caractères est une séquence d'éléments pouvant être parcourue par une boucle.  

# Itération sur les indices

On se rappelle que les caractères d'une chaîne sont accessibles par leur indice.  
Par exemple pour la chaîne `s = 'python'`

|s[0]|s[1]|s[2]|s[3]|s[4]|s[5]|  
|:---:|:---:|:---:|:---:|:---:|:---:|  
|'p'|'y'|'t'|'h'|'o'|'n'|  

Nous pouvons donc utiliser la fonction `range()` pour parcourir notre chaîne caractère par caractère en utilisant leur indice respectif.  

```python
>>> chaine = "Numérique !"
>>> for i in range(0, len(chaine), 1):
        print(chaine[i], '*', end=" ")
 
 N * u * m * é * r * i * q * u * e *   * ! * 
```

* La variable de boucle nommée `i` correspond à l'indice : dans cet exemple sa première valeur sera __0__.   
    
* Le deuxième paramètre __len(chaine)__ donnera la longueur de la chaine c'est à dire 11 (La variable `i` ne prendra pas cette valeur).  
    
*  L'indice du dernier caractère de la chaîne est 10 : la chaîne sera bien parcourue jusqu'au bout  
  
*  Le pas est bien de 1 pour parcourir tous les caractères  

__La variable `i` prendra successivement les valeurs entiers générés par `range` (qui correspondent aussi aux indices de `chaine`).__  

Cet exemple peut être simplifié en :

```python
for i in range(len(chaine)):
    print(chaine[i], '*', end=" ")
```
 


💾 _Attention_ : si les paramètres de `range()` sont mal choisis une __exception__ `IndexError` est levée !

```python
>>> for i in range(12):
        print(chaine[i], '*', end=" ")

N * u * m * é * r * i * q * u * e *   * ! * Traceback (most recent call last):
  File "C:\Users\nsi.DESKTOP\Cours_boucles\test.py", line 3, in <module>
    print(chaine[i], '*', end=" ")
IndexError: string index out of range
```

__📢 Définition :__  

> Quand Python détecte une erreur dans votre code, il lève une __exception__.  
> _Vous en avez sans doute rencontrer quelques unes_ : `IndentationError`, `ImportError`, `NameError`, `IndexError`, `TypeError`...





# Itération sur les caractères

La parcours d'une séquence est une opération très fréquente en programmation, on peut faciliter l'écriture de la boucle précédente en itérant non pas sur les indices des caratères mais sur les caractères eux-mêmes.  

```python
>>> chaine = "Numérique !"
>>> for car in chaine:
        print(car, '*', end=" ")

 N * u * m * é * r * i * q * u * e *   * ! *  
```

__Cette fois-ci la variable de boucle est `car` et, la séquence n'est plus numérique (comme avec `range`) mais elle correspond à la chaîne elle-même.__  

* La chaîne de caractères est parcourue de gauche à droite caractère par caractère.  
  
* La variable de boucle prendra successivement la valeur de ces caractères.  

Comme vous pouvez le constater, cette structure est beaucoup plus compacte, plus lisible et ne pourra pas jamais lever l'exception précédente.    
Cependant,  en procédant ainsi les indices des caractères ne sont pas accessibles : tout dépend donc de l'information dont on a besoin !



