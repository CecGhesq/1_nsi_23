---
title: "Chapitre 8 : Parcours séquentiel"
subtitle : "TP  : Parcours de chaînes de caractères"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n°1: Recherche de caractère &#x1F3C6;  

__1)__ Ecrire un algorithme qui parcourt une chaîne de caractères et qui repère si un caractère spécifique est présent dans la chaîne.

_Par exemple_: On envoie la chaîne "Bonjour" et on recherche la lettre "o" :  la valeur de retour sera `True`, si on recherche "e" : `False`.

__2)__ Vous définirez un prédicat `recherche_caractere` traduisant en Python l'algorithme précédent.

__3)__ Python propose une solution encore plus simple pour répondre à ce problème.
Testez les deux instructions suivantes

```python
>>> 'n' in 'nsi' 

>>> 'f' in 'nsi'
```


# Exercice n°2: Compter les caractères &#x1F3C6;  

__1)__ Ecrire un algorithme qui parcourt une chaîne de caractères et qui compte le nombre de fois qu'un caractère figure dans la chaîne. 

__2)__ Vous définirez une fonction `compter_caractere` acceptant deux paramètres et traduisant en Python l'algorithme précédent.

# Exercice n°3  Découper une chaîne &#x1F3C6; &#x1F3C6;  

Définir une fonction `premier_mot(chaine)` qui renvoie le premier mot d’une chaîne de caractère. 

_Par exemple_  

```python
>>> chaine = 'samedi soir, je vais au cinéma'
>>> premier_mot(chaine)

 'samedi'
```

_Consignes_ : __vous n'avez pas le droit d'utiliser la méthode `find()`.__ 

# Exercice n°4: Verlan &#x1F3C6; &#x1F3C6;  

Écrire une fonction `verlan(mot)` qui prend en argument un mot et renvoie le mot écrit à l’envers.    

_Par exemple_  

```python
>>> verlan('nez')

 'zen'
```

_NOTE_ : Le programme écrit ici ne réalise pas du "vrai" verlan mais inverse simplement l'ordre des caractères dans la chaîne

# Exercice n°5: Anagrammes &#x1F3C6; &#x1F3C6; &#x1F3C6;  

> L’anagramme consiste à mélanger les lettres d’un mot, d’une expression, de manière à former un nouveau mot.
> _(source : https://fr.wiktionary.org/wiki/anagramme)_

__1)__ Ecrire un algorithme qui compare deux chaînes de caractères et vérifie si ces chaînes sont des anagrammes.

__2)__ 🥇 Vous définirez un predicat `sont_anagrammes` traduisant en Python l'algorithme précédent. (vous pourrez utiliser l'opérateur `in` comme vu dans l'exercice 1)

# Exercice n°6: Palindrome &#x1F3C6; &#x1F3C6; &#x1F3C6;  

> Un palindrome est une chaîne de caractères qui est identique qu'on la lise de gauche à droite ou de droite à gauche.  
> _Par exemple_: "été", "ressasser", "radar", "kayak" sont des palindromes.


__Ecrire une fonction qui vérifie si une chaîne de caractère est un palindrome.__







