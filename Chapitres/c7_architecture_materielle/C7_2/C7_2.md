---
title: "Chapitre 7_2 : Algèbre de Boole et circuits combinatoires"  
subtitle: "Chapitre 7 : Architecture de l'ordinateur"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# L'algèbre de Boole

## Un peu d'histoire  

C'est le mathématicien britannique **George Boole** qui a réalisé cette algèbre à l'aube du 20-ème siècle. Boole a noté des similitudes entre le calcul algébrique sur les nombres comme l'addition et la multiplication, et le calcul en logique propositionnelle avec la disjonction et la conjonction de valeurs de vérité et a fait le pont entre ces deux visions. Il a en quelque sorte algébrisé la logique.

Les applications de l'algèbre qui porte aujourd'hui son nom sont innombrables et centrales en informatique, en automatique et en électronique. Les circuits qui constituent un ordinateur s'appuient sur des circuits logiques qui font exclusivement du calcul booléen à partir de portes logiques. C'est le mathématicien **Claude Shannon** qui a été l'un des premiers à appliquer l'algèbre de Boole à la conception des circuits. Les calculs réalisés par les circuits logiques sont la matérialisation des calculs abstraits réalisés en algèbre de Boole.  

## Les connecteurs logiques

**Un peu de vocabulaire:**  

 Une **proposition** est un énoncé auquel on peut attribuer une valeur de vérité **vrai ou faux**.  

 Une **assertion** est une proposition énoncée comme vraie. Une assertion est donc une affirmation.  

 On appelle **interprétation d'une proposition** toute fonction de l'ensemble des variables propositionnelles qu'elle contient dans l'ensemble {vrai,faux}.  

Deux formules E1 et E2 qui ont même valeur de vérité pour toute interprétation I de leurs variables sont dites logiquement équivalentes, ce que l'on note E1 $`\equiv`$ E2. Le symbole $`\equiv`$ est appelé équivalence logique.

À partir des variables propositionnelles, on peut créer de nouvelles propositions grâce aux `connecteurs logiques` qui les mettent en relation dans une formule.
Les connecteurs logiques sont ainsi :  

![connecteurs logiques](img/connecteurs_logiques_rz.png)  

## Les tables de vérités

Le nombre de combinaisons dans une table de vérité dépend du nombre de variables d’entrée. Pour « n » entrées, il y aura  $`2^n`$ combinaisons possibles.
Exemple :

* Si on a 2 variables d’entrée (n = 2) donc on a  $`2^2`$ combinaisons soit 4 combinaisons
* Si on a 3 variables d’entrée (n = 3) donc on a  $`2^3`$ combinaisons soit 8 combinaisons
* Si on a 4 variables d’entrée (n = 4) donc on a  $`2^4`$ combinaisons soit 16 combinaisons

Si on veut attribuer un sens à une formule, on veut l'interpréter. Seules deux valeurs logiques sont possibles, vrai ou faux, c'est la valeur de vérité de la formule.

**Nous avons déjà vu  les trois premières tables** interprétant les fonctions non, et, ou.  

* La table negation  NOT (NON ; $`\overline{e}`$  ; $`\lnot`$ )  

On associe le chiffre **0 à faux et 1 à vrai**.

|a|$`\overline{a}`$|
|:---:|:---:|
|0|__1__|
|1|__0__|

* La table conjonction AND (ET ; . ; $`\land`$ )  
  
|a|b|$`a \land b`$|
|:---:|:---:|:---:|
|0|0|__0__|
|0|1|__0__|
|1|0|__0__|
|1|1|__1__|  

* La table  OR (OU ; +, $`\lor`$)  

On qualifie ce "ou" de inclusif car si a et b sont tous les deux égaux à 1, a $`\lor`$ b est égal à 1.
  
|a|b|$`a\lor b`$|
|:---:|:---:|:---:|
|0|0|__0__|
|0|1|__1__|
|1|0|__1__|
|1|1|__1__|  

## Comment lire une table de vérité

![lecture de la table de vérité](./img/lecture_table_verite.PNG)

La table de vérité permettait de donner l’état logique de la variable de sortie en fonction de toutes les combinaisons possibles des états logiques des variables d’entrée. Pour déterminer l’équation de la variable de sortie, il faut :

* Ecrire la combinaison des entrées pour chaque ligne où la sortie est égale à 1.
* Appliquer une fonction « ou » entre chaque ligne.
  
On peut ainsi écrire  S = $`(\lnot  e1 \land e2 \land \lnot e3) \lor (\lnot e1 \land e2 \land e3) \lor (e1\land e2 \land \lnot e3) \lor (e1 \land e2 \land e3)`$

# Les portes logiques

##  Histoire et définition

**Charles Babbage**, vers 1837, conçut la « machine analytique », assemblage de portes reliées à des roues dentées pour effectuer des opérations logiques. Par la suite, les opérations logiques furent effectuées grâce à des relais électromagnétiques.  

En 1891, **Almon Strowger** déposa un brevet pour un appareil contenant un commutateur basé sur une porte logique. Son invention ne fut guère exploitée jusque dans les années 1920. À partir de 1898, **Nikola Tesla** déposa une série de brevets concernant des appareils basés sur des circuits à portes logiques. Finalement, les  `tubes à vides` remplacèrent les relais pour les opérations logiques. En 1907, Lee De Forest modifia l'un de ces tubes et l'utilisa comme une porte logique ET. **Claude E. Shannon** introduisit l'utilisation de l'algèbre de Boole dans la conception de circuits en 1937. **Walther Bothe**, inventeur du circuit de coïncidence, reçut le prix Nobel de physique en 1954, pour la création de la première porte logique ET électronique moderne en 1924. Des travaux de recherche sont actuellement menés pour la génération de portes logiques moléculaires.  

A l'échelle microscopique, l'ordinateur peut se décrire comme un assemblage de transitor. Un transistor est un circuit électronique à trois fils appelés *le drain, la source et la grille*.Selon les tensions appliquées entre la grille et la source, **le transistor peut être bloqué ou passant : grâce à lui on a créé un interrupteur, à la base des portes logiques**.

Les portes peuvent être classées suivant leur nombre d'entrées :

* « portes » sans entrée : VRAI, FAUX ;
* portes à une entrée : NOT, YES ;
* portes à deux entrées : AND , NAND, OR, NOR, OU exclusif (XOR), Coïncidence (informatique) dite aussi NON-OU exclusif ou équivalence (XNOR), implication ;  

À partir de trois entrées, le nombre de fonctions commence à subir l'influence de l'explosion combinatoire. On note toutefois l'existence de : ET, OU, etc. à plus de deux entrées.  
Il est possible de reconstituer les fonctions NOT, AND et OR en utilisant uniquement soit la fonction ~NAND~, soit la fonction ~NOR~. **On évoque cette caractéristique sous la notion d'universalité des opérateurs NON-OU et NON-ET.**  

Lorsqu'on associe deux portes logiques compatibles, on peut connecter deux entrées ensemble, ou une entrée sur une sortie. Il ne faut en aucun cas connecter deux sorties différentes car elles peuvent produire des données différentes ; dans le cas de portes électroniques, cela équivaudrait à un court-circuit.  

##  Les portes logiques NOT , AND et OR  

Les portes logiques sont représentées par des graphiques. Il existe deux normes : la norme ANSI/IEEE 91-1984 et son supplément 91a-1991(nommée symbole américain) ainsi que  la norme CEI 60617-12( symbole européen).  

|Fonction logique   |Utilité de la porte   | Symbole américain  | Symbole européen |  Equivalence électrique |
|:---:|:---:|:---:|:---:|:---:|
|NOT| Lorsqu'il s'agit d'inverser l'information : le VRAI devient FAUX et le FAUX devient VRAI. On appelle cela également **complémenter.** |![symbole américain](./portes_logiques/NOT_a.PNG) | ![symbole européen](./portes_logiques/NOT_e.PNG)| ![symbole électrique]( ./portes_logiques/NOT_el.PNG)|
|AND|   on veut réagir à l'activation à l'état VRAI de **toutes** les entrées| ![symbole américain](./portes_logiques/AND_a.PNG) | ![symbole européen](./portes_logiques/AND_e.PNG)| ![symbole électrique]( ./portes_logiques/AND_el.PNG)|
|OR|  on veut réagir à l'activation de l'état VRAI **d'une des entrées** au moins|![symbole américain](./portes_logiques/OR_a.PNG)| ![symbole européen](./portes_logiques/OR_e.PNG)| ![symbole électrique]( ./portes_logiques/OR_el.PNG)|  

```python
def fonction_non(a) :
    a_ = not a
    return a_

>>> fonction_non(0)
False
```

```python
def fonction_and(a,b) :
    s =  a and b
    return s

>>> fonction_and(1,0)
0
```

```python
def fonction_or(a,b) :
    s =  a or b
    return s

>>> fonction_or(1,0)
1
```

## La fonction NOR  et sa porte logique

__Comme son nom l'indique, elle correspond à not(a or b):__  

|a|b| a $`\lor b`$|$`\lnot (a \lor b)`$|  
|:---:|:---:|:---:|:---:|
|0|0|0|1|
|0|1|1|0|
|1|0|1|0|
|1|1|1|0|

La représentation symbolique de la porte est un mélange des deux symboles OR et NOT :  
  
|Fonction logique | Symbole américain| Symbole européen|  
|:---:|:---:|:---:|
|NOR|![symbole américain](./portes_logiques/NOR_a.PNG)|![symbole européen](./portes_logiques/NOR_e.PNG)|  
  
```python
def fonction_nor(a,b) :
    s =  not(a or b)
    return s

>>> fonction_nor(0,0)
True
```


## La fonction NAND  et sa porte logique

__Cette fois cette fonction correspond  à not(a and b):__ 
 
|a|b|a $`\land`$ b|$`\lnot`$(a $`\land`$ b)|  
|:---:|:---:|:---:|:---:|
|0|0|0|1|
|0|1|0|1|
|1|0|0|1|
|1|1|1|0|

La représentation symbolique de la porte est un mélange des deux symboles AND et NOT :  

|Fonction logique  |Utilité de la porte | Symbole américain| Symbole européen|  
|:---:|:---:|:---:|:---:|
|NAND |on veut réagir à l'activation de l'état FAUX d'**une des entrées au moins**|![symbole américain](./portes_logiques/NAND_a.PNG)| ![symbole européen](./portes_logiques/NAND_e.PNG)|  
  
Remarque importante :  
Il est possible de reconstituer les fonctions NOT, AND et OR en utilisant uniquement soit la fonction `NAND`, soit la fonction `NOR`. On évoque cette caractéristique sous la notion **d'universalité des opérateurs NOR et NAND**.

Lorsqu'on associe deux portes logiques compatibles, on peut connecter deux entrées ensemble, ou une entrée sur une sortie. Il ne faut en aucun cas connecter deux sorties différentes car elles peuvent produire des données différentes ; dans le cas de portes électroniques, cela équivaudrait à un court-circuit.  

```python
def fonction_nand(a,b) :
    s =  not(a and b)
    return s

>>> fonction_nand(1,1)
False
```

## Le OR exclusif ou XOR  

__Le booléen a ou b est égal si et seulement si a =1 ou b= 1, mais pas les deux.__  

Cette fonction permet de comparer des valeurs et de valider lorsque celles-ci sont **différentes**.
On l'exprime en logique en utilisant le symbole : $\oplus`$  


a $`\oplus`$ b =  ( not(a)and b) or ( a and not(b))  

|a|b|a $`\oplus`$ b|
|:---:|:---:|:---:|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|0|  

La représentation symbolique de la porte est  :  

|Fonction logique|Symbole américain| Symbole européen|  
|:---:|:---:|:---:|
|XOR|![symbole américain](./portes_logiques/XOR_a.PNG)|![symbole européen](./portes_logiques/XOR_e.PNG)|

```python
def fonction_xor(a,b):
    s = (not(a) and b) or (a and not(b))
    return s

>>> fonction_xor(1,0)
True
```

# Addition binaire

Le circuit pour additionner des nombres binaires de *n* bits est construit en mettant en cascades des additionneurs  1 *bit* qui réalisent chacun l'addition de deux nombres de *1* bit. Ces additionneurs *1 bit* sont eux mêmes construits à partir d'un circuit encore plus simple appelé *demi-additionneur*.

## Le demi-additionneur

Un demi-additionneur *1 bit* prend en entrée deux *bits e0 et e1* et il envoie sur une sortie s la somme e0 + e1. Si ce calcul provoque une retenue, il positionne alors à 1 une autre sortie c, qui indique l retenue éventuelle.
Voici la table de vérité d'un demi-additionneur *1 bit* :  
  
|entrées|||sorties||
|:---:|:---:|:---:|:---:|:---:|
|**e0**|**e1**||**s**|**c**|
|0|0||0|0|
|0|1||1|0|
|1|0||1|0|
|1|1||0|1|

On remarque que le fonctionnement d'un demi-additionneur peut se définir ainsi :

* s = $`e0 \oplus e1`$
* c = $`e0 \land e1`$

On en déduit le schéma de construction d'un demi-additionneur en assemblant une porte XOR et une porte AND :  

|![le demi-additionneur 1 bit](./portes_logiques/demi_add_1_bit_rz.png)|
 |:---|  
  
##  L'additionneur complet

On le construit en utilisant deux demi-additionneurs. Le premier calcule la somme s0 = e0 + e1, en positionnant éventuellement à 1 une retenue c1. Le second additionneur calcule la somme s = c0 + s0, en positionnant éventuellement à 1 une retenue c2. La retenue finale C vaut 1 si l'une des deux retenues c1 ou c2 est à 1. La table de verité d'un additionneur *1 bit* complet est ci-dessous :  

|entrées||||sorties||
|:---:|:---:|:---:|:---:|:---:|:---:|
|e0|e1|c0||S|C|
|0|0|0||0|0|
|0|0|1||1|0|
|0|1|0||1|0|
|0|1|1||0|1|
|1|0|0||1|0|
|1|0|1||0|1|
|1|1|0||0|1|
|1|1|1||1|1|

La retenue générée C vaut 1 si au moins 2 entrées parmi e0, e1 ou c sont 1; ceci est la **fonction majorité**.
On peut écrire celle-ci : C= $`(e0 \land e1)   \lor   (e0 \land  c0)  \lor (e1 \land c0).`$  

On peut l'écrire avec l'autre notation C =  e0.e1 +e0.c0 + e1.c0  

Quant à la fonction S, elle s’écrit directement comme suit sous forme de somme de produits à partir de la table, ce qui donne:  
S= $`(\overline{e0} \land  \overline{e1} \land c0) \lor ( \overline{e0} \land e1 \land \overline{c0}) \lor (e0 \land e1 \land c0) \lor (e0 \land \overline{e1} \land \overline{c0})`$

 Pour l’implanter sous forme de circuit, il faut donc 4 portes AND à  trois entrées et un OR à  quatre entrées (et des inverseurs, ou NOT).  

|![additionneur 1 bit](./img/addit_1bit_rz.png)|
|:---:|  

Une autre implantation de S utilise la formule S=e0 $`\oplus`$ e1 $`\oplus`$ c car le XOR (ici représenté par $`\oplus`$) est la somme de deux bits modulo 2 c'est à dire le bit de somme quand on oublie la retenue comme on le voit sur la table:  

|e0|e1|e0 $`\oplus`$ e1|
|:---:|:---:|:---:|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|0|  

|![additionneur 1 bit](./img/additionneur_-_bit_rz.png)|
 |:---:|
  
La somme des trois bits c0,e0 et e1 est donc c0 $`\oplus e0 \oplus`$ e1 ce qui donne un circuit plus simple.Cette implantation est cependant plus lente car la porte XOR peut être de 50% plus lente que les portes AND et OR.  
Peu importe le choix d’implementation, le circuit qui calcule S=e0 $`\oplus`$ e1 $`\oplus`$  c0 et C =  e0.e1 +e0.c0 + e1.c0  sera représenté par le symbole :

|![shéma électrique de l'additionneur](./img/circ_add_rz.png)|
 |:---:|

# Fonctions combinatoires logiques ou séquentielles

Il existe deux grands types de fonctions logiques :

* les fonctions logiques dites «**combinatoires** », bases du calcul booléen, elles résultent de l'analyse combinatoire des variations des grandeurs d'entrées uniquement.  

Par exemple, une lampe est allumée quand son interrupteur est fermé et elle est éteinte quand son interrupteur est ouvert ; l'état de la lampe dépend de la position de l'interrupteur mais pas de la position de l'interrupteur une seconde ou une minute plus tôt.  

* les fonctions logiques dites « **séquentielles** » ou bascules, qui résultent de l'association de plusieurs fonctions logiques « combinatoires » synchronisées grâce à une « horloge » qui donne le tempo ; les valeurs de sorties dépendent non seulement des valeurs d'entrée, mais aussi de l'instant où elles sont mesurées (avant ou après la synchronisation par l'horloge).

Par exemple, lorsqu'on appuie sur la touche 1 d'une calculatrice, le chiffre 1 apparaît sur l'écran, mais quand on relâche,le chiffre 1 ne disparaît pas. L'état de l'écran dépend de toute l'histoire du clavier.

Sources :  

* http://zanotti.univ-tln.fr/MD/   et  http://www.iro.umontreal.ca/~boyer/addition.pdf
* "Informatique et sciences du numerique" par Gilles Dowek  chez Eyrolles ISBN: 978-2-212-13676-0
* https://fr.wikipedia.org/wiki/Fonction_logique
* Cours de 1STi2D de louis Pasteur Hénin Beaumont
* "NSI, 30 leçons avec exercices corrigés" Ellipses Balabonski T. Conchon S. Filliâtre J.C. Nguyen K.
