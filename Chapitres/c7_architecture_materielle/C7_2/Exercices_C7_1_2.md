---
title: "Exercices C7_1-2 "  
subtitle: "Architecture von Neuman; algèbre de Boole"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# QCM : 

**question 1** 
L'UAL: 

* [ ] permet de gérer la mémoire
* [ ] permet de gérer les entrées/sorties
* [ ] est une adresse internet
* [ ] est l'endroit où se déroulent les calculs

**question 2**

L'UAL ne peut pas:

* [ ] faire des additions
* [ ] calculer des puissances entières de nombres entiers
* [ ] faire des racines carrées
* [ ] faire des opérations logiques : seules les additions, soustractions multiplications et divisions sont possibles

### question 3
Quelle affirmation est exacte? 

* [ ] La mémoire RAM ne fonctionne qu'en mode lecture
* [ ] La mémoire RAM permet de stocker des données et des programmes
* [ ] Une mémoire morte ne peut plus être utlisée
* [ ] La mémoire RAM permet de stocker définitivement des données  

**question 4**

Parmi les quatre expressions suivantes, la ou lesquelle(s) renvoie 1?
Rappel: 1 est renvoyé lorsque l'expression booléenne vaut True. 

* [ ] True and (True or False)
* [ ] False or (True and False)
* [ ] False and (True and False)
* [ ] True or (True and False) 

**question 5**

Parmi les quatre expressions suivantes, laquelle donne correctement une expression du théorème de Morgan?

* [ ] $`\overline{a + b} = \overline{a}+ \overline{b}`$ 
* [ ] $`\overline{a . b} = \overline{a}+ \overline{b}`$ 
* [ ] $`\overline{a . b} = \overline{a}. \overline{b}`$
* [ ] $`\overline{a + b} = \overline{a}+ \overline{b}`$

**question 6**

Parmi les quatre expressions suivantes, laquelle donne le nom de la table de vérité suivante ?  

|a|b|?|
|---|---|---|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|0|


* [ ] la fonction xor ( or exclusif) 
* [ ] la fonction or (or inclusif)
* [ ] la fonction and
* [ ] la fonction mux (multiplexage)

**question 7**
Si a et b sont les entrées d'un demi-additionneur 1 bit, s et r les sorties pour la somme et la retenue, quelle proposition est exacte?  

* [ ] s = a or b et r = a and b
* [ ] s = a xor b et r = a and b
* [ ] s = a and b et r = a or b
* [ ] s = a and b et r = a xor b

# Tables de vérité
1. Compléter le tableau suivant :
   
|a|b|(a or b) and (a and b)|
|---|---|---|
|0|0||
|0|1||
|1|0||
|1|1||

1. Comment peut-on simplifier la règle (a or b) and (a and b)? 

2. Compléter le tableau suivant :  
   
|a|b|(a or b) or (a and b)|
|---|---|---|
|0|0||
|0|1||
|1|0||
|1|1||

3.  Comment peut-on simplifier la règle (a or b) or (a and b)?




# Jane Doo :
Soient a et b deux booléens. On définit la fonction jane_Doo telle que : 
 a jane_Doe b = ( a or b) and not(a and b)  
 ou  encore  ( a+b).$`\overline{a.b}`$ 

 1. Compléter la table de la fonction jane_Doe suivante : 
 
 |jane_Doe|**0**|**1**|
|---|---|---|
|**0**|||
|**1**|||
 
2.  Quelle fonction se cache derrière jane_Doe?  

#  Jane Doo bis :
Soit le programme suivant :


```python
def jane_Doe (A,B,x):
    return ((x in A or x in B) and not(x in A and x in B))

A = "ABCDEFGHIJKLM"
B = "OKLM"

print(jane_Doe(B,A,"M"))
```

1. Qu'affiche-t-il si la lettre testée est M?             
2. Trouver une valeur de x pour qu'il affiche "True".          
3. Que se passe-t-il quand x = "A" si on définit la fonction jane_Doe de la manière suivante :                   


```python
def jane_Doe2 (A,B,x):
    return (x in A or B) and not(x in A and B)

A = "ABCDEFGHIJKLM"
B= "OKLM"

print(jane_Doe2(A,B,"A"))
```

    False
    

# Lois de Morgan
A l'aide d'une table de vérité, démontrer les lois de Morgan :
$`\lnot (x \land y)  = \lnot x \lor  \lnot y`$  
$`\lnot (x \lor y)  = \lnot x \land  \lnot y`$

# Xor
Ecrire en python une fonction `xor(x,y)` qui réalise l'opération du même nom sur les booléens.

# Démonstration
Montrer à l'aide de tables et ensuite par l'algèbre de Boole l'égalité suivante : 
$`(x \land y)\lor (\lnot y \land z) = ( x \lor \lnot y ) \land (y \lor z)`$ 

# Additionneur 2 bits
Expliquer comment combiner deux additionneurs 1 bit pour obtenir un additionneur 2 bits. Donner la table de vérité de cet additionneur 2 bits. Elle doit avoir 16 lignes.
