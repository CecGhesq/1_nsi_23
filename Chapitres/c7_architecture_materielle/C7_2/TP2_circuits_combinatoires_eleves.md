---
title: " TP2 Circuits combinatoires"  
subtitle: "Chapitre 7.2 :  Algèbre de Boole et circuits combinatoires"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# Le décodeur

## Rôle du circuit

Un décodeur est un dispositif essentiel à l’entrée de l’unité logique et arithmétique (UAL) de l’unité  centrale  de  l’ordinateur. Ce type de circuits très fréquent dans un ordinateur sert aussi à sélectionner des registres, des cellules mémoires ou des lignes de périphériques.

Un décodeur *n bits* possède n entrées et $`2^{n}`$ sorties. Les  n bits en entrée sont utilisés pour mettre à 1  dont le numéro  est égal au nombre codé (en base 2) par les entrées et mettre les autres sorties à 0.  

 **Un décodeur donc décode l'adresse et active la ligne correspondante.**

## Table de vérité d'un décodeur 2 bits

Par exemple pour un décodeur à *2 bits*, avec 2 entrées (e0 et e1) et 4 sorties (s0, s1, s2, s3), correspond à la table de vérité suivante :  

<table class="tg">
  <tr>
    <th class="tg-cly1">e1</th>
    <th class="tg-0lax">e0</th>
    <th class="tg-cly1">s0</th>
    <th class="tg-cly1">s1<br></th>
    <th class="tg-cly1">s2</th>
    <th class="tg-cly1">s3</th>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-0lax">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-0lax">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
  </tr>
  <tr>
    <td class="tg-cly1">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">0</td>
  </tr>
  <tr>
    <td class="tg-cly1">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
  </tr>
</table>

![schema_elec](./img_tp/decodeur.PNG)

## Expressions booléennes et application
Etablir le fonctionnement d'un décodeur à l'aide de quatre expressions booléennes en analysant la table de vérité  :  
* s0 =
* s1 = 
* s2 = 
* s3 = 

Compléter le schéma ci-dessous:  

![Décodeur 4 bits](./img_tp/decodeur_a_completer_rz.png)
[puis compléter celui-ci en ligne ; ](https://circuitverse.org/simulator/embed/circuit-decodeur-a-completer)

Vérifier le fonctionnement

# Le multiplexeur

## Rôle du circuit

Le multiplexage est une technique qui consiste à faire passer plusieurs informations à travers un seul support de transmission : cette fonction est utilisée dans les clés USB.  

On a $`2^{n}`$ entrées, n fils de sélection qui sont aussi des entrées, et une seule sortie.
Le multiplexeur sélectionne l’une des $`2^{n}`$ entrées pour l’envoyer en sortie. En effet chacune des $`2^{n}`$  entrées  est  numérotée  par  un  nombre  en  binaire  de longueur n.  Le  choix  se  fait  par  le  mot  de longueur n  des fils  de  sélection,  et  c’est  l’entrée  correspondant  à  ce  mot  qui  passe  en  sortie.  Cela s’appelle plus précisément un multiplexeur un parmi $`2^{n}`$

**Un multiplexeur permet de sélectionner la bonne sortie lors d'une lecture.**

## Le multiplexeur 1 parmi 2

Le **multiplexeur 1 parmi 2**  a deux entrées `e0` et `e1` et un fil de sélection `s0` pour choisir l’une des deux entrées et la mettre en sortie `S`. Il est ainsi représenté :
![multiplexeur](./img_tp/mux_schema_rz.png)

* Si s0 vaut 0 alors mux(s0,e0,e1) vaut e0 
* si s0 vaut 1 alors mux(s0,e0,e1) vaut e1

Compléter la table de vérité pour répondre à la contrainte énoncée ci-dessus.

<table class="tg">
  <tr>
    <th class="tg-cly1">s0</th>
    <th class="tg-cly1">e0</th>
    <th class="tg-cly1">e1</th>
    <th class="tg-cly1">S</th>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1"> </td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax"> </td>
  </tr>
</table>  

## Expression booléenne

* En déduire la valeur de la variable de sortie S en lisant cette table :  

S = 

* Essayez de la simplifier en utilisant des identités élémentaires comme la distributivité, le complément et l'élément neutre:  

$`S =`$  

$`S =`$  

$`S =`$  

$`S =`$  


**Ainsi S = mux(s0,e0,e1) = ( $`\lnot`$ s0 $`\land`$  e0) $`\lor`$ (s0 $`\land`$ e1)**
ou encore S = ($`\overline{s0}`$.e0)+(s0.e1)

## Réalisation du multiplexeur 1 parmi 2

* Combien de portes sont-elles nécessaires et lesquelles?
.-.-

* Créer ce montage sur CircuitVerse et réaliser des copies d'écran dans un traitement de texte pour les différentes valeurs des entrées .

```python
def fonction_mux(s0,e0,e1):
    s = (not(s0) and e0) or (s0 and e1)
    return s

>>> fonction_mux(1,0,1)
1

>>> fonction_mux(0,0,1)
0
```

## Application décodeur/multiplexeur

Une    application  des  décodeurs  se  situe  à  l’entrée  de  la  mémoire  principale.  Imaginons une  mémoire  d’un  Mo,  c’est-à-dire  $`10^{6}`$ =  $`(10^{3})^{2}`$  =$`(2^{10})^{2}`$ =  $`2^{20}`$  octets.  En  supposant  qu’un  octet occupe une cellule mémoire, il faudrait utiliser $`2^{20}`$ fils d’adresses pour pouvoir accéder à n’importe quelle cellule mémoire. Cela est impensable.  

Aussi  met-on  en  place  une  autre  organisation,  avec un  décodeur  et  un  multiplexeur  qui  vont permettre de réduire le nombre de fils de transfert d’adresses à l’entrée de la mémoire.  
D’abord la mémoire est rendue rectangulaire, avec des lignes et des colonnes, par exemple avec $`2^{10}`$ colonnes et autant  de  lignes.  Chaque  cellule  a  son  adresse  définie  par  la  ligne  et  la  colonne où  elle  se  trouve.   
D’autre  part  on  utilise  20  fils  où  passent  les  adresses,  une  adresse  ayant  pour  longueur  20,  soit    $`a_{19}a_{18}.... a_{0}`$.  Ces  adresses  sont  ensuite  coupées  en  deux : $`a_{19}a_{18}.... a_{10}`$,  et $`a_{9}a_{8}.... a_{0}`$.  La demi-adresse $`a_{9}a_{8}.... a_{0}`$ est envoyée sur les 10 lignes d’entrée d’un décodeur  qui les dispatche en sortie sur les $2^{10}$ lignes de la mémoire rectangulaire (ici carrée). On accède ainsi à la ligne où se trouve  la  cellule  concernée.  
Reste  à  trouver  sa  colonne,  ce  qui  donnera  la  position  exacte  de  la cellule  dont  on  pourra  envoyer  le  contenu  en  sortie.  Pour  cela  on  utilise  un  multiplexeur  avec  10 fils  de  sélection  contenant l’autre  moitié  de l’adresse,  soit $`a_{19}a_{18}.... a_{10}`$ correspondant  à l’adresse de la colonne concernée. Ce multiplexeur a aussi $`2^{10}`$ lignes d’entrée provenant des $`2^{10}`$ colonnes de la  mémoire,  permettant  grâce  aux  fils  de  sélection d’atteindre  la  colonne  où  se  trouve  la  cellule concernée, et d’envoyer dans le fil de sortie le contenu de la cellule.

![dec_mux](./img_tp/dec_mux_rz.png)

# L'additionneur

## Le demi-additionneur

Commençons par l’addition de deux bits a et b en entrée, avec en sortie la somme S et la retenue R. Cela s’appelle le `demi-additionneur`, parce qu’il ne tient pas compte de la retenue qui peut aussi arriver en entrée, provenant de calculs précédents.

* Remplir la table de vérité suivante : 

<table class="tg">
  <tr>
    <th class="tg-cly1">a</th>
    <th class="tg-cly1">b</th>
    <th class="tg-cly1">S</th>
    <th class="tg-cly1">R<br></th>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1"> </td>
  </tr>
</table>  

* En déduire les expressions booléennes pour S et R :  

$`S =`$  

$`R =`$

* Combien de portes vont-elles être nécessaires?

* Réaliser le circuit dans CircuitVerse et réaliser des copies d'écran pour les différentes situations.

## L'additionneur 1bit complet

On  tient  maintenant  compte  de  la  retenue r  provenant  éventuellement  d’un  calcul  en  amont. L’additionneur complet a trois entrées a, b et r, ainsi que deux sorties S et R.

* Compléter la table de vérité :

<table class="tg">
  <tr>
    <th class="tg-cly1">a</th>
    <th class="tg-cly1">b</th>
    <th class="tg-cly1">r</th>
    <th class="tg-cly1">S</th>
    <th class="tg-0lax">R</th>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1"> </td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1"> </td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1"> </td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1"> </td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax"> </td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax"> </td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax"> </td>
    <td class="tg-0lax"> </td>
  </tr>
  <tr>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax"> </td>
    <td class="tg-0lax"> </td>
  </tr>
</table>  

* Etablir les expressions booléennes S et R ( en utilisant l'autre notation):
  
$`S=`$  

$`R=`$  

qui peuvent être  simplifiées  :  
 
$`S = \overline{r}.(\overline{a}.b + a.\overline{b}) + r.(a.b + \overline{a}.\overline{b})`$  

$`S = \overline{r}.(a \oplus b) + r.(\overline{a \oplus b})`$  

$`S = r \oplus (a \oplus b) = r \oplus a \oplus b`$  


$`R = ab + ar + br`$  

$`R = ab + r.(a \oplus b)`$  

* Si on se limite à utiliser les portes AND, OR et NOT : combien sont nécessaires pour réaliser S?  

* on va utiliser les deux demi-additionneurs pour le réaliser avec les portes XOR ; les relier ci-dessous :  

![additionneur à compléter](./img_tp/add_a_comp_rz.png)

* Réaliser le circuit sur CircuitVerse et réaliser des copies d'écran pour différents cas.

## UAL

On donne ci-dessous  une  version  très  simplifiée  de  cette  unité logique  et  arithmétique  en  supposant  qu’elle  est  capable  d’exécuter  quatre  opérations :  le  ET,  le OU, le NON et l’addition, à partir de deux nombres binaires a et b de un bit chacun. A la sortie on aura le résultat de l’opération concernée sur un bit. 
Il s’agit d’une UAL de 1 bit.  
Rappelons-nous :  l’`unité  de  commande`  de  l’unité  centrale  recueille  dans  un  `registre`  le  nombre codé associé à une opération à réaliser, et portant sur les deux opérandes a et b. Elle se charge de `décoder`  cette  opération  pour  qu’elle  soit  dirigée  vers  l’opération  concernée  dans  l’UAL.  C’est  là qu’intervient le décodeur. Le code à deux chiffres de l’instruction entre dans le décodeur par les fils e0  et e1.  Le décodeur  a  quatre  fils  en sortie,  et  selon  l’opération  à  réaliser  un  de ces fils est  activé (mis à 1) tandis que les autres restent à 0. On en déduit ce schéma de l’UAL :

![UAL](./img_tp/ual_rz.png)

* Que se passe-t-il si à l'entrée du décodeur on met (e1,e0) à (0,1) ?  

* Que se passe-t-il si à l'entrée du décodeur on met (e1,e0) à (1,1) ?  

[Voir l'UAL](https://urlz.fr/gUJo)


source : 
* http://www.pierreaudibert.fr/ens/7-CIRCUITS%20COMBINATOIRES.pdf


