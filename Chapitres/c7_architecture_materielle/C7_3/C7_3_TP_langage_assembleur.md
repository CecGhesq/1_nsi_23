---
title: "TP sur le langage assembleur"  
subtitle: "Chapitre 7_3 "
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# La soustraction

1. Sur le modèle vu en cours, vous allez réaliser un programme en langage assembleur en utilisant la machine M999 qui va réaliser une soustraction. La première donnée qui va se trouver à l'adresse mémoire 95 a la valeur de 20. La deuxième donnée qui va se trouver à l'adresse mémoire 96 à la valeur 16. La première donnée sera chargée dans le registre A et l'autre dans le registre B. Le résultat sera affecté à l'adresse mémoire 97, puis on arrêtera le programme. Ecrire le code avec les mnémoniques puis avec le code chiffré dans la machine M999.

2. Réaliser un programme en langage assembleur qui va réaliser une soustraction. Si le résultat de la soustraction donne une valeur strictement positive, le programme s'arrête sinon, on inverse les valeurs affectées aux registres A et B, on réalise de nouveau la soustraction puis on arrête le programme.
   

# La multiplication en langage assembleur

En vous aidant de l'algorithme suivant et des programmes déjà réalisés en langage assembleur, réalisez la multiplication de deux entiers stockés dans les mémoires d'adresse 95 et 96. (Vous prendrez les valeurs que vous voulez.)

Algorithme:  

 x, y, somme : entiers  

 somme = 0  
 Tant que y > 0  
   somme = somme + x  
   y = y - 1  
 fin tant que

Les valeurs de x, y, somme et autres variables pourront déjà être affectées dans les mémoires.