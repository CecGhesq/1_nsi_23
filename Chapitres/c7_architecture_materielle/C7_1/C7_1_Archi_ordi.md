---
title: "Chapitre 7 : Architecture d'un ordinateur"  
subtitle: "C7_1 Architecture de von Neumann"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

![BO](./img/BO.PNG)  

# Un peu d'histoire....  

## L'ENIAC 

Le tableau de ce qu’en 1945 on n’appelait pas encore l’informatique présente un paysage contrasté. D’un côté, la notion de calcul effectif a trouvé un cadre rigoureux grâce aux avancées d’une discipline nouvelle née dans les années 1930, la méta-mathématique. Le lambda-calcul du mathématicien **Alonzo Church** et la machine universelle (abstraite) d’**Alan Turing**, schémas dont Turing a montré l’équivalence, sont proposés en 1936 comme base de définition de l’`algorithme`, pièce maîtresse du processus calculatoire. D’un autre côté, plusieurs tentatives indépendantes visent à construire des machines électroniques ou électro-mécaniques capables d’exécuter des calculs complexes à grande vitesse. Les précurseurs en sont John Atanasoff en 1938 aux États-Unis et Konrad Zuse en 1941 en Allemagne.

Ces deux courants, celui des **mathématiciens et logiciens** d’une part, et celui des **ingénieurs** d’autre part, sont issus de deux mondes séparés et s’ignorent mutuellement. Les travaux de Turing ont sans doute eu une influence sur la conception en 1943-44 du calculateur Colossus à Bletchley Park en Angleterre, mais il s’agit d’une machine spécialisée dont le seul objectif, qui sera d’ailleurs atteint, est le décryptage du code secret de la machine Lorenz, successeur de l’Enigma, utilisée par l’armée allemande. La guerre aura d’autres effets : les autorités allemandes ne soutiendront que modestement les travaux pionniers de Zuse, alors que le département américain de la Défense finance un projet ambitieux lancé en 1943 à l’Université de Pennsylvanie par **J. Presper Eckert et John Mauchly**. Cet effort aboutira à la construction d’un grand calculateur électronique, **l’ENIAC**, qui ne sera néanmoins pleinement opérationnel qu’en 1946. Il occupait une surface de 167 m<sup>2</sup> et consommait 150 kW. Sur cette machine, une multiplication était réalisée en 0,001 seconde ; de nos jours, une telle opération demande 30 nanosecondes. 

À cette même époque (1944), l’informaticien Howard Aiken mène un autre grand projet à Harvard avec la collaboration d’IBM, mais la technique choisie est basée sur l’électromécanique. Bien plus fiable que les tubes électroniques, cette voie ne sera toutefois pas poursuivie, mais l’expérience acquise sera exploitée plus tard par IBM dans la conception de ses premiers ordinateurs.  
![eniac](img/eniac.jpg)   
<center>L’ENIAC (Electronic Numerical Integrator And Computer) à Philadelphie. Exemple d’un ordinateur câblé. </center> 
<center> Crédits : U.S. Army Photo / Public Domain via Wikimedia </center>

## L'architecture von Neumann

Dans les années 1945, __John Von Neumann__ définit l’architecture des ordinateurs dits à programme enregistré. Ces ordinateurs se distinguent de leurs prédécesseurs par le fait qu’ils disposent d’un **programme composé d’instructions** qui doivent être placées dans une mémoire. Chaque instruction définit une action à réaliser sur des données, par exemple une addition entre deux nombres. Auparavant, **les premiers ordinateurs n’étaient pas programmables**. Ils n’exécutaient qu’un seul programme câblé dans l’ordinateur. Chaque exécution d’un nouveau programme nécessitait de recâbler l’ordinateur.  
<span style="color: #58ACFA" >L’ENIAC </span> (Electronic Numerical Integrator and Computer),  est l’exemple type de ce genre de machine.  
L’architecture des ordinateurs à programme enregistré comporte les éléments suivants :

* Une unité centrale chargée d’exécuter les instructions d’un programme ;
* Une mémoire centrale qui contient les instructions d’un programme ;
* Des unités d’entrées-sorties prenant en charge l’échange d’informations entre le couple unité centrale-mémoire centrale et l’extérieur de l’ordinateur, via des périphériques tels que le clavier, la souris, l’écran, etc.
  
![modele](./img/modele-originel2.gif)

La première innovation est la *séparation nette* entre l’**unité de commande ou de contrôle**, qui organise le flot de séquencement des instructions(= chef d'orchestre), et l’**unité arithmétique**, chargée de l’exécution proprement dite de ces instructions.  
La seconde innovation, la plus fondamentale, est l’idée du **programme enregistré** : les instructions, au lieu d’être codées sur un support externe (ruban, cartes, tableau de connexions), sont enregistrées dans la mémoire selon un codage conventionnel. Un compteur ordinal contient l’adresse de l’instruction en cours d’exécution ; il est automatiquement incrémenté après exécution de l’instruction, et explicitement modifié par les instructions de branchement.

Un emplacement de mémoire peut contenir indifféremment des instructions et des données, et une conséquence majeure (dont toute la portée n’avait probablement pas été perçue à l’époque) est qu’un __programme peut être traité comme une donnée par d’autres programmes__. Cette idée, présente en germe dans la machine de Turing, trouvait ici sa concrétisation.

Cette architecture est toujours celle mise en œuvre dans les ordinateurs actuels.  

Un documentaire d'Arte sur la vie très riche de John von Neumann est disponible :  
https://www.youtube.com/watch?v=c9pL_3tTW2c  
Regarder surtout entre 39:12 et 46:00

## Qu’en est-il aujourd’hui ?

Plus de soixante ans après son invention, le modèle d’architecture de von Neumann régit toujours l’architecture des ordinateurs. Par rapport au schéma initial, on peut noter deux évolutions.
![modèle actuel](img/modele-actuel.gif)
*Le modèle de von Neumann, aujourd’hui.*

   Les entrées-sorties, initialement commandées par l’unité centrale, sont depuis le début des années 1960 sous le contrôle de processeurs autonomes (canaux d’entrée-sortie et mécanismes assimilés). Associée à la multiprogrammation (partage de la mémoire entre plusieurs programmes), cette organisation a notamment permis le développement des **systèmes en temps partagé** .
   
   Les ordinateurs comportent maintenant des **processeurs multiples**, qu’il s’agisse d’unités séparées ou de « cœurs » multiples à l’intérieur d’une même puce. __Cette organisation permet d’atteindre une puissance globale de calcul élevée sans augmenter la vitesse des processeurs individuels, limitée par les capacités d’évacuation de la chaleur__ dans des circuits de plus en plus denses.</span>

Ces deux évolutions ont pour conséquence de mettre la *mémoire, plutôt que l’unité centrale, au centre de l’ordinateur*, et d’augmenter le degré de parallélisme dans le traitement et la circulation de l’information. Mais elles ne remettent pas en cause les principes de base que sont la séparation entre traitement et commande et la notion de programme enregistré.

L’accès des processeurs à la mémoire se fait à travers un *bus (non représenté sur la figure), voie d’échange assurant un transfert rapide de l’information*. Mais au cours du temps, et pour des raisons technologiques, le débit du bus a crû moins vite que le débit d’accès à la mémoire et surtout que la vitesse des processeurs. D’où un phénomène d’attente — le « goulot de von Neumann » — qui réduit les performances. Des palliatifs sont l’usage généralisé de **caches à plusieurs niveaux** (mémoire d’accès rapide, voisine du processeur, et retenant les données courantes), et le développement de machines à mémoire distribuée mais se posent alors des problèmes de cohérence pour les données en copies multiples.

# La mémoire centrale 

## Description

La `mémoire centrale` contient **le programme que le processeur doit exécuter** ; ce programme est constitué d’un ensemble d’instructions et de données sur lesquelles les instructions vont agir.

Données et instructions sont codées sous forme de **chaînes binaires** et sont contenues dans des mots de la mémoire. Un mot est un emplacement de la mémoire, constitué d’un nombre fixe d’octets, typiquement 4 ou 8 ;  il contient une information (instruction ou donnée) et il est identifié de façon unique par une adresse .  

Cette adresse permet au processeur de nommer le mot qu’il souhaite utiliser. Ce nom est le rang du mot dans la mémoire ; ainsi le processeur désigne le mot 0, le mot 1, le mot 2, etc. Finalement, la mémoire centrale peut être vue **comme un tableau de mots**, chaque mot étant désigné par son rang dans le tableau.

Le processeur utilise cette adresse pour désigner le mot sur lequel il doit agir. Le processeur peut demander à : 

* **lire** un mot auquel cas il va obtenir comme information le contenu de ce mot, 
* **écrire** dans un mot auquel cas il va enregistrer une information dans ce mot.

Le processeur communique avec la mémoire centrale par l’intermédiaire d’un `bus` de communication. Un bus est un support de communication permettant l’échange d’informations entre deux composants. Par exemple, en se référant à la figure 3 ci-dessous, si le processeur désire lire un mot d’adresse 7, il va placer sur le bus l’adresse 7 et activer la commande « lecture » ; la mémoire en retour place sur le bus la donnée contenue dans le mot d’adresse 7, soit 128.  

![mémoire centrale](img/memoire-centrale.png )  


Cette mémoire centrale est aussi appelée `mémoire vive`, car c’est une mémoire volatile : le contenu des mots disparait s’il n’y a plus d’alimentation électrique de l’ordinateur. On la désigne aussi sous le nom de `RAM` pour *Random Access Memory*.

Elle est caractérisée notamment par un temps d’accès et une capacité.

   La capacité représente le volume global d’informations que la mémoire peut contenir, par exemple 1 **gibi**octet (Gio), soit 2$`^{30}`$ octets, soit 2$`^{30}`$ × 8 bits.
    Le temps d’accès correspond au temps que doit attendre le processeur pour obtenir une information délivrée par la mémoire centrale.

La capacité usuelle d’une mémoire centrale sur un ordinateur personnel actuel est de 4 à 8 Gio.

La RAM se présente sous forme de barrettes, un circuit imprimé de forme rectangulaire enfichable sur la carte mère et qui comporte sur ses deux faces des puces mémoires.
![ram](./img/barrette-memoire.png)

## Stockage en informatique  
La grandeur de base est le bit (binary digit). Un `bit` est un élément pouvant être égal à 0 ou à 1 (deux valeurs possibles donc).

Un fichier est un ensemble de bits. Un ensemble de bits forme ce qu'on appelle un `mot binaire`.

Le langage binaire est le seul que l'ordinateur comprend.

Pour simplifier les grandeurs, on utilise des multiples de l'`octet` (un ensemble de 8 bit), à ne pas confondre avec la définition anglaise de l'octet qui s'écrit Byte (avec le B majuscule).

Un bit ne suffisant pas pour exprimer toutes les tailles de fichiers disponibles, des unités de mesures (comme le centimètre, le mètre et le kilomètre par exemple) ont été mises en place :

 * Le kilo-octet (Ko) : 1 Ko équivaut à 1000 octets.
 * Le méga-octet (Mo) : 1 Mo = 1000 Ko.
 * Le giga-octet (Go) : 1 Go = 1000 Mo.
 * Le téra-octet (To) : 1 To = 1000 Go.

Pour garder nos anciennes normes qui voulaient que les unités de mesures soient des puissances de 2 (1 Ko = 1024 octets par exemple), d'autres unités de mesures  ont été inventées :  

|Unité|puissance|valeur|valeur approchée|
|:---:|:---:|:---:|:---:|
|**kibi**-octet (Kio)  |$`2^{10}`$|1 024 octets|environ 1 ko|
|**mébi**-octet (Mio)  |$`2^{20}`$|1 048 576 octets|environ 1 Mo|
|**gibi**-octet (Gio)  |$`2^{30}`$|1 073 741 828 octets|environ 1 Go|
|**tébi**-octet (Tio)  |$`2^{40}`$|1 099 511 627 776 octets|environ 1 To|



# Le processeur

Le `processeur` (CPU, pour Central Processing Unit) est le **cerveau de l’ordinateur**. Il permet d’exécuter des instructions codées sous forme binaire.

Le processeur est un circuit électronique cadencé au rythme d’une `horloge` interne qui envoie des impulsions, appelées « top ». La `fréquence d’horloge` correspond au nombre d’impulsions par seconde. Elle s’exprime en **Hertz** (Hz). De nos jours, les processeurs possèdent par exemple une fréquence d’horloge de 2 GHz, ce qui correspond à 2 000 000 000 de battements par seconde. Le temps s’écoulant entre chaque battement constitue un cycle processeur, durant lequel le processeur exécute une *action élémentaire permettant l’exécution d’une partie d’instruction*.

Le processeur est composé de plusieurs éléments :

* Des **unités de mémorisation** appelées `registres` qui permettent la mémorisation d’informations au sein du processeur. Leur accès est très rapide. Parmi ces registres, certains sont spécialisés, c’est-à-dire qu’ils ont un rôle précis au sein du processeur et ils ne peuvent pas être utilisés pour une autre tâche. D’autres registres, au contraire, sont des registres banalisés ; ils servent notamment à stocker au niveau du processeur des résultats de calculs intermédiaires. Ces registres banalisés sont classiquement désignés par un numéro ; ainsi le registre banalisé 1 s’appelle *R1*, le registre banalisé 2 s’appelle *R2*, etc.
  
* Une **unité d’exécution** qui permet au processeur de réaliser des calculs ; elle comprend notamment un circuit appelé `Unité Arithmétique et Logique `(UAL) qui est constitué de l’ensemble des circuits arithmétiques et logiques permettant au processeur d’effectuer les opérations élémentaires telles que des additions, des soustractions, des multiplications ou des comparaisons, etc.
  
* Une **unité de commande** qui cadence et pilote le travail du processeur ; ce pilotage s’effectue au rythme du signal d’horloge délivré à celle-ci et qui définit les cycles du processeur.
  
* Des **unités de communication** qui permettent à l’information de circuler entre les différents composants du processeur.

Il se présente sous la forme d’une puce électronique reliée à la `carte mère `de l’ordinateur grâce à un socle appelé « socket » et il est surmonté d’un dispositif dissipant la chaleur produite lors de son fonctionnement.  
![carte mère](./img/processeur.png)

#  Exécuter un programme

Le rôle du processeur est donc d’exécuter les instructions d’un programme placé dans la mémoire centrale. Au sein du processeur, c’est **l’unité de commande** qui a la charge de piloter les actions menant à l’exécution d’une instruction. Le processeur agit ici comme un automate ; il exécute chaque instruction les unes à la suite des autres.

À ce niveau, une instruction est une *chaîne binaire* qui comprend deux grandes parties:

* La première partie de cette chaîne indique quelle est l’opération à réaliser, par exemple une addition, une lecture de la mémoire, le chargement du registre banalisé avec une valeur. Elle constitue le code opération de l’instruction.
  
* La seconde partie précise quelles sont les données sur lesquelles l’action indiquée par le code opération doit être réalisée. Cette seconde partie constitue les opérandes de l’instruction. 

Les opérandes de l’instruction peuvent être de différentes natures. Cela peut être une valeur, une donnée contenue dans un registre banalisé du processeur ou une donnée stockée dans un mot de la mémoire centrale. Par exemple, l’instruction indique qu’il faut faire une addition entre la valeur contenue dans le registre R1 du processeur et une valeur 10. Ou l’instruction indique qu’il faut placer la valeur se trouvant dans un mot mémoire d’adresse 100 dans le registre banalisé R1.

Pour chaque instruction à exécuter, le processeur réalise les trois étapes suivantes :

   1. Lecture de l’instruction,
   2. Reconnaissance de l’instruction et recherche des opérandes,
   3. Réalisation de l’opération reconnue.


Sources :   

* https://interstices.info/memoire-et-unite-centrale-un-couple-dedie-a-lexecution-des-programmes/  
* https://interstices.info/le-modele-darchitecture-de-von-neumann/
*  https://www.vulgarisation-informatique.com/ko-mo-go.php

