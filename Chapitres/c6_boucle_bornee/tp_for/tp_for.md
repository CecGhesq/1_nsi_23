---
title: "Chapitre 6 : Boucles bornées"
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


# Exercice 1 - Tours de chauffe  &#x1F3C6;  

Dans l'exercice suivant, vous utiliserez les boucles POUR décrites en cours pour réaliser les programmes suivants.  
Il vous est conseillé d'encapsuler chaque programme dans une fonction nommée par vos soins

__1)__ Ecrire une programme qui demande à l'utilisateur combien de fois il souhaite voir afficher la chaîne de caractères `NSI` puis qui l'affiche n fois.

__2)__ Ecrire un programme qui affiche les 20 premiers nombres pairs séparés par le caractère `-`

__3)__ Ecrire un programme qui affiche les entiers de 0 à 50 non compris de trois en trois séparés par le caractère `;`

__4)__ 🥇 En mathématiques, une suite géométrique est une suite de nombres dans laquelle chaque terme permet de déduire le suivant par multiplication par un facteur constant appelé raison.  

Ainsi, une __suite géométrique__ a  la forme suivante :  $`a, aq, aq^{2}, aq^{3}, aq^{4},\dots`$  
   
_Par exemple_ : $`2,  16, 128, 1024, 8192, \dots`$ est une suite géométrique croissante, de premier terme __a=2__ de raison __q=8__ ( ici seuls les 5 premiers termes sont présentés)  

__Créer un programme demandant à l’utilisateur :__  

- le premier terme a de la suite
- La raison q : de la suite
- le nombre n de termes à afficher.  
  
__Le but sera d’afficher les n premiers termes de la suite sur une même ligne.__ 

__5)__ Ecrire un programme calculant le produit des 100 premiers entiers naturels : $`1 \times 2 \times \dots \times 100`$ : vous utiliserez un accumulateur. [^1] 


# Exercice 2 - Dessinez avec la tortue : les déplacements  &#x1F3C6;  

La module `turtle` intégré à la bibliothéque standard de python permet de réaliser des dessins géométriques à l'aide d'une _tortue virtuelle_.  
Après avoir présenté quelques fonctionnalités de base, nous utiliserons ce module pour illustrer les boucles bornées vues en cours.

|Mouvements|méthode associée|nature du paramètre| type de paramètre|  
|:---:|:---:|:---:|:---:|    
|__Avancer__|`forward`_(d)_|distance parcourue en pixels| `int` ou `float`|  
|__pivoter à droite__|`right`_(a)_|angle de rotation de la tête en degré| `int` ou `float`|  
|__pivoter à gauche__|`left`_(a)_|angle de rotation de la tête en degré| `int` ou `float`|  
|__aller au point de coordonnées__|`goto`_(x, y)_|coordonnées du point| `int` ou `float`|  

__1)__ Tester l'exemple suivant : 

```python
from turtle import *
forward(100)
left(90)
forward(100)
right(90)
forward(50)
right(90)
forward(150)
```

__a)__ Expliquez la première ligne en rajoutant un commentaire.

_Remarques_ :  

*  La tortue commence toujours au point de coordonnées (0,0) situé au centre de la fenêtre `Python Turtle Graphics`.
*  Pour réaliser un nouveau dessin, fermer d'abord cette fenêtre sinon la tortue repartira de l'état final précédent.  

__Attention : le nom du script enregistré ne soit pas être turtle.py !! Car c'est aussi le nom du module importé__
  
__b)__ Définissez une procédure `carre` permettant de dessiner un carré et acceptant un paramètre `a` de type entier représentant la longueur de l'arête. 
Codez cette fonction d'abord sans boucle puis avec une boucle bornée `for`

__c)__ À l'aide de votre procédure `carre`, définissez une procédure nommée `carres_tournants` qui dessine n carrés de côté 100 pivotant autour d'un sommet commun pour faire un tour complet. La figure suivante montre l'exemple pour n=7.

![carres tournants](./fig/carres_tournants.jpg)  

[^2] 

__2)__ On peut également contrôler le crayon à l'aide de plusieurs instructions

|Instructions |description|   
|:---:|:---:|     
|`up()`|relève le crayon|  
|`down()`|abaisse le crayon (_le dessin peut reprendre_)|  

Testez le code suivant:

```python
forward(100)
up()
forward(50)
down()
forward(100)
```

__En utilisant les fonctions précédentes et une boucle bornée, définissez les procédures suivantes__

__a)__ `trois_carres(a)` dessinant trois carres d'arête `a` alignés horizontalement et séparés d'une distance `a`  

__b)__ `grille_horizontale(x, a)` dessinant `x` carres accolés horizontalement.  

_Conseils_:  

* _utilisez des petites distances de l'ordre de 10 à 20 pixels, et peu d'alternances pour que le dessin "reste" dans la fenêtre_ 
* _vous réglez la vitesse de la tortue à l'aide de la méthode `speed`_  (https://docs.python.org/3.3/library/turtle.html?highlight=turtle#turtle.speed)

__c)__ `grille_verticale(y, a)` dessinant `y` carres accolés verticalement.  

__d)__  🥇`grille(x, y, a)` dessinant une grille de x sur y carrés


# Exercice 3 - Utilisation d'un accumulateur  &#x1F3C6; &#x1F3C6;   

__1)__ Definissez une procedure `spirale_carre()` permettant de réaliser le dessin ci-dessous.

![spirale carré](./fig/spirale_carre.jpg)  

A chaque tour le trait est plus long de 10 pixels : il y a 20 traits au total. (_vous pouvez ultérieurement modifier ces paramètres ou bien les demander à l'utilisateur_)


__2)__
Le style du trait réalisé par le crayon peut être modifié : 

|Instructions |description|   
|:---:|:---:|     
|`width`_(e)_|fixe à _e_ l'épaisseur du trait|
|`color`_(c)_|sélectionne une couleur _c_ (type `str`) pour les traits|  

__a)__ Testez le code suivant:

```python
forward(100)
up()
forward(50)
down()
color("red")
width(5)
forward(100)
up()
forward (50)
down()
color("green")
width(10)
forward(50)
``` 

__b)__ Définissez une procédure `faisceau(c)` acceptant comme paramètre  `c` une couleur sous forme de chaîne de caractères et permettant de réaliser un trait de plus en plus large de couleur choisie par l'utilisateur.

![faisceau](./fig/faisceau.jpg)  


# Exercice 4 - Invariants de boucle  &#x1F3C6; &#x1F3C6;  

__1)__ $`r=x^{i}`$ est un invariant de boucle pour la fonction suivante. 

```python
def puissance(x, n):
    """
    Renvoie x à la puissance n
    n est supposé positif ou nul
    """
    r = 1
    for i in range(n):
        r = r * x
    return r
```

Prouver le bon fonctionnement de ce programme en utilisant la méthode donnée en cours.

__2)__ On considére l'algorithme suivant qui permet de calculer le quotient et le reste de la division euclidienne d’un entier positif par un entier strictement positif [^3] 

```
Entrées a le dividende, b : le diviseur tous les deux entiers positifs
Sorties : le quotient q et le reste r

q ← 0
r ← a
TANT QUE r >= b faire :
    q ← q +1 
    r ← r - b
Renvoyer q et r
```

__a-__ Décrire le fonctionnement de l'algorithme pour les entrées a= 17 et b=5 au moyen d'un tableau indiquant l'évolution des valeurs au fil des itérations    
__b-__ Montrer que la boucle se termine à l'aide du variant de boucle  $`r-b`$.  
__c-__ Montrer que la propriété $`a = bq +r`$ est un invariant de boucle en déduire que l'algorithme produit le résultat attendu.  


[^1]: Extrait de _Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

[^2]: Extrait de https://www.fil.univ-lille1.fr/~L1S1Inf

[^3]: http://www.emmanuelmorand.net/informatique/PTSI-1516/PTSI1516CoursInfo06.pdf
