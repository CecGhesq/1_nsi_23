

# Exercice : Tracé de fonctions.   &#x1F3C6;   &#x1F3C6; 

__1)__ On souhaite obtenir la représentation graphique de la fonction $`f(x) = 2 \times x + 1`$.    
Nous allons utiliser la bibliothéque `matplotlib`.     
   __a-__ Si elle n'est pas installée, ajouter-la via le menu `Outils\gérer les paquets` de `Thonny`.  
__b-__ Testons sur un exemple le tracé d'un graphique.

```python
import matplotlib.pyplot as plt
x = [1, 2, 3, 4, 5] # liste des abscisses
y = [1, 4, 9, 16, 25] # liste des ordinnées
plt.plot(x, y) #construction de la courbe (sans affichage)

plt.xlabel("abscisses") # Nom de l'axe des abscisses
plt.ylabel("ordonnées") # Nom de l'axe des ordonnées
plt.title("Test")  # titre du graphique
plt.show() # affiche la figure à l'écran
```
__c-__ Construisez par compréhension :  

* une liste d'abscisses composés de nombre entiers compris entre 0 et 50  
* une liste des ordonnées correspondantes à la fonction  f(x)  

Tracez le graphique correspondant.


__2)__ Réalisez ainsi la représentation graphique de la fonction $`f(x) = 2 \times x^{2} + 4`$. pour des valeurs de x comprises entre 0 et 10 par pas de 0.5.  

_Remarque :_ vous ne pourrez pas utiliser un pas de type flottant avec la fonction `range`. Faites autrement.


__3)__ Réalisez la représentation graphique de la fonction $`f(x) = 2 \pi + \sqrt x`$. pour des valeurs de x comprises entre 0 et 100 par pas de 0.1.

