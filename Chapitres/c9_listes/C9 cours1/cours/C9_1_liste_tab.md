---
title: "Chapitre 9-1 Les tableaux et listes en Python "
subtitle: "Thème 2: Données type construit"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---



# Les tableaux en informatique.

## Qu'est-ce qu'un tableau ?

Nous avons vu dans les séances précedentes la possibilité de déclarer des variables en Python.  
On commence par donner un nom à la variable puis on lui affecte une valeur. Dans certains langages, il est impératif de déclarer le type associé à la variable.

Cependant, lorsqu'il y a plusieurs variables à déclarer et que ces variables sont étroitement liées, il est préférable de les regrouper.  

Ainsi, au lieu d'écrire : 

```python
note1 = 8  
note2 = 10  
note3 = 12
```

On utilisera un tableau :

```python
tableau_de_notes = [ 8, 10, 12]
```

- On remarque que les trois notes sont regroupées dans la même structure.  
- Il n'y a qu'un nom pour l'ensemble des variables.  

## Les tableaux en Python

Le langage Python a développé quelques particularités en ce qui concerne les tableaux :  

- Les tableaux en Python s'appellent des listes. C'est un des seuls langages à utiliser cette terminologie.
  
- Le langage Python permet un agrandissement ou un rétrecissement par la droite d'une liste. De nombreux langages fixent une taille de tableau au départ qui ne pourra pas être modifiée.
  
- Enfin il est possible d'accéder à des éléments du tableau en utilisant des indices négatifs.

Toutes ces particularités nous montrent que ce que Python appelle des listes ne sont ni des listes ni des tableaux mais des structures de type construit spécifiques à Python.  

##  Déclarer un tableau

La déclaration d'un tableau dépend du langage utilisé mais en Python, on commence par le nom, un égal puis soit une paire de crochets vides soit un tableau déjà complété.

Première méthode, déclaration d'un tableau vide :

```python
    tableau = []
```

Deuxième méthode, déclaration d'un tableau complété :
```python
    tableau = [ 8, 10, 12]
```

On choisira la méthode en fonction de l'usage du tableau.  
Remarque: On peut aussi déclarer une liste avec l'instruction  __tableau = list()__

## Propriétés communes aux tableaux

- On peut connaitre la taille d'un tableau. C'est une grandeur importante car elle est utile pour parcourir un tableau. En Python, On pourra connaitre la taille d'un tableau avec la fonction len(). Le nom du tableau sera passé en paramètre de cette fonction.
Exemple :

```python
    tableau = [8, 10, 12]
    len(tableau)
    3
```

- Le premier indice d'un tableau est toujours 0.  
  Exemple :

```python
    tableau = [8, 10, 12]
```

Cela donne :  

| tableau | 8 | 10 | 12 |
|:--------|:---:|:---:|:--:|  
| indice  | 0 | 1  | 2  |

- On peut accéder à une valeur d'un tableau en utilisant les indices. Par exemple :  

```python
    tableau = [ 8, 10, 12]
    tableau[1]
    10
```

En mettant l'indice entre crochets à la suite du nom du tableau, on a en retour la valeur du tableau correspondant à cet indice.  

- On peut changer une valeur dans un tableau en utilisant les indices. Par exemple :  

```python
    tableau = [8, 10, 12]
    tableau[0] = 6
    print(tableau)
    [6, 10, 12]
```

Pour modifier une valeur dans un tableau, il faut écrire le nom du tableau suivi entre crochets par l'indice de l'élément qu'on souhaite modifier.

# Propriétés propres aux listes en Python

## Comment ajouter des éléments à la fin d'une liste en Python ?  

- On peut utiliser la méthode append() qui ajoute un élément à droite de la liste. Par exemple :  

```python
    tableau = [8, 10, 12]
    tableau.append(14)
    print(tableau)
    [8, 10, 12, 14]
```

On voit dans cet exemple que la valeur 14 a bien été ajoutée comme dernier élément de la liste.  

Attention, la méthode append() peut ajouter une liste comme dernier élément. On aura donc:  

```python
    tableau = [8, 10, 12]
    tableau.append([14, 16])
    print(tableau)
    [8, 10, 12, [14, 16]]
```

On voit ici que la méthode append() ajoute à la fin non pas les valeurs séparées mais la liste complète.  On a donc une liste à l'intérieur d'une autre liste.

- On peut utiliser la méthode extend() qui ajoute plusieurs éléments à droite de la liste. Par exemple:  

```python
    tableau = [8, 10, 12]
    tableau.extend([14, 16])
    print(tableau)
    [8, 10, 12, 14, 16]
```

Cette fois-ci, les éléments de la liste passée en paramètre de la méthode extend() ont été extraits et placés à droite de la liste nommée "tableau". On peut donc voir la méthode extend() comme étant un moyen du fusionner deux listes en Python.

- On peut aussi ajouter des éléments à droite du tableau par concaténation.

``` python
    tableau = [0, 1, 2]
    tableau = tableau + [4, 5]
    print (tableau)
    [0, 1, 2, 4, 5]
```

## Comment retirer des éléments d'une liste ?  

- On peut utiliser la méthode pop() qui retire l'élément le plus à droite dans une liste.

``` python  
    tableau = [0, 1, 2]
    tableau.pop()
    print(tableau)
    [0, 1]
```

- On peut supprimer un ou plusieurs éléments du tableau avec la fonction del() si on connait leurs indices.  

``` python
    tableau = [0, 2, 4, 6, 8, 10]
    del(tableau[1])
    print(tableau)
    [0, 4, 6, 8, 10]
```

Ici, l'élément d'indice 1 dans le tableau a été supprimé.

``` python
    tableau = [0, 2, 4, 6, 8, 10]
    del(tableau[1:3])
    print(tableau)
    [0, 6, 8, 10]
```

Ici, les éléments d'indices compris entre 1 inclus et 3 exclu sont supprimés.  

- On peut supprimer un élément dont on connait le contenu avec la méthode remove().  

``` python
    tableau = [ 'a', 'b', 'c', 'd']
    tableau.remove('b')
    print(tableau)
    [ 'a', 'c', 'd']
```

Attention: la méthode remove() ne retire que la première occurence rencontrée.  

## Quelques autres méthodes.  

| méthode | utilité |
|:--------|:--:|
| sort() | Trie une liste selon les valeurs du codage des éléments |
| index(élément) | Renvoie l'indice de l'élément passé en paramètre |  
| reverse() | Inverse les éléments de la liste |  
| split(élément séparateur) | Méthode appliquée à une chaine de caractères. Elle coupe la chaine dès qu'elle rencontre l'élément séparateur. Elle stocke les éléments dans une liste. |
| insert(indice,valeur) | Méthode qui insère à l'indice fixé en paramètre 1 la valeur fixée en paramètre 2 |

Remarque: L'ensemble des méthodes présentées dans cette partie II modifie directement la liste. Ces méthodes ne créent pas de nouvelles listes.  

# La mutabilité des listes

## Comprendre la mutabilité d'une structure en python 

Nous allons essayer de trouver une analogie pour comprendre ce phénomène de mutabilité.  Pour cela, nous allons nous intéresser au réseau du lycée.  

- Des élèves d'une même classe copient via une clé USB un fichier nommé "musique" dans leur espace personnel. Ils ont donc chacun un fichier nommé "musique" dans leur espace.  
L'un d'entre eux décide de modifier ce fichier et de l'enregistrer toujours dans son espace. Les autres ne voient aucune modification sur le fichier de leur espace.  

On comprend ici qu'il n'y a pas de modification pour l'ensemble des élèves car les fichiers copiés, qui ont le même contenu au départ, ont été stockés à des emplacements mémoires différents. Si on modifie le contenu d'un emplacement mémoire, on n'affecte pas les autres.

- Maintenant, une copie de ce fichier est placé dans le commun de la classe. Il est accessible par tous les élèves de la classe. Si un élève modifie le contenu du fichier sur le commun, tous les élèves auront le même fichier modifié.  

On voit bien ici que même si les élèves ont un accès depuis des comptes ou ordinateurs différents, ils accèdent au même emplacement mémoire.

## Mutabilité d'une liste  

Prenons un exemple dans lequel nous allons créer une liste "tableau" non vide puis nous allons l'affecter à une autre liste "tableaubis":

``` python
    tableau = [0, 1, 2, 3, 4, 5]
    tableaubis = tableau
```  

Nous allons vérifier si les variables tableau et tableaubis font référence au même espace mémoire. Pour cela, nous allons utiliser la fonction id() qui donnera l'identifiant mémoire des listes.

``` python
    tableau = [0, 1, 2, 3, 4, 5]
    tableaubis = tableau
    print(id(tableau))
    print(id(tableaubis))

    57778904
    57778904
```

les variables tableau et tableaubis référencent la même liste qui se trouve à l'emplacement mémoire dont l'identifiant est 57778904.  

Nous allons vérifier que si nous modifions la liste tableau, la liste tableaubis est aussi modifiée.  

```python
    tableau = [0, 1, 2, 3, 4, 5]
    tableaubis = tableau
    tableaubis.pop()
    print(tableaubis)
    print(tableau)

    [0, 8, 2, 3, 4]
    [0, 8, 2, 3, 4]
```

Tout changement qui sera réalisé sur tableau sera réalisé sur tableaubis et inversement car ils référencent la même liste. Il faudra donc être très prudent lors d'une copie de liste par simple affectation.  

## Comment réaliser une copie de liste indépendante ?  

Pour réaliser une copie de liste qui sera affectée sur un autre espace mémoire, il faut importer la bibliothèque copy.

```python
    from copy import copy
    tableau = [0, 1, 2, 3, 4, 5]
    tableaubis = copy(tableau)
    print(id(tableau))
    print(id(tableaubis))

    57778904
    63775343
```

La fonction copy() réalise donc une copie de liste sur un autre emplacement mémoire ce qui rend les deux listes indépendantes.
