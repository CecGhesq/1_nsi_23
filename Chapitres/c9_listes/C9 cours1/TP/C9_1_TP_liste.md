---
title: "C9_1_TP Les tableaux et listes "
subtitle: "Thème 2: Données type construit"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---  

# QCM  

Question 1 :  
Quelle est l'instruction qui crée une liste ?  

* [ ] liste()
* [ ] [ ]
* [ ] print(liste)
* [ ] { }

Question 2 :  
Quel est le résultat de l'expression suivante ? [0, 1, 2, 3] + [4]  

* [ ] [0, 1, 2, 7]
* [ ] [4, 0, 1, 2, 3]
* [ ] [4, 1, 2, 3]
* [ ] [0, 1, 2, 3, 4]  

Question 3:
Quel est le résultat du programme suivant ?  
```python
>>>liste = [0, 2, 4, 6]
>>>liste.append(5)  
```  

* [ ]  [5, 2, 4, 6]
* [ ]  [5, 0, 2, 4, 6]
* [ ]  [0, 2, 4, 6, 5]
* [ ]  [5]  

Question 4 :
Que fait la méthode pop() ?  

* [ ]  Un air de musique
* [ ]  Elle ajoute un élément au début de la liste.
* [ ]  Elle retire l'élément le plus à droite dans la liste.
* [ ]  Elle trie la liste par ordre croissant.  

# Exercice 1 : Premiers exercices d'applications.  

 1) Créer une fonction qui crée la liste [0, 1, 2] et la retourne. (Exercice très simple !)

 2) Créer maintenant une fonction qui prend comme paramètre un entier n. La fonction retournera une liste contenant par ordre croissant les n premiers entiers naturels (n exclus). On utilisera la boucle __for__.  

 3) Créer une fonction qui prend comme paramètre un entier n.  La fonction retourne une liste contenant n nombres aléatoires compris entre 0 et 10.

 4) Vous allez créer une fonction qui affichera les mois de l'année précédés de leur numéro. On utilisera une liste contenant les mois de l'année et une boucle for pour faire l'affichage.

``` python
    >>> 1 'Janvier', 2 'Février' ...
```
Remarque : On n'utilisera pas range() dans la boucle for. La liste des mois sera utilisée comme séquence dans le for. On utilisera un accumulateur pour les nombres devant les mois.

\newpage

 5) Créer une fonction qui prend deux paramètres, un entier n et une chaine de caractères. Si le deuxième paramètre est 'pair', alors la fonction retourne une liste des premiers entiers pairs jusque n. Si le deuxième paramètre est 'impair', alors la fonction retourne une liste des premiers entiers impairs jusque n. Si le deuxième paramètre n'est ni 'pair' ni 'impair', la liste retournée est vide.  
```python
>>> pair_impair(7,'pair')
[0, 2, 4, 6]
>>> pair_impair(7,'impair')
[1, 3, 5, 7]
>>> pair_impair(7,'pouick')
[]
```
# Exercice 2 : Manipulons les listes
1) Ecrire la fonction `somme(liste)` qui renvoie la somme des éléments d'une liste d'entiers.

```python
>>> somme([3,6,2,6])
17
```

2) Ecrire la fonction `occurrences(liste, valeur)` qui renvoie le nombre de fois où la valeur est rencontrée dans la liste. 
```python
>>> occurrences([6,3,6,3,2,9], 6)
2
```

3) Ecrire la fonction `indices_occurrences(liste, valeur)` qui renvoie une liste contenant les indices où sont placés la valeur dans la liste. Si la valeur est absente, on renvoie -1.
```python
>>> indices_occurrences([1,5,3,6,2,5,3,78,4,5], 3)
[2, 6]
>>> indices_occurrences([1,5,3,6,2,5,3,78,4,5], 7)
-1
```
4) Ecrire la fonction sans_voyelles(chaine) qui renvoie la chaine sans les voyelles. 
On utilisera la fonction list() qui transforme la chaine en liste, puis `''.join(liste)` qui rassemble une liste en chaine.
```python
>>> liste1 = list('joie')
>>> liste1
['j', 'o', 'i', 'e']
>>> ''.join(liste1)
'joie'

>>> sans_voyelles('arbuste')
'rbst'
```

# Exercice 3 : Le bulletin  

 1) Vous allez commencer par créer une fonction __creation_liste()__ qui crée une liste contenant les valeur 10, 14 et 8. La fonction retournera une liste qui sera stockée dans la variable __notes__.

 2) Vous allez créer une fonction __affichage(texte, param)__ qui affiche la liste __notes__ ou autres.

Par exemple :  
 affichage("Notes", notes)

 Notes : [10, 14, 8]
   
 3) Vous allez ensuite créer une fonction __entrer_notes(param)__ qui demande à l'utilisateur de rentrer des notes. Les notes seront stockées dans la liste notes. Pour cela, on demandera à chaque fois si l'utilisateur veut entrer une note. Si la réponse est __oui__ on demande la note, on la stocke et on repose la question, si la réponse est __non__ on retourne la liste complétée. Si la réponse est différente de __oui ou non__ on repose la question après avoir précisé que l'entrée était mauvaise.  

 4) Vous allez créer une fonction __calcul_moyenne(param)__ qui retourne la moyenne des éléments de la liste __notes__.

 Fonction principale : 

 - Vous allez créer une fonction appelée __principale()__  
 - Vous allez commencer par créer la liste __notes__ remplie des trois valeurs.  
 - Vous allez ensuite afficher la liste __notes__
 - vous demanderez ensuite s'il faut ajouter des notes. Puis un fois terminé on affiche de nouveau la liste __notes__
 - Enfin, vous calculez la moyenne des valeurs de la liste __notes__ et vous l'affichez.

Questions complémentaires :  

- On pourra créer une fonction qui recherche la meilleure note. (On n'utilisera pas une méthode existante.)  

- On pourra chercher la note minimale. (On n'utilisera pas une méthode existante.) 

- On pourra proposer de supprimer la dernière note entrée.  

- On pourra proposer de supprimer une note dont on connait la valeur.  

    
# Exercice 4 : Modifications de listes

_(extraits de : "Numériques et sciences informatiques", Balabonski, Conchon, Filiâtre, Nguyen)_

__1)__ Définissez une fonction `echange(tab, i, j)` qui échange dans le tableau `tab` les éléments aux indices `i` et `j`. 
Cette fonction n'a pas de valeur de retour, mais un __effet de bord__ : elle modifie la liste passée en paramètre.
Pour le vérifier créer d'abord le tableau, puis appeler la fonction et enfin évaluer de nouveau le tableau.


__2)__ Définissez une fonction `miroir(tab)` qui accepte comme paramètre un tableau et le modifie pour échanger le premier élément avec le dernier, le second avec l'avantdernier etc. Vous utiliserez la fonction `echange`

\newpage 

# Exercice 5 : Analyse de code

__Sans tester avec Thonny__, pour chacun des cas suivants expliquer l'erreur commise et corriger-la:

__a)__  

```python
liste = []
liste[0] = 'a'
```

__b)__  

```python
liste = [0, 1, 2, 3]
liste = liste + 4
```


__c)__  

```python
films= ['Un nouvel espoir', "L'Empire contre-attaque", 'Le Retour du Jedi']
for i in range(4):
    print(films[i])
```

__d)__  

```python
films= ['La Menace fantôme', "L'Attaque des clones", 'La Revanche des Sith']
for film in len(films):
    print(film)
```

__e)__  

```python
films= ['Le Réveil de la Force', "Les Derniers Jedi", "L'Ascension de Skywalker"]
print("Bientôt au cinéma :",films[len(film)])
```

# Exercice 6 :  Mutabilité des listes

(_Extrait de Eduscol, Ressources pour NSI_)

__1)__  Que vaut `s` après ces instructions ? (Expliquer)

```python
t= [1,2,3]
s= t
t[0]= 5 
```


__2)__  Que vaut `s` après ces instructions ? (Expliquer)

```python
def truc(t):
    t.append(0)

t=[4]
s = [1]
truc(s)
```

__3)__ Que se passera-t-il après ces instructions ? (Expliquer)

```python
s = list('abce')
s[3]='d'
```

# Exercice 7 : Cryptographie  le code de César.  

C'est une méthode ancienne de cryptographie qui consiste à réaliser un décalage constant dans l'ordre alphabétique. Ce mode de cryptographie a été rapidement abandonné car une fois qu'on connait la méthode, le décryptage est très simple. Cependant, vous allez dans cet exercice reproduire le code de César.  

 1) Dans un premier temps, vous allez créer une fonction qui retourne une liste comportant l'ensemble de l'alphabet dans l'ordre croissant. Cette liste sera stockée dans une variable nommée __alphabet__
``` python  
    >>>[ 'a', 'b', ...]
```

 2) Dans un deuxième temps, vous allez créer une fonction __decalage(n, param_alphabet)__ qui prendra en paramètres un entier n qui représente le décalage à effectuer et la variable alphabet. La fonction retournera une liste nommée __liste_decalee__ qui aura subi un décalage de n. (On pourra utiliser les méthodes .pop() et insert())  
``` python
    liste_decalee = decalage(2,alphabet)
    print(liste_decalee)
    ['y', 'z', 'a', 'b', 'c', ...]
```
Remarque: Attention, il faut prendre en compte la mutabilité des listes.


 3) Vous allez créer une fonction qui va demander à l'utilisateur le mot à crypter. Cette fonction retournera une chaine de caractères qui sera sotckée dans la variable __mot__. (On se contentera d'un mot pour le moment.)

 4) Vous allez créer une fonction qui va demander à l'utilisateur la clé de cryptage c'est à dire ici le décalage souhaité. La fonction retournera un entier stocké dans une variable nommée __valeur__

 5) Créer une fonction qui prendra trois paramètres, la lettre à crypter, la variable alphabet et la variable liste_decalee. Elle retournera la valeur cryptee du caractère passé en paramètre. (On pourra utiliser la méthode .index())

 6) Vous allez créer une fonction qui affiche un caractère passé en paramètre.

Fonction principale :  

 - Créer une liste représentant l'alphabet dans l'ordre croissant stockée dans la variable __alphabet__
 - Demander à l'utilisateur la clé de cryptage.
 - Créer une liste décalée en utilisant la clé de cryptage et la variable __alphabet__.  
 - Demander le mot à l'utilisateur.  
 - Crypter une lettre du mot et l'afficher. (On répètera cette opération pour toutes les lettres du mot.)
