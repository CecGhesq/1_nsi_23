---
title: "C9_2_TP listes avancées "
subtitle: "Thème 2: Données type construit"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

# QCM  

__Question 1 :__  
Que va retourner l'expression suivante: [2*i+1 for i in range(5)]

* [ ]  [5, 5, 5, 5, 5]  
* [ ]  [1, 3, 5, 7, 9]  
* [ ]  [2*i+1, 2*i+1, 2*i+1, 2*i+1]
* [ ]  [1, 3, 5, 7]  

__Question 2:__  
Que va faire ce code?  
```python
   liste = [[12, 13, 14], [6, 16, 18], [9, 14, 17]]
   for j in range(3):
      for i in range(3):
         print(liste[j][i], end=" ")
      print("")
```
* [ ]  Remplir la liste avec des espaces.  
* [ ]  Supprimer la liste.  
* [ ]  Afficher la liste.  
* [ ]  Créer une liste à deux dimensions.  

__Question 3:__  
Comment créer une liste qui contient 4 listes de 6 éléments?  

* [ ]  [0 for i in range(6)]  
* [ ]  [[0 for i in range(4)] for j in range(6)]  
* [ ]  [[0 for i in range(6)] for j in range(4)]
* [ ]  [[0 for i in range(4)] for j in range(4)]  

__Question 4:__  
De quelle dimension est cette liste?  
[ [ [0, 1] , [6, 4]],[[5, 6], [7, 8] ] ]  

* [ ] Liste impossible.  
* [ ] Liste de dimension 2.  
* [ ] Liste de dimension 1.  
* [ ] Liste de dimension 3.  

# Exercice n°1: Premières applications de listes par compréhension.  

1) Créer une liste par compréhension qui contient  les 20 premiers entiers en commençant par 0.  

2) Créer une liste par compréhension qui contient les éléments [3, 6, 9, ...,24].  

3) Créer une listepar compréhension en deux dimensions 4x4 qui ne contient que des 0.  

# Exercice n°2 :  

1) Créer une fonction comportant deux paramètres u0 et q qui sont le premier terme de la suite et q la raison. On retournera les 5 premiers éléments de cette suite géométrique. On créera une liste par compréhension.  

2) Créer une liste par compréhension à deux dimensions 3x3 qui comportent un 0 si la somme de l'indice de ligne et l'indice de colonne est paire sinon un 1.  

3) Créer une liste par compréhension à deux dimensions 4x4 qui comportent uniquement les 16 premiers entiers impairs.  

4) Créer une liste par compréhension qui a pour affichage:  
(On créera une fonction pour créer la liste et on utilisera le cours pour créer une fonction qui affichera cette liste.)  

1 0 0 0 0 0 0 0 0 0  
0 1 0 0 0 0 0 0 0 0  
0 0 1 0 0 0 0 0 0 0  
0 0 0 1 0 0 0 0 0 0  
0 0 0 0 1 0 0 0 0 0  
0 0 0 0 0 1 0 0 0 0  
0 0 0 0 0 0 1 0 0 0  
0 0 0 0 0 0 0 1 0 0  
0 0 0 0 0 0 0 0 1 0  
0 0 0 0 0 0 0 0 0 1   

5) Créer une liste par compréhension qui a pour affichage:

1 0 0 0 0 0 0 0 0 0  
1 1 0 0 0 0 0 0 0 0  
1 1 1 0 0 0 0 0 0 0  
1 1 1 1 0 0 0 0 0 0  
1 1 1 1 1 0 0 0 0 0  
1 1 1 1 1 1 0 0 0 0  
1 1 1 1 1 1 1 0 0 0  
1 1 1 1 1 1 1 1 0 0  
1 1 1 1 1 1 1 1 1 0  
1 1 1 1 1 1 1 1 1 1  

6) Créer une liste par compréhension qui a pour affichage:  

1 0 0 0 0 0 0 0 0 1  
0 1 0 0 0 0 0 0 1 0  
0 0 1 0 0 0 0 1 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 1 0 0 0 0 1 0 0  
0 1 0 0 0 0 0 0 1 0  
1 0 0 0 0 0 0 0 0 1  

# Exercice n°3: Carré magique  
Un carré d'ordre n est un tableau carré contenant n² entiers strictement positifs.
On dit qu'en carré d'ordre n est magique si :  

•	Il contient tous les nombres entiers de 1 à n² ;  
•	Les sommes des nombres de chaque rangée, les sommes des nombres de chaque colonne et les sommes des nombres de chaque diagonale principale sont égales.  
On modélise un carré par une liste de listes de nombres.  
Exemples :
![](carre.jpg)
1. Ecrire une fonction `get_line(carre, j)` permettant de renvoyer les éléments d'une ligne d'indice j du carré( ce sera une liste)  
2. Ecrire une fonction `get_values(carre,i,j)` permettant de récupérer une valeur de colonne i et de ligne j du carré.  
3. Ecrire la fonction `somme_ligne(carre, j)` permettant de réaliser la somme des éléments d'une ligne d'indice j du carré.  
4. Ecrire la fonction `somme_colonne(carre, i)` permettant de calculer la somme des éléments d'une colonne d'indice i du carré.  
5. Ecrire la fonction `somme_diagonale_1(carre)` permettant de calculer la somme des valeurs dans la diagonale du carré dans laquelle l'indice de la ligne et colonne sont identiques ( i = j).  
6. Ecrire la fonction `somme_diagonale_2(carre)` permettant de calculer la somme des valeurs dans l'autre diagonale.  
7. Ecrire le prédicat vérifiant qu'un carré est magique.  Le tester avec les deux carrés proposés en exemple.


# Exercice 4 TIC TAC TOE
Le but de cet exercice n'est pas de faire le jeu (Vous pourrez le terminer en classe ou chez vous si vous avez le temps.)  
On souhaite juste créer deux fonctions:  

- La première fonction crée une liste à deux dimensions avec deux boucles for imbriquées (pas une liste par compréhension).  
- La deuxième affiche la liste à deux dimensions qui représente le jeu du tic-tac-toe.  

```python
    -------  
    | | | |  
    -------  
    | | | |  
    -------  
    | | | |  
    ------- 
```

Remarque: Si vous avez le temps pour faire le jeu du TIC-TAC-TOE, il est conseillé de réutiliser des fonctions développées dans le projet NIM.  

- Comment doit-on modifier les fonctions de création et d'affichage de liste pour faire l'affichage de la grille du jeu de puissance 4 par exemple?  (On pourra par la suite afficher n'importe qu'elle grille.)  

```python
   ---------------
   | | | | | | | |
   ---------------
   | | | | | | | |
   ---------------
   | | | | | | | |
   ---------------
   | | | | | | | |
   ---------------
   | | | | | | | |
   ---------------
   | | | | | | | |
   ---------------
```
