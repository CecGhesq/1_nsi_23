#tri ex 6
def insert(liste, index) :
    a_placer = liste[index]
    i = index-1
    while i>=0 and a_placer <liste[i]:
        liste[i+1]=liste[i]
        i = i-1
    liste[i+1] = a_placer
    return liste

    
def i_sort(liste):
    for i in range(len(liste)):
        liste_triee = insert(liste, i)
    return (liste_triee)

def valeur_plus_presente(liste):
    liste_triee=i_sort(liste)
    frequence=1
    frequence_max=1
    valeur_frequente=liste[0]
    for i in range(len(liste_triee)-1):
        if liste_triee[i]==liste_triee[i+1]:
            frequence=frequence+1
        else:
            frequence=1
        if frequence> frequence_max:
            frequence_max=frequence
            valeur_frequente=liste[i]
    return valeur_frequente

liste=[1,5,8,3,4,4,2,7,3,4,3,6,4]
liste_triee=[1, 2, 3, 3, 3, 4, 4, 4, 4, 5, 6, 7, 8]       
    
valeur_plus_presente(liste_triee)
