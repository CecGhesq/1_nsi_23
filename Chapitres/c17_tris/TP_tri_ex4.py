#tri_ex4
def histogramme(liste):
    '''renvoie la fréquence de répétition d'une valeur dans une lsite triée
    entrée : liste de longueur non nulle triée
    sortie : affichage de la valeur et de sa fréquence
    '''
    assert len(liste)>0
    valeur = liste[0]
    frequence = 1
    for i in range (1,len(liste)): # attention à bien démarrer à 1 sinon la première valeur est comptée une fois de trop
        if liste[i] == valeur:
            frequence = frequence +1
        else :
            print(frequence,"fois",valeur) # 
            valeur = liste[i]
            frequence = 1
    print(frequence,"fois",valeur) # obligatoire pour écrire la dernière valeur

liste=[1,1,1,5,6,6,7,7,8]
histogramme(liste)