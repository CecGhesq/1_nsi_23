#exercice 5
# Une solution est de compter le nombre de 0 puis de créer la nouvelle liste
def tri_deux_valeurs(liste):
    '''trie une liste ne contenant que deux valeurs
    entrée : liste avec deux valeurs différentes
    sortie : liste triée
    '''
    nbre_0 = 0
    for elt in liste:
        if elt == 0:
            nbre_0 = nbre_0+1
    return [0]*nbre_0 + [1]*(len(liste)-nbre_0)
        
    
    
liste_bool=[0,0,1,1,0,1,1,0,0]