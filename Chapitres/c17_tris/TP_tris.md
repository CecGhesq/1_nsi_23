---
title: "Les algorithmes de tri"
subtitle: "TP"
papersize: a4
geometry: margin=2cm
fontsize: 12pt
lang: fr
---

## Exercice 1  
1. Détailler les différentes étapes du programme suivant sur la liste suivante : liste=[3,7,5,12,8] en remplissant le tableau ci-dessous:

```python
def echange(t,i,j):
    ''' entrées : t est une liste à trier ; i et j des index de la liste'''
    tmp = t[i]
    t[i] = t[j]
    t[j] = tmp

def tri_par_selection(t):
    '''entrée : liste à trier'''
    for i in range (len(t)):
    # t [0,i[ est trié et <= à t[i..[
    # on cherche le minimum de t[i..[
        m=i
        for j in range (i+1, len(t)):
            if t[j] <t[m]:
                m=j
        echange (t,i,m)
```

|            | valeur de i | minimum de t[i:] | valeur de m | échange effectué |
|------------|-------------|-----------------|-------------|:----------------:|
| [  ,  ,  ,  ,  ]           |             |                 |             |  echange( , , )  |
|  [  ,  ,  ,  ,  ]    |             |                 |             |                  |
| [  ,  ,  ,  ,  ] |             |                 |             |                  |
| [  ,  ,  ,  ,  ] |             |                 |             |                  |
| [  ,  ,  ,  ,  ] |             |                 |             |                  |

1. Vérifier votre résultat en utilisant le lien vers 
[Python Tutor](http://pythontutor.com/visualize.html#code=def%20echange%28t,i,j%29%3A%0A%20%20%20%20'''%20entr%C3%A9es%20%3A%20t%20est%20une%20liste%20%C3%A0%20trier%20%3B%20i%20et%20j%20des%20index%20de%20la%20liste'''%0A%20%20%20%20tmp%20%3D%20t%5Bi%5D%0A%20%20%20%20t%5Bi%5D%20%3D%20t%5Bj%5D%0A%20%20%20%20t%5Bj%5D%20%3D%20tmp%0A%0Adef%20tri_par_selection%28t%29%3A%0A%20%20%20%20'''entr%C3%A9e%20%3A%20liste%20%C3%A0%20trier'''%0A%20%20%20%20for%20i%20in%20range%20%28len%28t%29%29%3A%0A%20%20%20%20%23%20t%20%5B0,i%5B%20est%20tri%C3%A9%20et%20%3C%3D%20%C3%A0%20t%5Bi..%5B%0A%20%20%20%20%23%20on%20cherche%20le%20minimum%20de%20t%5Bi..%5B%0A%20%20%20%20%20%20%20%20m%3Di%0A%20%20%20%20%20%20%20%20for%20j%20in%20range%20%28i%2B1,%20len%28t%29%29%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20t%5Bj%5D%20%3Ct%5Bm%5D%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20m%3Dj%0A%20%20%20%20%20%20%20%20echange%20%28t,i,m%29%0Atri_par_selection%28%5B3,7,5,12,8%5D%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

3. Dans la toute dernière étape du tri il n'y a plus qu'une seule valeur dans la partie droite et il est donc inutile d'y chercher la plus petite valeur ni de l'échanger avec elle-même. Que faut-il modifier le programme précédent pour éviter cette étape inutile?

## Exercice 2
. Détailler les différentes étapes du programme suivant sur la liste suivante : liste=[3,7,5,12,8]en remplissant le tableau ci-dessous:

```python
def insere(t,i,v):
    """insere v dans t[0..i[ supposé trié"""
    j=i
    while j > 0 and t[j- 1] > v :
        t[j] = t[j-1]
        j= j-1
    t[j] = v

def tri_par_insertion(t):
"""trie le tableau t dans l'ordre croissant"""
    for i in range(1, len(t)):
        #invariant : t[0..i[ est trié
        insere(t,i,t[i])
```

|            | valeur de i | insertion effectuée |
|------------|-------------|---------------------|
| [  ,  ,  ,  ,  ]           |             |    insere(       )  |
| [  ,  ,  ,  ,  ] |             |                     |
| [  ,  ,  ,  ,  ] |             |                     |
| [  ,  ,  ,  ,  ] |             |                     |
| [  ,  ,  ,  ,  ] |             |                     |

1. Vérifier votre résultat en utilisant le lien vers 
[Python Tutor](http://pythontutor.com/visualize.html#code=def%20insere%28t,i,v%29%3A%0A%20%20%20%20%22%22%22insere%20v%20dans%20t%5B0..i%5B%20suppos%C3%A9%20tri%C3%A9%22%22%22%0A%20%20%20%20j%3Di%0A%20%20%20%20while%20j%20%3E%200%20and%20t%5Bj-%201%5D%20%3E%20v%20%3A%0A%20%20%20%20%20%20%20%20t%5Bj%5D%20%3D%20t%5Bj-1%5D%0A%20%20%20%20%20%20%20%20j%3D%20j-1%0A%20%20%20%20t%5Bj%5D%20%3D%20v%0A%0Adef%20tri_par_insertion%28t%29%3A%0A%20%20%20%20%22%22%22trie%20le%20tableau%20t%20dans%20l'ordre%20croissant%22%22%22%0A%20%20%20%20for%20i%20in%20range%281,%20len%28t%29%29%3A%0A%20%20%20%20%20%20%20%20%23invariant%20%3A%20t%5B0..i%5B%20est%20tri%C3%A9%0A%20%20%20%20%20%20%20%20insere%28t,i,t%5Bi%5D%29%0Atri_par_insertion%28%5B3,7,5,12,8%5D%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

3. Que se passe-t-il lorsque le tri par insertion est appliqué à un tableau qui se présente en ordre décroissant?


## Exercice 3

1. Ecrire une fonction `premiers_classés` qui prend en paramètres une liste_1 et un entier k supposé inférieur à la longueur de la liste_1 et qui renvoie une liste_2 contenant dans l'ordre, les k premiers éléments de la liste_1. On pourra s'inspirer de l'exercice précédent et utiliser `insere()`.
   
2. Ecrire une fonction `k_premiers_classés` qui prend en paramètres une liste_1 et un entier k supposé inférieur à la longueur de la liste_1 et qui renvoie une nouvelle liste contenant dans l'ordre, les k plus petits éléments de la liste_1. On pourra s'inspirer de l'exercice précédent et utiliser `insere()`.

## Exercice 4
Ecrire une fonction qui prend en argument un tableau d'entiers **trié** et affiche son contenu sous la forme d'un histogramme c'est à dire quelque chose comme :
*1 fois 0*
*2 fois 2*
*1 fois 3*
*4 fois 7*

## Exercice 5

Ecrire une fonction `tri_deux_valeurs`qui renvoie un tableau trié  (reçu en argument) qui ne contient que deux valeurs différentes. On pourra supposer qu'il s'agit des entiers 0 et 1 pour simplifier.

## Exercice 6

En se servant du tri par insertion, écrire une fonction qui prend en argument un tableau d'entiers et renvoie la valeur la plus fréquente du tableau.
Aide : une fois le tableau trié, les valeurs égales se retrouvent côte à côte dans le tableau.
