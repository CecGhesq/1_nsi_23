---
title: "C17 les algorithmes de tri"
subtitle: "Thème 7 : Algorithmique"
papersize: a4
geometry: margin=2cm
fontsize: 12pt
lang: fr
---

||||
|---|---|---|
|Tris par insertion, par sélection|Ecrire un algorithme de tri. Décrire un invariant de boucle qui prouve la correction des tris par insertion, par sélection|La terminaison de ces algorithmes est à justifier. On montre que leur coût est quadratique dans le pire des cas|

Les tris sont des algorithmes fondamentaux, certainement les plus utilisés.

Il existe de très nombreux algorithmes de tri. Cette activité propose de mettre en œuvre au moins un algorithme de tri simple.

En informatique _trier_ signifie _ordonner_. On trie par exemple une liste d'entiers du plus petit élément au plus grand élément, ou une liste de mots comme dans un dictionnaire.

Trier nécessite donc une relation d'ordre donnée par un _opérateur de comparaison_. Pour trier une liste d'entiers du plus petit au plus grand, on utilise la relation d'ordre habituelle « inférieur ou égale », c'est-à-dire l'opérateur $\leq$. Pour trier une liste de mots suivant l'ordre du dictionnaire, on utilise l'ordre lexicographique défini à partir de l'ordre des lettre de l'alphabet. 

----
# Python trie ! 

* Python propose une méthode `sort()` prédéfinie sur les listes qui s'utilise de la manière suivante :

```python
>>> liste_1= ["yacc", "python", "zopl", "lisp", "bcpl"]
>>> liste_1.sort()
>>> liste_1
['bcpl', 'lisp', 'python', 'yacc', 'zopl']
>>> li = [3, 6, 2, 1, 7]
li = [3, 6, 2, 1, 7]
>>> li.sort()
>>> li
[1, 2, 3, 6, 7]	
```


On remarque que la méthode `sort()` modifie la liste sur laquelle elle est appliquée. On dit que la liste est triée, plus généralement modifiée, _en place_.

On remarque aussi que la méthode `sort()` trie aussi bien une liste de chaînes de caractères qu'une liste d'entiers.

* La méthode `sort()` se base sur l'opérateur de comparaison $\leq$ qui compare aussi bien des chaînes de caractères que des entiers :

```python
>>> 3 <= 7 
True
>>> "pascal" <= "python"
True
```

# Trions avec Python

L'objet des exercices suivant est d'écrire « à la main » des fonctions de tri de listes en Python.
On n'utilisera bien évidement pas la méthode `sort()`. 

## Tri par sélection

### Principe

Le principe du tri par sélection est le suivant :

* on cherche le plus petit élément de la liste
* on échange le 1er élément de la liste avec cet élément

<!-- --> 
* on recommence avec le 2e élément, puis le 3e...

<a title="Joestape89 [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Selection-Sort-Animation.gif"><img width="64" alt="Selection-Sort-Animation" src="https://upload.wikimedia.org/wikipedia/commons/9/94/Selection-Sort-Animation.gif"></a>

<span style="color:cyan">Activité : 
A l'aide des cartes proposées, réaliser le tri par sélection </span>

### Programmation
Deux fonctions sont donc utiles pour mettre en œuvre le tri par sélection :

* une fonction `index_min()` pour identifier le plus petit
  élément
* une fonction `swap()` pour échanger deux éléments d'une liste

**Proposez une fonction `index_min()` qui prend en paramètre une liste et un index de départ, renvoie l'index du plus petit élément de la liste dont l'index est supérieur ou égal à l'index donné.**

On a par exemple

```python
>>> index_min(["curl", "bash", "python", "cilk", "nesl"], 0)
1
>>> index_min(["curl", "bash", "python", "cilk", "nesl"], 2)
3
```

```python
# A vous
```

**Proposez une fonction `swap()`{.py} qui prend en paramètre une liste et deux index. Cette fonction modifie la liste en échangeant les deux éléments dont les index sont donnés.**

On a par exemple

```python
>>> liste_2 = ["perl", "vhdl", "java", "python", "caml"]
>>> liste_2
['perl', 'vhdl', 'java', 'python', 'caml']
>>> swap(liste_2, 1, 4)
>>> liste_2
['perl', 'caml', 'java', 'python', 'vhdl']
```

```python
# A vous
```

Une fonction de tri par sélection d'une liste va donc pour chaque élément de la liste :

* trouver l'élément de rang supérieur le plus petit
* échanger ces deux éléments

**Proposez une fonction `s_sort()` qui trie une liste donnée en paramètre.**

```python
>>> liste_3 = [ "ruby", "python", "logo", "elan", "rust"]
>>> s_sort(liste_3)
>>> liste_3 
['elan', 'logo', 'python', 'ruby', 'rust']
```


```python
# A vous
```

### Efficacité de l'algorithme
Est-ce une façon efficace de trier? Quel temps faut-il au tri par sélection pour trier un tableau de 1000 éléments, 10 000 éléments, 1 million....?
Ainsi si on crée des tableaux  contenant des valeurs aléatoires et qu'on mesure le temps de tri, on obtient le tableau et le graphique suivant:
![](./eff_tri.jpg)
On observe que le temps de calcul augmente rapidement avec la taille du tableau. Celui-ci est de plus non proportionnel à la taille du tableau : pour un tableau de taille deux fois supérieure, le temps nécessaire est plus de deux fois supérieur.
On peut calculer ceci: 

* le tri par sélection parcourt dans une première étape tout le tableau : le temps est proportionnel à la taille du tableau (taille du tableau nommée **n**)
* dans la deuxième étape, on parcourt **n-1** cases du tableau
* dans la troisième étape on parcourt **n-2** cases du tableau
* et ainsi de suite jusque la dernière étape avec la dernière valeur  

La somme est donc égale à n+ n-1 + n-2 + n-3 + ...+ 1.

Ceci est égal à $\frac{n(n+1)}{2}$  

On remarque que cette valeur croît beaucoup plus vite que n. Elle est en n² : on dit  que **le coût est quadratique**.

### Analyse de l'algorithme

* L'algorithme ne contient que des boucles bornées donc il se termine nécessairement.
* A la fin de la boucle `for i in range` le tableau obtenu est un tableau ne contenant que des valeurs du tableau initial, et les i premiers éléments de ce tableau coïncident avec ceux du tableau final : c'est **un invariant de boucle**, donc l'algorithme est correct.

## Tri par insertion

### Principe

Le tri par insertion est un autre tri assez naturel. On considère une partie de la liste qui est déjà triée, et on insère à la bonne place le premier élément qui vient de la partie non triée. En répétant cette action sur tous les éléments de la liste du deuxième au dernier, on arrive à trier la liste complète.

<span style="color:cyan">Activité : 
A l'aide des cartes proposées, réaliser le tri par insertion </span>

### Programmation

**Proposez une fonction `insert()` qui prend en paramètre une liste `liste` et un indice `i`.La liste est triée sur les `i` premiers éléments (indicés de `0` à `i - 1`). Cette fonction modifie la liste en insérant `liste[i]` à sa place. À la sortie, `liste` est triée sur les `i + 1` premiers éléments (indicés de `0` à `i`).**

```python
>>> liste_3 = [ "ruby", "python", "elan", "logo", "rust"]
>>> insert(liste_3,1)
>>> liste_3
[ 'python', 'ruby', 'elan', 'logo', 'rust']
>>> insert(liste_3,2)
>>> liste_3
[ 'elan', 'python', 'ruby', 'logo', 'rust']
>>> insert(liste_3,3)
>>> liste_3
[ 'elan', 'logo', 'python', 'ruby', 'rust']
```

```python
# A vous
```

**Proposez une fonction `i_sort()` qui trie une liste donnée en paramètre en utilisant la fonction `insert()`**

```{.py}
>>> liste_3 = [ "ruby", "python", "elan", "logo", "rust"]
>>> isort(ll3)
>>> liste_3 
['elan', 'logo', 'python', 'ruby', 'rust']
```

```python
# A vous
```

### Efficacité de l'algorithme

Dans le pire des cas, le tri par insertion n'est pas meilleur que le tri par sélection. Il faut alors 1 + 2 + 3 + 4 + ... + (n-2) + (n-1) opérations pour trier le tableau de taille n. Cependant cette méthode de tri peut être meilleure si le tableau est déjà trié en partie. En effet si une partie est triée, le corps de boucle du while ne sera pas atteinte. Le temps sera nettement réduit. 
On retiendra donc d'une manière générale que  que le tri par insertion se comporte favorablement si la tableau est "presuqe trié" par rapport au tri par sélection. 

### Analyse de l'algorithme

* la boucle `while` est dangereuse car ne peut jamais se terminer; il y a deux conditions dans cette boucle de la forme "condition1 **and** condition2". La boucle s'arrête lorsque **not**(condition1 **and** condition2) c'est à dire que lorsque **not**condition1 **or** **not**condition2 est vérifiée. Au moins une des *conditions contraires* doit être vérifiée pour que la boucle s'arrête. Or comme on décrémente à chaque étape i, celui-ci sera négatif et donc nous sommes assurés que la boucle s'arrêtera.
* Soit P(i) la propriété : P(i) "le tableau [t[0], t[1],.... , t[i-1]] est trié" ; on peut montrer que pour tout i variant de 1 à n-1, donc à chaque itération, P(i)est vraie. Comme P(n-1) signifie que le tableau entier est trié, l'algorithme est correct. P(i) est un **invariant de boucle**.

## Tri à bulles (ou par propagation)

Le principe est de comparer deux élements consécutifs d'un tableau et de les permuter si le second est plus grand que le premier. On continue de trier tant qu'il y a eu une permutation. Il porte ce nom puisque les plus grands éléments "remontent" en fin de liste (comme les bulles d'air à la surface d'un liquide).
<p><a href="https://commons.wikimedia.org/wiki/File:Sorting_bubblesort_anim.gif#/media/Fichier:Sorting_bubblesort_anim.gif"><img src="https://upload.wikimedia.org/wikipedia/commons/5/54/Sorting_bubblesort_anim.gif" alt="Sorting bubblesort anim.gif"></a><br>Par <a href="//commons.wikimedia.org/w/index.php?title=User:Simpsons_contributor&amp;action=edit&amp;redlink=1" class="new" title="User:Simpsons contributor (page does not exist)">Simpsons contributor</a> — <span class="int-own-work" lang="fr">Travail personnel</span>, <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=14827645">Lien</a></p>
           
Il existe plusieurs manière de coder cet algorithme. L'une d'elles consiste à prendre en compte le fait que pour une liste de taille n, à la fin de la première étape, le plus grand élément est en fin de liste, et donc il reste à trier les n-1 premiers éléments :

* étape 1 : on parcourt la liste et on permute deux éléments consécutifs s'ils ne sont pas rangés
* étape 2 : on parcourt la liste *- le dernier élément* et on permute deux éléments consécutifs s'ils ne sont pas rangés
* on itère le processus jusqu'à ce qu'il ne reste qu'une liste de 2 éléments

**En utilisant la fonction `swap()` déjà codée pour le tri par sélection, proposer une fonction `b_sort` qui modifie la liste en place. Elle prend en paramètre une liste `l`.**


L'une des "optimisations" possibles de ce tri est de l'interrompre dès qu'il n'y a pas eu de permutations en parcourant le tableau en désordre, ce qui signifie que le tableau est trié en entier. 


```python
# A vous
```

* Sources : 
  * cours à la faculté de Lens par Benoît Fortin
  * les vrais exos Nathan ISBN : 978-209-157465-3
  * NSI ellipses T. Balabonski S. Conchon JC Filliatre K Nguyen
  * [Présentation des algorithmes de tri et codage](http://lwh.free.fr/pages/algo/tri/tri_selection.html)
  * [Un programme Javascript pour la comparaison de tri](http://lwh.free.fr/pages/algo/tri/comparaison_tri.html)
