---
title: "Chapitre 18 : Recherche dichotomique dans une liste triée"
subtitle : "Cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Tout le monde connait le jeu consistant à deviner un nombre en se voyant répondre 'plus grand' ou 'plus petit'. Ce principe est connu en informatique sous le nom de __diviser pour régner__ et il est appliqué dans de nombreux algorithmes. La recherche dichotomique en est l'expression la plus simple.

# Principe de la recherche dichotomique[^1]

> Le principe de la recherche par dichotomie d'un élément dans une liste triée consiste à comparer cet élément avec l'élément situé au milieu de la liste :
>* si ils sont égaux, on a trouvé une solution, la recherche s'arrête
>* si l'élément recherché est supérieur, il est impossible qu'il se trouve avant le milieu dans la liste , et il ne reste à traiter que la moitié(droite) de cette liste;
>* de même si l'élément est inférieur à l'élément situé au milieu, on ne peut le trouver que dans la moitié gauche de la liste.  
> On continue ainsi la recherche, en diminuant de moitié le nombre d'élèments de la liste restant à traiter, après chaque comparaison; et si on effectue la recherche  dans une liste ne contenant aucun élèment, la recherche se termine par un échec : l'élément n'est pas présent dans la liste.

# Exemple de mise en oeuvre en Python

```python
def recherche_dichotomique(tab, val):
    """ 
    Recherche la position d'un entier dans un tableau trié
    """
    gauche = 0
    droite = len(tab) - 1
    while gauche <= droite:
        milieu = (gauche + droite) // 2
        if tab[milieu] < val :
            gauche = milieu + 1
        elif tab[milieu] > val:
            droite = milieu - 1
        else :
            return milieu
    return None
```

Afin de mettre en oeuvre la recherche dichotomique, on délimite la portion du tableau `tab` dans laquelle la recherche de `val` est effectuée à l'aide de deux indices appelés `gauche` et `droite`.
Autrement dit, avec une notation Python on cherche `val` dans `tab[gauche:droite + 1]`. 

Déroulé sur un exemple :

![Exemple de recherche dichotomique](./fig/exemple.jpg)

# Terminaison du programme[^2]

Pour s'assurer que le programme fonctionne correctement il faut notamment s'assurer qu'il renvoie bien un résultat. Ici est-on sûr de sortir de la boucle ?

Pour prouver que c’est bien le cas, nous allons utiliser un __variant de boucle__.

> 📢 Un __Variant de boucle__ est  une quantité entière qui :  
> * doit être positive ou nulle pour rester dans la boucle ;
> * doit décroître strictement à chaque itération.

Si l’on arrive trouver une telle quantité, il est évident que l’on a nécessairement sortir de la boucle au bout d’un nombre fini d’itérations, puisque un entier positif ne peut décroître infiniment.

Pour  le  cas  qui  nous  occupe,  un  variant  est  très  facile  à  trouver  :  il  s’agit  de  la  __largeur  de  la  quantité__   `droite - gauche` .

* La condition de boucle étant `gauche <= droite`, on en déduit tout d'abord que `droite-gauche` est forcément positif ou nul. 
* D'autre part, à chaque itération de la boucle `droite-gauche` décroît au moins d'une unité. En effet  `gauche <= milieu <= droite`, trois cas sont alors possibles: 
  * si `tab[milieu] == val`, on sort directement de la boucle à l’aide d’un return. La terminaison est assurée.
  * si `tab[milieu] > val`, on modifie la valeur de droite. Cette nouvelle valeur est égale à $`milieu - 1`$.  Et donc la nouvelle largeur est  $`(milieu - 1 - gauche)`$ strictement inférieure à la précédente => __le variant a strictement décru__.  
  * sinon si `tab[milieu] < val` on modifie `gauche`. Avec un raisonnement similaire on montre que la nouvelle valeur de gauche est : $`(milieu + 1)`$. Et donc la nouvelle largeur est égale à $`droite - (milieu + 1)`$  strictement inférieure à la précédente => __le variant a strictement décru__.

Ayant réussi à exhiber un variant pour notre boucle, nous avons prouvé qu’elle termine bien.

# Complexité

Le nombre de valeurs présents dans un tableau peut toujours être majoré par une puissance de 2 entière : $`2^{n}`$

Par exemple :  

* si 5 éléments sont présents dans le tableau : n = 3 car  $`5 \leq  2^{3}`$
* si l = 16 : n = 4 car $`16 \leq  2^{4}`$
* etc...

Lors d'une recherche dichotomique : à chaque itération de la boucle, on sélectionne une moitié de ce qui reste. La moitié restante aura donc au plus $`\frac{2^{n}}{2} = 2^{n - 1}`$ éléments.  
Et donc au bout de k itérations, la taille à étudier sera au plus de $`2^{n - k}`$.
Si on fait en particulier n itérations, il restera au plus $`2^{n - n} = 1`$ valeur à examiner.

> 📢 __Pour la recherche dichotomique d'une valeur dans une liste de longueur l, dans le pire des cas : le nombre d'itérations à effectuer est égal au plus petit entier n tel que $` l \leq 2^{n} - 1`$__  
> __On peut aussi dire que ce nombre d'itérations est égal au nombre de bits n nécessaires pour représenter le nombre d'éléments de la liste à une unité près__

Dans l'exemple de la figure 1 : `l = 13`, donc on peut majorer ainsi  $`13 \leq  2^{4} - 1`$.  
On en déduit donc qu'il faudra au pire 4 itérations pour retrouver la valeur recherchée : c'est compatible avec ce que l'on constate dans la figure 1 (_on peut aussi le vérifier dans tous les autres cas_)

> 📢 La fonction mathématique permettant de calculer la puissance de 2 correpondant à l'écriture d'un nombre s'appelle __logarithme base 2__ noté $`log_{2}`$  
> On dit aussi  que __la recherche dichotomique a une complexité logarithmique__

Le nombre d'itérations croît très lentement avec la taille du tableau comme on peut le constater avec le tableau suivant :

|longueur du tableau | nombre maximal d'itérations dans le cas d'une recherche dichotomique|
|:---:|:---:|
|10|4|
|100|7|
|1000|10|
|1000000|20|
|1000000000|30|

Ceci fait de la recherche dichotomique un algorithme très efficace.[^3]  
_On se rappelle par exemple que la complexité d'une recherche naïve comme la recherche séquentielle est au pire des cas égale au nombre d'éléments dans cette liste !_


# Compléments : preuve de correction 🚀

On peut montrer que le programme est correct et qu'il n'échouera jamais à trouver une valeur si elle est présente dans un **tableau trié**.
Si vous souhaitez le comprendre vous pouvez consulter cet article : https://professeurb.github.io/articles/dichoto/


[^1]: https://www.lri.fr/~mcg/PDF/FroidevauxGaudelSoria.pdf
[^2]: https://cache.media.eduscol.education.fr/file/NSI/76/3/RA_Lycee_G_NSI_algo-dichoto_1170763.pdf
[^3]: Numérique et sciences informatiques(Balabonski, Conchon, Filiâtre, Nguyen)  

