---
title: "Chapitre 18 : Recherche dichotomique dans une liste triée"
subtitle : "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : Programmation en Python &#x1F3C6;

Reprendre le code Python de la fonction `recherche_dichotomique` présentée en cours.

__a-__ Réaliser une docstring complète  
__b-__ Mettre en place des doctests pertinents  
__c-__ Ajouter également  deux assertions vérifiant que la liste fournie :  
* n'est pas vide  
* qu'elle est triée  


# Exercice 2 : Suivi de l'algorithme &#x1F3C6;&#x1F3C6;

__a-__ Créez une nouvelle fonction `recherche_dichotomique_suivi` reprenant le code de la précédente et permettant en plus à chaque tour de boucle d'afficher :
* le nombre d'itérations de la boucle déjà effectué
* la partie du tableau traitée

__b-__ Testez tout d'abord cette fonction avec l'exemple du cours  :

```python
>>> recherche_dichotomique_suivi([3, 3, 5, 6, 8, 11, 13, 14, 14, 17, 19, 21, 23], 10)
itération de la boucle :  1
partie du tableau traitée :  [3, 3, 5, 6, 8, 11, 13, 14, 14, 17, 19, 21, 23]
indice de l'élément au milieu :  6
itération de la boucle :  2
partie du tableau traitée :  [3, 3, 5, 6, 8, 11]
indice de l'élément au milieu :  2
itération de la boucle :  3
partie du tableau traitée :  [6, 8, 11]
indice de l'élément au milieu :  4
itération de la boucle :  4
partie du tableau traitée :  [11]
indice de l'élément au milieu :  5
```

__c-__ Dans chacun des cas suivants prévoir le nombre d'itérations maximal pour rechercher une valeur quelconque par dichotomie dans ces tableaux.

|cas n°|tableau|
|:---:|:---:|
|n°1|`[1, 2, 5, 20, 40, 55, 67]`|
|n°2|`[1, 2, 5, 20, 40, 55, 67, 68]`|

__d-__ Avec votre fonction testez la recherche des nombres et notez notamment le nombre d'itérations.

* 5, 40, 55, 100 dans le premier tableau
* 20, 55, 200 dans le deuxième tableau 

Concluez en comparant vos résultats à la questions précédente

__e-__ Par recherche dichotomique dans quel cas aura-ton nécessairement : 

* un nombre d'itérations maximum ?
* un nombre d'itérations minimum ?


# Exercice 3 : Comparaison de fonctions mathématiques &#x1F3C6;&#x1F3C6;

__a-__ Créer en compréhension une liste croissante de nombres entiers allant de 1 à 50 (bornes comprises) par pas de 1.  
__b-__ Créer en compréhension 2 listes permettant à partir de la liste précédente de représenter les comportements des fonctions $`f(x) = x`$ et  $`g(x) = log_{2}(x)`$  
__c-__ Tracer les deux graphiques correspondants aux fonctions précédentes à l'aide de matplotlib (vous légenderez correctement les courbes).   
__d-__ Comparez-les et faites le lien avec la recherche dichotomique

# Exercice 4 : QCM &#x1F3C6;&#x1F3C6;

__1.__ On considère la fonction Python suivante, qui prend en argument une liste L et renvoie le maximum des éléments de la liste :

```python
def rechercheMaximum(L):
    max = L[0]
    for i in range(len(L)):
        if L[i] > max:
            max = L[i]
    return max
```
On note 𝑛 la taille de la liste. Quelle est la complexité en nombre d’opérations de l’algorithme ?  

* [ ] constante, c’est-à-dire ne dépend pas de $`n`$
* [ ] linéaire, c’est-à-dire de l’ordre de $`n`$
* [ ] quadratique, c’est-à-dire de l’ordre de $`n^{2}`$
* [ ] cubique, c’est-à-dire de l’ordre de $`n^{3}`$


__2.__ En utilisant une recherche dichotomique, combien faut-il de comparaisons avec l'opérateur `==` pour trouver une valeur dans un tableau trié de 1000 nombres, dans le pire des cas ?  

* [ ] 3
* [ ] 10
* [ ] 1000
* [ ] 1024

__3.__ Un algorithme de recherche dichotomique dans une liste triée de taille 𝑛 nécessite, dans le pire des cas, exactement 𝑘 comparaisons.
Combien cet algorithme va-t-il utiliser, dans le pire des cas, de comparaisons sur une liste de taille 2𝑛 ?

* [ ] 𝑘
* [ ] 𝑘 + 1
* [ ] 2𝑘
* [ ] 2𝑘 + 1

__4.__ Pour pouvoir utiliser un algorithme de recherche par dichotomie dans une liste, quelle précondition doit être vraie ?

* [ ] la liste doit être triée
* [ ] la liste ne doit pas comporter de doublons
* [ ] la liste doit comporter uniquement des entiers positifs
* [ ] la liste doit être de longueur inférieure à 1024

# Exercice 5 : Devine le nombre &#x1F3C6;&#x1F3C6;

(_Extrait de Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_) 

Ecrire un programme qui permette à l'ordinateur de jouer à _devine le nombre_ contre l'utilisateur. C'est  l'utilisateur qui choisit un nombre entre 0 et 100 et l'ordinateur qui doit le trouver, le plus efficacement possible.  
A chaque proposition faite par l'ordinateur, l'utilisateur doit donner une réponse sous la forme d'une chaîne de caractères parmi "plus grand", "plus petit" ou "bravo".
