---
title: "Chapitre 13 : les tuples"  
subtitle: "TD "
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# Exercice 1 &#x1F3C6;

Soit le tuple (4,8,'trou', 'gagné', 7, True) écrit en Python :

1. Comment demander pour que la valeur 'gagné' soit renvoyée?
2. Comment demander pour obtenir le nouveau tuple : ('trou', True)
3. Comment demander la première valeur de ce nouveau tuple?
4. Que donnera tuple[:5] ?
5. Que donnera tuple[5:] ?
6. Que donnera tuple[1:4:2] ?

# Exercice 2 &#x1F3C6;

On souhaite définir une fonction *is_square(t)* admettant un triplet t pour argument, ce triplet contenant la longueur des trois côtés d'un triangle.
Compléter le programme suivant afin qu'il vérifie si le triangle est rectangle ou non. 

```python
def is_square(t):
    if  .... or .......  or ...... :
        print("ce triangle est rectangle")
    else : 
        print("ce triangle n'est pas rectangle")
```

# Exercice 3 &#x1F3C6;&#x1F3C6;

Créer un tuple avec par exemple une liste de prénoms.

1. Créer une fonction permettant de savoir si une personne est dans cette liste en utilisant une boucle `for`. La fonction doit répondre si la personne est présente ou absente.

2. Refaire une version de la fonction en utilisant `ìn` : c'est bien plus rapide!!!!

# Exercice 4 &#x1F3C6;&#x1F3C6;

En utilisant un tuple, réalisez une fonction nom_mois qui à un entier compris entre 1 et 12 associe le nom du mois correspondant.

# Exercice 5 &#x1F3C6;&#x1F3C6;

Réalisez une fonction nommée *insere* qui renvoie un tuple dans lequel l’élément e a été placé à l’indice i dans le tuple t avec e, i et t passés en paramètre.

Par exemple :

insere(3,0,(1,4,1,5,9)) doit renvoyer (3,1,4,1,5,9)

insere(4,2,(3,1,1,5,9)) doit renvoyer (3,1,4,1,5,9)

insere(9,5,(3,1,4,1,5)) doit renvoyer (3,1,4,1,5,9)

# Exercice 6: &#x1F3C6;&#x1F3C6;

Réalisez une fonction nommée renverse qui renvoie un tuple contenant les éléments du tuple t passé en paramètre dans l’ordre inverse.

```python
mon_tuple=('A','B','C','D')
```

```python
renverse(mon_tuple)
```

```python
    ('D', 'C', 'B', 'A')
```

# Exercice 7 : Fonctions &#x1F3C6;&#x1F3C6;  

Pour chacune des fonctions qui suit vous n'oublierez pas de réaliser une __docstring compléte__ et écrirez au moins une __assertion__. 

__1)__ Définir une fonction `somme` qui renvoie la somme des éléments d'un tuple.

_Exemple :_

```python
>>> somme((1, 5, 8, 10))
24
```

__2)__ Définir une fonction `inverse` qui inverse les éléments d'un tuple pour construire un nouveau tuple et le renvoyer.

_Exemple :_

```python
>>> inverse(('a', 2, 3))
(3, 2, 'a')
```

__AIDE__ : On pourra notamment utiliser les fonctions `list` ou `tuple` permettant repectivement de convertir une séquence quelconque en liste ou en tuple (_voir_eventuellement_ [video](https://www.youtube.com/watch?v=Kw8UIavg8Os&feature=youtu.be))

__3)__ On souhaite définir un prédicat `is_right_angle` admettant un triplet `t` pour argument, ce triplet contenant la longueur des trois côtés d'un triangle (exprimés dans la même unité).  
Le prédicat devra renvoyé `True` si le triangle est rectangle, `False` dans le cas contraire

_Exemple :_

```python
>>> is_right_angle((6, 8, 10))
True
```

__4)__ 🥇 Définir une fonction `infos_notes` qui accepte une liste de notes en paramètre et qui renvoie un triplet contenant dans l'ordre la moyenne arrondi au dixième, la note la plus basse ainsi que la note la plus haute.

_Exemple :_

```python
>>> infos_notes([15, 12, 10, 8, 17, 11])
(12.2, 8, 17)
```

# Exercice 8 : Coordonnées de points &#x1F3C6;&#x1F3C6;

Pour chacune des fonctions qui suit vous n'oublierez pas de réaliser une __docstring compléte__ et mettrez en place des __doctests__ adaptés.  

__1)__ On considère deux points A et B d'un repère quelconque. Leurs coordonnées sont des tuples à deux éléments.[^1]  
Écrire une fonction `milieu` qui prend en argument les coordonnées de ces deux points et qui renvoie les coordonnées du milieu du segment formé par ces deux points.

__Par exemple :__

```python
>>> A = (3, 12)
>>> B = (-4, 5)
>>> milieu (A, B)
(-0.5, 8.5)
```

__2)__ Etant donné une liste de points dans un plan par exemple, on souhaite trouver le point le point le "plus haut et le plus à droite possible".[^2]  
Définir une fonction `point_sup_droit` acceptant comme paramètre une liste de tuples correspondant à des coordonnées de points dans un plan.  
Cette fonction renverra le tuple correspondant aux coordonnées du point recherché.

```python
>>> point_sup_droit([(0, 1),(1, 2),(1, 1),(1.2, 3)]) 
(1.2, 3)
```

_Conseil :_
On peut comparer deux tuples : le premier élément du premier tuple sera comparé au premier du second et ainsi de suite .....

```python
>>> (0,1) < (1,2)
True

>>> (1,1) < (1,2)
True
```

# Exercice 9 : Synthèse additive RGB &#x1F3C6;&#x1F3C6;&#x1F3C6;  

Définir une fonction `rgb_to_hex` qui renvoie la valeur hexadécimale d'une couleur lorsqu'on lui passe en paramètre un triplet de valeur décimale.

_Exemple :_

```python
>>> rgb_to_hex((255, 0, 255))
'#ff00ff'
```

__Rappel__ : dans le système RGB (ou RVB en français) chaque composante est codé sur un octet : le premier octet correspond à la composante rouge, le second au vert, le dernier au bleu.
Pour un octet les valeurs équivalentes :
* en décimal vont de `0` à `255`
* en hexadécimal vont de `00` à `ff`

__Attention__ : la valeur de retour doit être une chaîne toujours composée de __6 caractères hexadécimaux__ et précédée de __#__

--> Vous n'oublierez pas de réaliser une __docstring complète__ , écrirez quelques __assertions__ et mettrez en place des __doctests__ adpatés.

---
[^1]: sur une idée de http://isn.cassin.free.fr
[^2]: sur une idée de http://www.mathly.fr/
