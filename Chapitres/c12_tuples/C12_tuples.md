---
title: "Chapitre 13 : les tuples"  
subtitle: "cours"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# Les tuples
## Définition
__📢 Tuple :__ 
>Un tuple peut être vu comme une liste immuable, c'est-à-dire qu'il n'est pas possible de l'étendre, de la réduire, ni de modifier ses valeurs. </span>  

Les tuples sont des données qui, comme les chaînes de caractères, font partie d’une famille appelées **séquences**.  

Le tuple est un **itérable** : c'est une donnée que l'on peut parcourir avec une boucle `for`. A ce titre le tuple accepte les tests :  x **in** Tuple ou x **not in** Tuple.

Le tuple, appelé encore p-uplet peut comporter des termes de type différent ; voici par exemple avec trois termes un triplet :(True, 4, 'varicelle')  

Un tuple se déclare avec un ensemble de valeurs passées entre **parenthèses** : 


```python
>>>mon_tuple = (3,2,3,4,2,7)
>>>print(mon_tuple)
(3, 2, 3, 4, 2, 7)
```    

```python
>>>x= (1,2,3,4)
>>>type(x)
<class 'tuple'>
```

```python
>>>y = 1,2,3,4
>>>y
(1, 2, 3, 4)
```

```python
>>>z = ("a", 0 )
>>>z
('a', 0)
```

## Créer des tuples
### Tuple à un seul élément
Pour fabriquer un tuple à un élément unique, on écrira :  

```python
>>>x = (1,)

>>>y = 1,
>>>type(y)
<class 'tuple'>
```

Remarque : On voit que le délimiteur du tuple est la `,` car x = 1 affecterait la valeur 1 à x

### Fonction `tuple`
on peut utiliser la fonction `tuple` par exemple sur une chaîne:  

```python
>>>mon_tuple=tuple("Marianne")
>>>print (mon_tuple)
('M', 'a', 'r', 'i', 'a', 'n', 'n', 'e')
```

### Tuple en compréhension

On dit qu’on construit un tuple en compréhension lorsqu’on le définit en appliquant **une fonction à une séquence d’éléments.**

Voici à titre d’exemple plus complexe, le tuple des carrés des 10 premiers entiers positifs ou nuls. Il est construit à l’aide de l’expression i*i dans laquelle i prend les valeurs successives 0, 1, ..., 9 (grâce à la boucle for).

```python
>>>mon_tuple=tuple(i*i for i in range(10))
>>>print(mon_tuple)
(0, 1, 4, 9, 16, 25, 36, 49, 64, 81)
```

# Accéder à un élément du tuple
On accède à un élément du tuple (comme pour une liste), l'indice de **départ étant  0 :**

```python
>>>mon_tuple = (3,2,3,4,2,7)
>>>print("élément en position 0 : " , mon_tuple[0])
élément en position 0 :  3
```
Par contre il est bien impossible de modifier une valeur d'un tuple : 


```python
mon_tuple[0]=11
 ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-15-7f2d5e86c1cb> in <module>
    ----> 1 mon_tuple[0]=11
    

    TypeError: 'tuple' object does not support item assignment
```

Voici ci-dessous un tableau résumant l'indexation pour les tuples mais aussi pour les listes, les chaînes de caractères etc..   
[source](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf) (Mémento Python 3 Laurent Pointal Mémento v2.0.7)

![mémento](./memento_python/indexation.png)

# Recherche d'éléments  et fonctions diverses
Comme pour une liste, il est possible 

* de connaître le nombre d'éléments d'un tuple : **fonction** `len`

```python
>>>len(mon_tuple)
6
```

* de connaître le nombre d'occurences d'une valeur : **méthode**`count()`

```python
>>>nb_trois = mon_tuple.count(3)
>>>print('nb de 3 dans le tuple : ', nb_trois)
nb de 3 dans le tuple :  2
```

* de savoir où se trouve la première apparition d'un élément du tuple : **méthode**  `index()`

```python
>>>pos_deux = mon_tuple.index(2)
>>>print('2 apparait une première fois en position ', pos_deux)
2 apparait une première fois en position  1
```

```python
>>>pos_deux_2 = mon_tuple.index(3,1)
>>>print("3 apparaît, lorsqu'on regarde à partir de la position 1, en position ", pos_deux_2)
3 apparaît, lorsqu'on regarde à partir de la position 1, en position  2
```

# Ajouter des éléments
Comme signalé, un tuple est immuable, il n'est pas possible de modifier ses éléments ni d'en ajouter.
Par contre, il est possible de créer un nouveau tuple à partir d'un tuple existant, simplement en lui ajoutant un tuple :


```python
>>>tuple1 = (1,2)
>>>tuple2 = (3,4)
>>>tuple_nouveau = tuple1 + tuple2
>>>print(tuple_nouveau)
(1, 2, 3, 4)
```

Pour ajouter 1 seul élément à un tuple, la syntaxe est la suivante : ( on retrouve la `, ` !!!)

```python
tuple1 = (1,2)
tuple1 = tuple1 + (3,)
(1, 2, 3)
```

# Exemple d'application des tuples
Soit un élève qui a obtenu des notes en Sciences physiques : celles-ci ne peuvent être changées! L'usage des tuples est donc approprié.
Nassim a ainsi eu pour le premier trimestre les notes suivantes : 16,0 - 16,0 - 10,5 - 13,0.  
A l'aide de trois fonctions appelant le tuple créé, trouver la note maximale, minimale, et la moyenne du trimestre.

<span style="color:green" > Parcours  séquentielle du tuple  : **for var in tuple :**
</span> 

```python
Nassim_notes= (16,14,10.5,13)
def note_max(tup):
    note_mx = tup[0] # on définit une variable dans laquelle on stocke au départ la première note du tuple 
    for var in tup : # on parcourt le tuple
        if var> note_mx:
            note_mx= var 
    return note_mx

def note_min(tup):
    note_mn = tup[0]
    for var in tup : 
        if var< note_mn:
            note_mn= var 
    return note_mn
def moyenne(tup):
    somme = 0
    for var in tup :
        somme = somme + var
    moy = (somme/len(tup))
    return moy
```

```python
>>>note_max(Nassim_notes)
16
```

```python
>>>note_min(Nassim_notes)
10.5
```

```python
moyenne(Nassim_notes)
13.375
```


SOURCES :

* Le petit Python - Aide mémoire pour Python 3- Gérard Gomez  
* Cours Anne Parrain- Faculté de Lens  
* http://www.fil.univ-lille1.fr/~L1S1Info/Doc/New/cours6_fr.html  

