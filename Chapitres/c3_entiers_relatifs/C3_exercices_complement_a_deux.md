---
title: "Chapitre 3 Entiers relatifs  exercices "
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---  
# Exercice n°1 #

1) Convertir 54<sub>(10)</sub> et 38<sub>(10)</sub> en base 2.  
2) Réaliser l'addition de ces deux nombres binaires.  
3) Vérifier que la valeur correspond bien à 92

# Exercice n°2 #

Que se passe-t-il si on ajoute 1 à 1 1 1 1 1 1 1 1 alors que la représentation se fait sur 8 bits?

# Exercice n°3 #

Donner les complément à deux des nombres suivants: ( Nous représenterons ces nombres sur 8 bits.)  

 6 ; 44 ; -15 ; -74

# Exercice n°4 #

En utilisant le complément à deux des nombres de l'exercice n°3, vérifiez que l'addition de 44 et -15 vaut bien 29 (représentation sur 8 bits)