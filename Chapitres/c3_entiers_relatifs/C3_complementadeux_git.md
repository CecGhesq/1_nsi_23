---
title: "Chapitre 3 Représentations des entiers relatifs"
subtitle: 
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---  
__Introduction__
Dans notre cours, l'ensemble de nos exemples se feront avec des nombres codés sur 8 bits ou moins. Cela rendra le cours plus simple à comprendre. Cependant, tous les résultats obtenus pourront être généralisés à des mots de 16, 32 ou 64 bits. Pour rappel, les entiers relatifs en mathématique sont définis comme des entiers pouvant être positifs ou négatifs.  

# L'addition de deux nombres binaires
 Dans le cas particulier de la base 2, seuls les chiffres 0 et 1 sont donc utilisés, ce qui rend les opérations mathématiques de base particulièrement simples à réaliser. En effet, les règles de calcul apprises à l’école primaire en base 10(addition, soustraction, multiplication, division) se généralisent en base 2.

 Règles de base en binaire:
           

- 0  +  0  =  0

- 1  +  0  =  1

- 0  +  1  =  1

- 1  +  1  =  1 0  -->  Dans cette addition, le 1 de ce nombre 1 0 sera considéré comme une **retenue** ou **report**.

Exemple:  
Dans notre exemple, on souhaite additionner les nombres binaires 1 1 0 1 et 1 0 1   

![Addition binaire](images/Addition_binaire.png)

# Première approche de la représentation des entiers relatifs : le binaire signé #


- Les entiers relatifs sont appelés en informatique des **nombres signés** car pour la représentation de ces nombres, il faut réserver un bit pour stocker le signe.

- La première idée consiste à réserver le bit de poids fort c'est à dire celui le plus à gauche pour stocker le signe puis les autres bits pour stocker la valeur du nombre. Le zéro sera réservé au signe positif et le 1 au signe négatif.  


![Entiers relatifs](images/imageentierrelatif_rs.jpg)
 

Nous voyons que même si cette idée est simple à comprendre, elle présente deux inconvénients.  

- Le zéro est codé de deux façons différentes: **0000** et **1000**, ce qui peut poser problème.

- Si on additionne deux nombres de signe opposé en utilisant les règles définies au II par exemple 2 et -2, on ne trouve pas zéro mais -4. Cela nécessite donc de changer les règles de calcul de base.  
 
**Nous ne pouvons donc pas garder cette première approche de représentation des entiers relatifs.**  

# Le complément à deux 
Que gardons nous de la première approche?  

- Nous garderons le bit de poids fort pour le signe avec la convention 0 pour le positif et 1 pour le négatif.

- Pour les entiers relatifs positifs, nous garderons la représentation des binaires signés.

- Nous devrons trouver une représentation des entiers relatifs négatifs pour garder les règles de base du calcul vues en primaire. Nous utiliserons **le complément à deux**.

- Il n'y aura qu'une manière de représenter le zéro.

Comme nous l'avons dit en introduction, nous allons réaliser le complément à deux sur des mots de **8 bits**. Ce que nous verrons pourra être généralisé sur 16, 32 ou 64 bits.

Nous pourrons donc sur 8 bits avoir la représentation des nombres allant de "1 1 1 1 1 1 1 1" c'est à dire -127 à "0 1 1 1 1 1 1 1" c'est à dire 127

## Le dépassement de capacité

Pour avoir un dépassement de capacité, il faut tout d'abord définir sur combien de bits se fera la représentation des nombres relatifs. Nous fixerons pour une question de simplicité une représentation sur 8 bits.  

Lorsqu'on additionne deux nombres et qu'il y a une retenue qui se repporte sur le neuvième bit alors, il y a dépassement de capacité. Dans ce cas, la retenue est perdue et on ne prend en compte que les valeurs des 8 bits.

Ce dépassement de capacité va nous permettre de réaliser le complément à deux.

Exemple:  
 
![Dépassement de capacité](images/depassement.png)

Dans le résultat, le 1 qui se trouve tout à gauche est en dépassement de capacité puisque la représentation se fait sur 8 bits.

## Comment réaliser le complément à deux d'un entier relatif négatif?

- Dans un premier temps, on cherche la représentation binaire positive de l'entier relatif négatif.  
 Par exemple, pour -8, on prendra la représentation 0 0 0 0 1 0 0 0 soit 8.

- Ensuite on fait le complément à 1 c'est à dire qu'on cherche le nombre binaire qui additionné à "0 0 0 0 1 0 0 0" donnera "1 1 1 1 1 1 1 1". Dans notre exemple, le nombre qui est complément à 1 de 8 est 1 1 1 1 0 1 1 1.

**Plus simplement, le complément à 1 d'un nombre binaire consiste à inverser les valeurs de chaque bit.**  

- Enfin, on réalise le complément à deux. Pour cela, on prend le complément à 1 qu'on vient de trouver et on ajoute 1 à cette valeur.  
Dans notre exemple, on a :  

![ajout de 1](images/complementadeux.png)  

La représentation de -8 en complément à deux est 1 1 1 1 1 0 0 0.  

On peut vérifier que si on additionne 8 et -8 on retrouve 0.
  

![8 +(-8)](images/verif.png)  

On rappelle que le 1 tout à gauche est en dépassement de capacité puisqu'il est sur le neuvième bit. Donc le résultat est 0 0 0 0 0 0 0 0.

## Conclusion 
__📢 Définition :__  
> __Sur __n bits__, la représentation en complément à deux permet de stocker tous les entiers compris entre $`\bf-2^{n-1}`$ et  $`\bf 2^{n-1} -1`$.__    
> __Le bit de poids fort représente toujours le signe de l'entier__  

__📝Application__ :  
Nous pourrons donc sur 8 bits avoir la représentation des nombres allant de  __-128__  à  __127__  c'est à dire  de $`(\textbf{1}0000000)_{2}`$ à $`(\textbf{0}1111111)_{2}`$.   


![Complement à deux sur 8 bits](images/complement_2_8bits.jpg)  

_Remarques pour un codage sur 8 bits_ :  

- Le nombre 0 n'a qu'une seule représentation avec cette convention : $`\bf(00000000)_{2}`$   
  
- Les entiers positifs sont représentés dans l'intervalle $`[0;127]`$   
  
- Les entiers négatifs le sont dans l'intervalle $`[128;255]`$ :  
   
    * En effet, $`\bf-128`$ est représenté comme $`2^{8}-128 \text{ soit } 128`$   
     
    * Le plus grand entier négatif : $`\bf-1`$ est représenté comme $`2^{8}-1 \text{ soit } 255`$   
