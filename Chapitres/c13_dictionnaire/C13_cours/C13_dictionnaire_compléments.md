---
title: "Chapitre 13  : les dictionnaires en python "
subtitle: "compléments"
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---


# Dans quels cas utilise-t-on un dictionnaire ?

* Lorsqu'on veut manipuler des données qui ont toutes une même structure, avec des caractéristiques de nature différentes. On est capable pour ces données de fixer à l'avance quelles seront les clés;
  
* Pour manipuler des données qui associent à un élément d'un certain type A une valeur d'un certain type B. Par exemple, on veut conserver les notes d'un contrôle : on associe une note à un élève ;


```python
interro = {"Kévin": 12, "Anissa": 15, "Mohamed": 12, "Sophie": 8}
```

* Pour agréger dans une seule variable de nombreux paramètres qu'on a souvent besoin de transmettre ensemble à des fonctions.

# Opérations sur un dictionnaire

## Parcourir un dictionnaire

On peut parcourir toutes les clés d'un dictionnaire, ou tous les couples *(clé, valeur)*, ou toutes les valeurs d'un dictionnaire. C'est particulièrement utile quand on manipule un dictionnaire dont on ne connaît pas à l'avance les clés.

La fonction `keys()`retourne un itérateur ( Objet qui permet de parcourir tous les éléments contenus dans une collection) sur les clés, la fonction `values()` retourne un itérateur sur les valeurs, la fonction `items()` retourne un itérateur sur les couples *(clé, valeur)*.

## Tester si une clé existe déjà

C'est l'opérateur `in` sur l'itérateur `keys()` qui nous donne l'information ; attention on n'accède pas à la valeur mais bien à la clé.

```python
'Anissa' in interro.keys()
```

```python
    True
```

```python
'Benjamin' in interro.keys()
```

```python
    False
```

## Récupérer une valeur
La méthode `get()`permet de récupérer une valeur :

```python
interro = {"Kévin": 12, "Anissa": 15, "Mohamed": 12, "Sophie": 8}

```

```python
interro.get('Anissa')
```

```python
    15
```

Si la clé n'est pas trouvée, rien ne s'affiche.

# Copier, comparer ou fusionner des dictionnaires

## Copier
Les dictionnaires étant une structure de données modifiable (muable, mutable), c'est la méthode `copy()` qui permet de faire une copie d'un dictionnaire.


```python
interro2 = interro.copy()
>>> interro
{'Kévin': 12, 'Anissa': 15, 'Mohammed': 12, 'Sophie': 8}

>>> interro2
{'Kévin': 12, 'Anissa': 15, 'Mohammed': 12, 'Sophie': 8}
```

```python
interro2['Sophie'] = 18
```

```python
>>> interro2
{'Kévin': 12, 'Anissa': 15, 'Mohammed': 12, 'Sophie': 18}
```

```python
>>> interro
{'Kévin': 12, 'Anissa': 15, 'Mohamed': 12, 'Sophie': 8}
```

Attention, ce n'est pas une **copie profonde** ! les deux dictionnaires peuvent être modifiés si les valeurs sont des listes,  si on modifie la copie. On le voit ci-dessous avec deux exemples.

* premier exemple


```python
dico={'one':[1,1],'two':[1,2],'three':[1,3]}
dico2 = dico.copy()
dico2['one'][1]=0
```

```python
>>> dico2
{'one': [1, 0], 'two': [1, 2], 'three': [1, 3]}
```

```python
>>> dico
{'one': [1, 0], 'two': [1, 2], 'three': [1, 3]}
```

*  deuxième exemple


```python
>>> anissa
{'nom': 'Kazcmarek',
     'prenom': 'Anissa',
     'classe': '1F',
     'specs': ['nsi', 'mathématiques', 'SVT'],
     'moyennes': [15, 16.8, 15.3]}
```


```python
anissa2 = anissa.copy()
```

```python
anissa2["moyennes"][0] += 2 # on ajoute deux à sa moyenne de nsi (indice 0 de la liste)
```

```python
>>> anissa2
{'nom': 'Kazcmarek',
     'prenom': 'Anissa',
     'classe': '1F',
     'specs': ['nsi', 'mathématiques', 'SVT'],
     'moyennes': [17, 16.8, 15.3]}
```

```python
>>> anissa
{'nom': 'Kazcmarek',
     'prenom': 'Anissa',
     'classe': '1F',
     'specs': ['nsi', 'mathématiques', 'SVT'],
     'moyennes': [17, 16.8, 15.3]}
```

## Fusionner deux dictionnaires

La méthode `update()` permet de fusionner, c'est à dire d'ajouter tous les couples *(clé,valeur)* d'un dictionnaire à un autre.


```python
interro3 = {'Robert': 5, 'Marcelle': 15} # on crée le dictionnaire à fusionner
interro.update(interro3)
```

```python
>>> interro
{'Kévin': 12, 'Anissa': 15, 'Mohamed': 12, 'Sophie': 8, 'Robert': 5,   'Marcelle': 15}
```

```python
>>> interro3
{'Robert': 5, 'Marcelle': 15}
```

## Comparer des dictionnaires

* on peut trouver les clés communes  avec l'opérateur booléen `& `:

```python
# on compare des compositions de smoothies d'un bar à jus :
smoothieEnergie = {'bananes':'oui','oranges':'oui', 'kiwis':'oui', 'gingembre':'oui' }
smoothieDouceur = {'bananes':'oui','oranges':'oui', 'abricots':'oui', 'framboises':'oui' }
```

```python
>>> smoothieEnergie.keys() & smoothieDouceur.keys()
{'bananes', 'oranges'}
```

* on peut de même trouver les couples clé-valeur communes  avec l'opérateur booléen `& `:

```python
>>> smoothieEnergie.items() & smoothieDouceur.items()
{('bananes', 'oui'), ('oranges', 'oui')}
```

* enfin on peut savoir quelles sont les clés présentes dans un dictionnaire et pas dans l'autre

```python
>>> smoothieEnergie.keys() - smoothieDouceur.keys()
{'gingembre', 'kiwis'}
```

**CE QUE JE RETIENS** 

![mémento](./img/ope_dico.png)

Sources : 

* cours Anne Parrain faculté de Lens
* NSI ellipses ISBN 9782340-031722
* memento Python 3 : [memento](https://perso.limsi.fr/pointal/python:memento)

    
