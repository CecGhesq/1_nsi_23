---
title: "Chapitre 13 : les dictionnaires"  
subtitle: "TP : Où se repose spiderman entre deux exploits ?"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

Lorsqu'on prend une photo avec un appareil photo ou un smartphone, un fichier de métadonnées dans le format **EXIF** (Exchangeable Image File Format) est incorporé dans la photo elle-même.
 Les balises de métadonnées définies dans le format EXIF donnent la date et l'heure de la prise de vue, les réglages de l'appareil( (marque, modèle, ouverure, vitesse d'obturation,...), des informations géographiques provenant du GPS de l'appareil,  les droits d'auteur.
 
 A l'aide d'un programme Python, vous allez extraire ces données sous forme de dictionnaires puis les exploiter pour découvrir où s'était caché Spiderman :  
![spider-man](./img/spider_rz.jpg)

# Extraction de données EXIF

Lorsqu'on clique sur les propriétés d'une photo, on accède aux données EXIF. De même les logiciels de retouche photo permettent d'obtenir ces mêmes informations. 
En python, la bibliothèque PIL contient les modules Image et ExifTags qui permettent d'accéder rapidement aux tags et nous récupérerons les données sous forme de dictionnaires.

## Installation des modules

IIl faut tout d'abord installer dans Thonny les plugins PILLOW et FOLIUM  : 
 
 * dans Outils, sélectionner "gérer les plugins" 

* dans la fenêtre qui s'ouvre vérifier la présence de pillow ; sinon, rechercher le paquet dans PyPI et l'installer. Fermer et réouvrir Thonny.

* Recommencer pour FOLIUM
  
|  ![plugin](./img/plug.png) | ![pillow](./img/pillow1.png)  |
|:-:|---|

## Dictionnaires des données

Voici le code pour extraire les données EXIF de la photo de lieu inconnu.

```python
from PIL import Image, ExifTags

def get_exif(image):
    img = Image.open(image)  # ouverture du fichier image
    info = img._getexif() # récupération des données
    return info
```

1. Le tester dans la console de Thonny avec `get_exif("spider.jpg")` en enregistrant votre fichier python dans le même dossier le fichier image ["spider.jpg"](./Chapitres/c14_dictionnaire/spider.JPG)

* Que remarque-t-on? ................

2. Afin de classer les données dans un dictionnaire, ajouter au sein de la fonction précédente le code suivant :

```python
ret = {}
for tag, value in info.items(): # on parcourt les couples clé valeur des données EXIF
        decoded = ExifTags.TAGS.get(tag, tag)
        if tag in ExifTags.TAGS:
            ret[decoded] = value
return ret
```

* Que remarque-t-on?
  ...........................................

* Retrouver dans les données la date de la prise de vue ainsi que l'heure :
  ...........................................
  
* Quel type d'appareil a pris la photo? 
  ...........................................

* Quelle commande permet d'extraire et d'afficher la marque et le modèle de l'appareil?

# Les données GPS

Afin d'isoler les données GPS, on utilise le code suivant

```python
#affichage des données gps
def gpsInfo(exifData):
    gpsinfo = {}
    for key in exifData['GPSInfo'].keys(): # ligne A
        decode = ExifTags.GPSTAGS.get(key,key) # ligne B
        gpsinfo[decode] = exifData['GPSInfo'][key]
    return gpsinfo
```

1. Expliquons le code :

* Expliquer le code de la ligne A : 
 .........................................

`ExifTags.GPSTAGS`  relie les identifiants numériques des données EXIF à des noms.

* Rappeler le rôle de la méthode get() de la ligne B : 
...........................................

2. Isolons donc les données GPS de la photo. Rajouter ces ligne en bas de  votre code :

```python
print("")
print("les données GPS de la photo sont : ")
print(dict_gps)
```

```python

les données GPS de la photo sont : 
{'GPSLatitudeRef': 'N', 'GPSLatitude': (40,  24,  5.645), 'GPSLongitudeRef': 'W', 'GPSLongitude': (3, 42, 2.944, 100, 'GPSAltitudeRef': b'\x00', 'GPSAltitude': (74039, 114), 'GPSTimeStamp': ((13, 1), (17, 1), (2400, 100)), 'GPSSpeedRef': 'K', 'GPSSpeed': (0, 1), 'GPSImgDirectionRef': 'M', 'GPSImgDirection': (46831, 342), 'GPSDestBearingRef': 'M', 'GPSDestBearing': (46831, 342), 'GPSDateStamp': '2017:10:25', 'GPSHPositioningError': (10, 1)}
```

* Combien y a-t-il  de clés dans ce dictionnaire ?
............................................

On souhaite récupérer les coordonnées et les transformer sous forme décimale.
Les coordonnées géographiques sont habituellement exprimées dans le système sexagésimal, ou DMS pour degrés (°), minutes (') et secondes ("). L’unité est le degré d’angle (1 tour = 360°), puis la minute d’angle (1° = 60'), puis la seconde d’angle (1° = 3 600").

Par rapport au plan équatorial, la latitude est complétée d’une lettre N (hémisphère) ou S selon qu’on se situe dans l’hémisphère Nord ou Sud. Par rapport au méridien de Greenwich, la longitude est complétée d’une lettre W ou E selon qu’on se situe à l’Ouest ou à l’Est.

* Sous quelle forme de structure de données en Python sont récupérées la latitude et longitude ?
.............................................

1. Afin de localiser Spider Man il nous faut récupérer les coordonnées des lattitude et longitude sous forme décimale.
Pour comprendre étudions le document suivant : 
Sur le site [coordonnées-gps.fr](https://www.coordonnees-gps.fr/), on repère la position de la Tour Eiffel :

![Tour Eiffel](./img/tourEiffel.png)

Le tuple de la latitude serait :
(48, 51, 28.921)
Or pour repérer la position de Spider Man il nous faut la latitude en **degrés décimaux**.
Le calcul de latitude serait : 
$latitude = 48 + (\frac {51} {60}  ) + (\frac {28.921} {3600}  )=48,85803361$
On retrouve ainsi la valeur calculée par le site web.

* Créer la fonction conv_DD(tuple) qui va convertir le tuple des coordonnées GPS en degré décimal


* Dans Thonny, créer en bas de votre code les constantes Latitude et Longitude , stocker les tuples et afficher les valeurs des coordonnées converties.

```python
Latitude = dict_gps["GPSLatitude"]
Latitude_DD = conv_DD(Latitude)
print ("La latitude du lieu inconnu est : ", Latitude_DD)
Longitude = dict_gps["GPSLongitude"]
Longitude_DD = conv_DD(Longitude)
print ("La longitude du lieu inconnu est : ", Longitude_DD)
```

# Alors.... où se cache Spider Man? 

**Folium** est un module utilisant **Leaflet** qui est la principale librairie JavaScript open source.

* Exécuter ce code à la suite du précédent :
  
```python
import folium
import webbrowser

carte= folium.Map( #création de la carte centrée sur la position choisie avec un zoom choisi
    location=[Latitude_DD, Longitude_DD],
    zoom_start=16
)

folium.Marker( # création du marqueur de position
    location=[Latitude_DD, Longitude_DD],
    popup='Où est-on?',
    icon=folium.Icon(color='red')
).add_to(carte)


folium.CircleMarker(
    location=[Latitude_DD, Longitude_DD],
    radius=100,
    popup='Le quartier',
    color='#ff0000', # couleur rouge en hexadécimal
    fill=True,
    fill_color='#ccccccc'
).add_to(carte)


carte.save('lapositiondeSpider_Man.html')# sauvegarde de la position dans une page html
webbrowser.open('lapositiondeSpider_Man.html',new=2)
```

EUHHHHH touché coulé? on est dans la Méditerranée!!!

Car nous avons oublié de gérer si notre latitude était N ou S( Nord ou Sud) et si notre longitude est W ou E ( Ouest ou Est).
Si nous sommes dans l'hémisphère Sud, l'angle doit être négatif dans la convention des coordonnées GPS.

De même si la longitude est Ouest.

![Latitude Longitude](./img/latitude.gif)

* Modifier la fonction conv_DD(tuple) pour prendre en compte cette notion
  
C'est mieux non ?
**Alors où se cache Spider Man?**

............................................

Sources :

* [site NSI du lycée Clémenceau](https://www.numerique-sciences-informatiques.fr/dictionnaires.php)
  
* [site de Sylvain Durand](https://www.sylvaindurand.fr/gps-data-from-photos-with-python/) 
