---
title: "Chapitre 13  : les dictionnaires en python "
subtitle: "Exercices"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : le scrabble
 Le scrabble est un jeu de société où l'on doit former des mots avec un tirage aléatoire de lettres, chaque lettre valant un certain nombre de points.  
 Le tableau ci-dessous donne les points de certaines lettres :

 | Lettre | A | B | C | D | E | F | G | H | I | J | K |
|--------|---|---|---|---|---|---|---|---|---|---|---|
| Points | 1 | 3 | 3 | 2 | 1 | 4 | 2 | 4 | 1 | 8 | 5 |
 On considère le programme suivant :

```python
scrabble={"A":1,"B":3, "C":3,"D":2, "E":1, "F":4, "G":2,"H":4,"I":1, "J":8, "K":5}
somme=0
mot=input("Entrez un mot : ")

for i in mot :
    somme= somme + scrabble.get(i)
print (somme)
```

1. Quel est le type de la variable scrabble
    
2. Expliquer l'affectation de cette variable
   
3. Qu'affiche le programme si on entre le mot "KAKI"

4. Quel est le rôle de cette boucle?

# Exercice 2 :Tableau physico-chimique  

Le tableau suivant représente les données physico-chimiques de deux éléments chimiques. On donne la température d'ébullition Te, la température de fusion Tf , le numero atomique Z et le nombre de masse A.

| Elément | Te   | Tf   | Z  | A       |
|---------|------|------|----|---------|
| Au      | 2970 | 1063 | 79 | 196.967 |
| Ga      | 2237 | 29.7 | 31 | 69.72   |
 
1. Affectez des données de ce tableau à un dictionnaire Python de façon à pouvoir écrire par exemple :  
print (dico["Au"]["Z"])                           (affiche 79)

2. Afficher les clés de "dico"

3. Afficher toutes les clés de "Au"

4. Vérifier que les clés soient communes entre les deux éléments chimiques

# Exercice 3 : Moyennes

Soit le dictionnaire suivant listant pour chaque élève d'une classe, les notes d'une interrogation :
`interro = {"Kévin": 12, "Anissa": 15, "Mohmamed": 12, "Sophie": 18, "Grégory" : 14, "Yann" : 11, "Bryan": 7, "Lina" : 19, "Julien" : 17, "Tom" : 15} `

1. Calculer la moyenne des notes de l'interro en itérant sur les clés.
2. Calculer la moyenne des notes de l'interro en itérant sur les valeurs.
3. Quel est le nom de l'élève qui a la meilleure note (cas simple : on suppose qu'il n'y a qu'un premier de la classe) et quelle est sa note

# Exercice 4 : Likes
Sur le réseau social TipTop, on s’intéresse au nombre de « like » des abonnés. Les données sont stockées dans des dictionnaires où les clés sont les pseudos et les valeurs correspondantes sont les nombres de « like » comme ci-dessous :  
{'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50}
Écrire une fonction `max_dico` qui :  
-  prend en paramètre un dictionnaire dico non vide dont les clés sont des chaînes de
caractères et les valeurs associées sont des entiers positifs ou nuls ;
-  renvoie un tuple dont :
    - la première valeur est une clé du dictionnaire associée à la valeur maximale ;
    - la seconde valeur est la valeur maximale présente dans le dictionnaire.

Exemples :
```python
>>> max_dico({'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50})
('Ada', 201)
>>> max_dico({'Alan': 222, 'Ada': 201, 'Eve': 220, 'Tim': 50})
('Alan', 222)
```
# Exercice 5 :  Classement
Écrire une fonction `enumere` qui prend en paramètre une liste L et renvoie un dictionnaire d dont les clés sont les éléments de L avec pour valeur associée la liste des indices de l’élément dans la liste L.
Exemple :
```python
>>> enumere([1, 1, 2, 3, 2, 1])
{1: [0, 1, 5], 2: [2, 4], 3: [3]}
```

# Exercice 6 : plus petit,  plus grand ....
Écrire une fonction `min_et_max` qui prend en paramètre un tableau de nombres tab non vide, et qui renvoie la plus petite et la plus grande valeur du tableau sous la forme d’un dictionnaire à deux clés 'min' et 'max'.
Les tableaux seront représentés sous forme de liste Python.
L’utilisation des fonctions natives min, max et sorted, ainsi que la méthode sort n’est pas autorisée.
Ne pas oublier d’ajouter au corps de la fonction une documentation et une ou plusieurs assertions pour vérifier les pré-conditions.
Exemples : 
```python
>>> min_et_max([0, 1, 4, 2, -2, 9, 3, 1, 7, 1])
{'min': -2, 'max': 9} 
>>> min_et_max([0, 1, 2, 3]) 
{'min': 0, 'max': 3} 
>>> min_et_max([3])
{'min': 3, 'max': 3} 
>>> min_et_max([1, 3, 2, 1, 3])
{'min': 1, 'max': 3} 
>>> min_et_max([-1, -1, -1, -1, -1])
{'min': -1, 'max': -1}
```


# Exercice 7 : Refuge
On considère des tables (des tableaux de dictionnaires) qui contiennent des enregistrements relatifs à des animaux hébergés dans un refuge. Les attributs des enregistrements sont 'nom', 'espece', 'age', 'enclos'. Voici un exemple d'une telle table :
```python
animaux = [ {'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2},
{'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
{'nom':'Tom', 'espece':'chat', 'age':7, 'enclos':4},
{'nom':'Belle', 'espece':'chien', 'age':6, 'enclos':3},
{'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
```
Programmer une fonction selection_enclos qui :
- prend en paramètres :
    - une table table_animaux contenant des enregistrements relatifs à des animaux (comme dans l'exemple ci-dessus),
    - un numéro d'enclos num_enclos ;
- renvoie une table contenant les enregistrements de table_animaux dont l'attribut 'enclos' est num_enclos.

Exemples avec la table animaux ci-dessus :
```python
>>> selection_enclos(animaux, 5)
[{'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
{'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
>>> selection_enclos(animaux, 2)
[{'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2}]
>>> selection_enclos(animaux, 7)
[]
```
# Exercice 8 : fusion de dictionnaires
Écrire une fonction `ajoute_dictionnaires` qui prend en paramètres deux dictionnaires d1 et d2 dont les clés sont des nombres et renvoie le dictionnaire d défini de la façon suivante :
- Les clés de d sont celles de d1 et celles de d2 réunies.
- Si une clé est présente dans les deux dictionnaires d1 et d2, sa valeur associée dans le dictionnaire d est la somme de ses valeurs dans les dictionnaires d1 et d2.
- Si une clé n’est présente que dans un des deux dictionnaires, sa valeur associée dans le dictionnaire d est la même que sa valeur dans le dictionnaire où elle est présente.

Exemples :
```python
>>> ajoute_dictionnaires({1: 5, 2: 7}, {2: 9, 3: 11})
{1: 5, 2: 16, 3: 11}
>>> ajoute_dictionnaires({}, {2: 9, 3: 11})
{2: 9, 3: 11}
>>> ajoute_dictionnaires({1: 5, 2: 7}, {})
{1: 5, 2: 7}
```


Sources :
* Les vrais exos Nathan  NSI
* Prépas sciences NSI Ellipses
* EP 2023
