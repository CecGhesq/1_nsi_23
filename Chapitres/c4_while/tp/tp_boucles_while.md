---
title: "Chapitre 4 : la boucle non bornée while"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---


## Exercice n°1 : Tours de boucles

Pour chacune des situations suivantes, dresser un tableau de suivi des valeurs des variables utilisées : au moment de l'initialisation puis à chaque tour de boucle.  
Déduisez-en ce qu'affichera le programme puis vérifiez-le avec Thonny.

__1)__

```python
i = 0
j = 1
while i < 3:
    i = i + 1
    print(i + j)
```

__2)__

```python
i = 1
j = 1
while i < 3:
    i = i + 1
while j < 1:
    j = j + 1
print(i + j)
```

__3)__
```python
x = 5
while x != 2:
    x = x - 1
print(x)
```

__4)__
```python
x = 12
while x > 12:
    x = x + 1
print(x)
```

## Exercice n°2 : Procédures et boucles  &#x1F3C6;

> 💾 En programmation on appelle __procédure__ une fonction sans valeur de retour.
> 
Par exemple :

```python
def affichage():
    x = 3
    print(x)
```

`affichage` est une procédure (aucun `return` n'est présent dans le bloc de code), cette distinction n'est pas utilisée dans le langage Python mais nous emploierons un vocabulaire précis par la suite.

__1)__ Ecrire une procédure `afficher_carre` qui affiche les carrés des chiffres de 0 à 3.

```python
>>> afficher_carre()
 0
 1
 4
 9
```


__2)__ Ecrire une procédure `afficher_ligne(n)` qui affiche __n__ caractères `|` les uns en dessous des autres

```python
>>> afficher_ligne(3)
 |
 |
 |
```
> 💾 _Note_ : à l'affichage de caractères grâce à la fonction `print`, remarquez que les guillemets n'apparaissent pas.

__3)__ Ecrire une procédure `afficher_triangle(n)` qui affiche un triangle de hauteur __n__ sur le modèle de l'exemple ci-dessous.

```python
>>> afficher_triangle(4)
 o
 oo
 ooo
 oooo
```
__4)__ 🥇 Ecrire une procédure `afficher_diagonale(n)` qui affiche une diagonale de __n__ caractères `.` sur le modèle de l'exemple ci-dessous.

```python
>>> afficher_diagonale(5)
 .
   .
     .
       .
         .
```

## Exercice n°3 : Tables de multiplication  &#x1F3C6;  &#x1F3C6; 

__1)__ Ecrire une procédure `afficher_multiples_2` qui affiche les dix premiers multiples de 2

__2)__ Ecrire une procédure `afficher_table_2` qui affiche la table de multiplication de 2 sur le modèle de l'exemple ci-dessous
```python
>>> afficher_table_2()
 2 x 1 = 2
 2 x 2 = 4
 2 x 3 = 6
 2 x 4 = 8
 2 x 5 = 10
 2 x 6 = 12
 2 x 7 = 14
 2 x 8 = 16
 2 x 9 = 18
 2 x 10 = 20
```

__3)__ Ecrire une procédure `afficher_table(n)` qui affiche la table de multiplication d'un entier n passé en paramètre.

## Exercice n°4 : La boucle tant que : une boucle non bornée  &#x1F3C6; 

Testez le code suivant

```python
x=0
while x < 1 :
    print("Je ne m'arrête jamais")
```

__1)__ Expliquez  le problème rencontré
__2)__ Déduisez-en une vérification systématique à effectuer lors de l'utilisation de boucles tant que


---

Algorithmes

---

### Exercice n°5 :  Algorithme n°1 &#x1F3C6; &#x1F3C6;

On souhaite implémenter en Python l'__algorithme donnant le nombre de bits nécessaires à la représentation en base 2 d'un entier naturel non nul__.

```
Entrée : n : nombre entier
Sortie : nombre de bits nécessaires à la représentation en base 2
1:  bits ← 1
2:  tant que n > 1 faire
3:    n ← n // 2
4:    bits ← bits + 1
4:  fin tant que
5:  renvoyer bits
```
__1)__ En prenant comme entrée n = 25, dresser un tableau de suivi des valeurs des variables utilisées et vérifier la cohérence de la sortie.


__2)__ Déclarer une fonction `nombre_bits(n)` acceptant un nombre entier décimal en paramètre et traduisez l'algorithme précédent en Python pour que la valeur de retour de la fonction soit le résultat donné par cet algorithme.



### Exercice n°6 :  Algorithme n°2 &#x1F3C6; &#x1F3C6; &#x1F3C6;

__1)__ En utilisant le pseudo-code suivant essayer d'implémenter en Python l'algorihme d'__écriture en base b d’un entier naturel n__ .   

* On appelle D la liste des chiffres (le résultat).
* Tant que n>0
    * Faire la division euclidienne de n par b
    * Ajouter le reste à D 
    * Mettre le quotient dans n
* Inverser l'ordre de D

Soit **x** la représentation du nombre en base 10, **r** la représentation du nombre en base **b** , r étant écrit en utilisant n chiffres : c<sub>0</sub>, c<sub>1</sub>...c<sub>n-1</sub>, nous avons :

$`\displaystyle r = c_{n-1} c_{n-2} ......c_{1} c_{0}`$ 

$`x= \sum \limits_{\substack{i=0}}^{n-1}{c_i b^i} =c_{n-1} b^{n-1} + c_{n-2} b^{n-2}+ ..... +c_2 b^2 +c_1 b^1 +c_0 b^0`$ 
Quelques conseils pour vous aider : 

* Vous définirez une fonction nommée `n_to_baseb`
* Cette fonction acceptera deux paramètres
  * b : la base dans laquelle il faut convertir
  * n : le nombre décimal à convertir
* La valeur de retour sera une chaîne de caractères correspondant à la représentation 

La traduction du pseudo-code est assez implicite sauf pour l'étape 5 :
```python
5:     ai ← chiffre correspondant à r
```
__Utilisez une variable de type chaine de caractères pour représenter tous les ai, en concaténant les valeurs de r vous obtiendrez la représentation souhaitée__

Vous pouvez vérifiez votre code :

```python
>>> n_to_baseb(2, 10)
 '1010'
```

__2)__ Quelle est la limite d'utilisation de cette fonction ? (Essayez avec plusieurs bases)
