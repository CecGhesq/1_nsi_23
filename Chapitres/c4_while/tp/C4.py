#######ex 2 #################
def affichage():
    '''affiche le carré des valeurs 0 à 3
    '''
    variable = 0 # on initie une valeur
    while variable < 4:
        print(variable**2)
        variable = variable + 1 # NE PAS OUBLIER 

def afficher_ligne(n):
    '''affiche n lignes avec le caractère |
    '''
#     variable = 0
#     while variable < n:
#         print('|')
#         variable = variable + 1
## V2
    while n!=0:
        print('|')
        n = n -1

def afficher_triangle(n):
    ''' affiche un triangle de hauteur n
    '''
    compteur = 1
    while compteur <= n:
        print(compteur*'o')
        compteur = compteur + 1

def afficher_diagonale(n):
    '''affiche une diagonale de n caractères .
    '''
    compteur = 0
    while compteur < n:
        print(compteur * ' '+'.')
        compteur = compteur + 1
        