# 1_nsi_23

Dépôt de fichiers de __première NSI__ Lycée M de Flandre à Gondecourt  
C. Ghesquiere : cecile.ghesquiere@ac-lille.fr  
lien vers le réseau du lycée : [ici](https://194.167.100.29:8443/owncloud/index.php) 

![partie pratique](./DS/epreuve_commune.md)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)  

__chapitre 22 : Représentations des données en tables__  

* [cours 1](./Chapitres/c22_traitement_donnees_tables/c21_p1_csv.md)  
* [tp1](./Chapitres/c22_traitement_donnees_tables/tp1_tables.md)  
* [cours2](./Chapitres/c22_traitement_donnees_tables/C21_2.md)
* [Activité Aéroports](./Chapitres/c22_traitement_donnees_tables/notebook_aeroport.zip)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif)  

__chapitre 21 : Algorithme KNN__  

* [Cours](./Chapitres/c21_knn/1_Cours/cours_algo_knn.md)  
* [TP](https://framagit.org/CecGhesq/1_nsi_22/-/blob/main/Chapitres/c21_knn/2_Activit%C3%A9s_TP_Jupyter/TP_knn.ipynb)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-010.gif)  

__chapitre 20 : Représentation des caractères__

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-001.gif)  

* [Activité](./Chapitres/c20_representation_caractères/c20_caracteres_ac_intro.md)   
* [Cours](./Chapitres/c20_representation_caractères/2_cours/c20_caracteres_cours.md)  

__chapitre 19 : Algorithme glouton__  
* [Cours](./Chapitres/c19_glouton/cours_algo_glouton.md)  
* [TP](./Chapitres/c19_glouton/TP_haie_fleurie.md)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-002.gif)  

__chapitre 18 : Recherche dichotomique__  
* [Cours](./Chapitres/c18_dichotomie/cours/cours_dichotomie.md)  
* [TP](./Chapitres/c18_dichotomie/tp_dichotomie/tp_dichotomie.md)


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-003.gif)  

__chapitre 17 : Les tris__

[cours à completer](./Chapitres/c17_tris/C17 LES TRIS.ipynb)  
[TP](./Chapitres/c17_tris/TP_tris.md)  


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-004.gif)  



__chapitre 16 : Réseau__  
* [Cours HTTP](./Chapitres/c16_reseau/c16_cours_http.md)  
    * [exercices](./Chapitres/c16_reseau/exercices/c16_exos_client_serveur.md) 
    * [Utilisations de formulaires A FAIRE POUR LE 4 AVRIL](./Chapitres/c16_reseau/C16_1_formulaire.md) 
* [cours TCP](./Chapitres/c16_reseau/c16_2_cours_tcp.md)
    * [tp1 Filius : table ARP](./Chapitres/c16_reseau/tp1_filius/tp1_filius.md)
    * [tp2 : TCP](./Chapitres/c16_reseau/tp2_tcp/tp2_tcp.md)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-005.gif)  

__chapitre 15__ : Interaction Homme machine
* Découverte du javascript : [TP](./Chapitres/c15_ihm/c15_p1_decouvertejs/c15_decouverte_js.md)
* Programmation événementielle : [cours](./Chapitres/c15_ihm/c15_p2_evenementiel/c15_evenementiel.md) et le [TP](./Chapitres/c15_ihm/c15_p2_evenementiel/tp_evenements/C15_2_tp_evenements.md)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-006.gif)

 __chapitre 14 : Pages WEB__  
* niveau [débutant](./Chapitres/c14_html/html_css_niveau_debutant/C14_HTML_CSS_decouverte_2024.md)
* niveau [avancé](.//Chapitres/c14_html/html_css_niveau_avance/C14_html_avance.md)  
--> [notice](./Notices/Notice_Zip.pdf) pour utiliser 7-zip afin de compresser un dossier en un seul fichier zippé


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-007.gif)

 __chapitre 13 : les dictionnaires__  
* Cours : [Généralités](./Chapitres/c13_dictionnaire/C13_cours/c13_dictionnaire.md)  
* Découverte du type dict : télécharger le [notebook](./Chapitres/c13_dictionnaire/exos/decouverte_dico.ipynb) 
* [Compléments](./Chapitres/c13_dictionnaire/C13_cours/C13_dictionnaire_compléments.md) du cours  
* [Exercices](./Chapitres/c13_dictionnaire/exos/C13_exercices.md)  
* Où se repose Spiderman entre deux exploits ? [ici](./Chapitres/c13_dictionnaire/C13_TP_Données EXif_dico.md)

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)  

__chapitre 12 : les tuples__  

* Cours [en ligne](./Chapitres/c12_tuples/C12_tuples.md)  ou  [![](./img/pdf.jpg)](./Chapitres/c12_tuples/C12_tuples.pdf)  
* TP [en ligne](./Chapitres/c12_tuples/c12_tp.md) ou [![](./img/pdf.jpg)](./Chapitres/c12_tuples/C12_TP_tuples.pdf)   

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif)  

__Chapitre 11 : Mise au point des programmes__  

* Cours [en ligne](./Chapitres/c11_mise_au_point/C11_mise_au_point.md)  
* TP [en ligne](./Chapitres/c11_mise_au_point/C11_TP_mise_au_point_programmes.md)   

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-010.gif) 
__Chapitre 10 : Système d'exploitation__ 

* [activité introductive](./Chapitres/c10_systeme_exploitation/ac_intro/C10_os_intro.md) ou [pdf](./Chapitres/c10_systeme_exploitation/ac_intro/C10_os_intro.pdf)  
* cours 10_1 : [Systèmes d'exploitation](./Chapitres/c10_systeme_exploitation/cours/C10_1_os.md)  ou  [![](./img/pdf.jpg)](.Chapitres/c11_systeme_exploitation/cours/C10_1_os.pdf)  
* [TP Shell](./Chapitres/c10_systeme_exploitation/tp_shell/tp_shell.md)  
* [TP Terminus](./Chapitres/c10_systeme_exploitation/tp_shell/TD_Jeu_Terminus.pdf)  A FAIRE EN AUTONOMIE
* Cours 10_2 : [droits des utilisateurs](./Chapitres/c10_systeme_exploitation/cours/C10_2_droits_utilisateurs.md) ou [![](./img/pdf.jpg)](./Chapitres/c10_systeme_exploitation/cours/C10_2_droits_utilisateurs.pdf)  
* TP Droits : [sujet en ligne](./Chapitres/c10_systeme_exploitation/tp_droits/C11_2_TP_droits.md) avec le fichier à télécharger [ici](./Chapitres/c10_systeme_exploitation/tp_droits/frozen-bubble-2_2_0.zip)  


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-001.gif)  

__Chapitre 9 : les tableaux : type list en PYTHON__ 

* Le type list : cours 1 [en ligne](./Chapitres/c9_listes/C9 cours1/cours/C9_1_liste_tab.md) 
* TP_1 : [en ligne](./Chapitres/c9_listes/C9 cours1/TP/C9_1_TP_liste.md)    
* travail en autonomie sur [e-nsi](https://e-nsi.forge.aeif.fr/pratique/A/10-tab_recherche/)  
* Listes en compréhension ; listes 2 D [en ligne](./Chapitres/c9_listes/C9 cours2 listes en Python/cours/C9_2_listes_en_comprehension.md)  ou [pdf](./Chapitres/c9_listes/C9 cours2 listes en Python/cours/C9_2_listes_en_comprehension.pdf)  
* TP_2 : [en ligne](./Chapitres/c9_listes/C9 cours2 listes en Python/TP/C9_2_TP_liste_en_comp.md)  ou [pdf](./Chapitres/c9_listes/C9 cours2 listes en Python/TP/C9_2_TP_liste_en_comp.pdf)  
    
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-002.gif)  

__Chapitre 8 : Parcours séquentiel dans une chaîne__  
    * cours : cours [en ligne](./Chapitres/c8_parcours_sequentiel/c8_for_string.md) ou [![](./img/pdf.jpg)](Chapitres/c8_parcours_sequentiel/c8_for_string.pdf)  
    * TP [en ligne](./Chapitres/c8_parcours_sequentiel/tp_parcours_chaines.md) ou [![](./img/pdf.jpg)](Chapitres/c8_parcours_sequentiel/tp_parcours_chaines.pdf)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-003.gif)  

__Chapitre 7 : Architecture matérielle__  
* Architecture d'un ordinateur : cours [en ligne](./Chapitres/c7_architecture_materielle/C7_1/C7_1_Archi_ordi.md) ou [![](./img/pdf.jpg)](./Chapitres/c7_architecture_materielle/C7_1/C7_1.pdf)  

    ![](./img/camera003.gif) [video du site lumni](https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs)

* Algèbre de Boole et circuits combinatoires: cours [en ligne](./Chapitres/c7_architecture_materielle/C7_2/C7_2.md) ou [![](./img/pdf.jpg)](./Chapitres/c7_architecture_materielle/C7_2/C7_2.pdf)  

* TP1 logique [en ligne](./Chapitres/c7_architecture_materielle/C7_2/TP_Portes_logiques_CircuitVerse_eleves.md) ou [![](./img/pdf.jpg)](./Chapitres/c7_architecture_materielle/C7_2/TP1_CircuitVerse.pdf)  [CORRECTION](./Chapitres/c7_architecture_materielle/C7_2/circuit_verse.pdf)

* TP2 circuits combinatoires [en ligne](./Chapitres/c7_architecture_materielle/C7_2/TP2_circuits_combinatoires_eleves.md) ou [![](./img/pdf.jpg)](./Chapitres/c7_architecture_materielle/C7_2/TP2.pdf)    [CORRECTION](./Chapitres/c7_architecture_materielle/C7_2/tp2.pdf)
* Exercices d'application : [en ligne](./Chapitres/c7_architecture_materielle/C7_2/Exercices_C7_1_2.md)  

* Le langage machine : cours [en ligne](./Chapitres/c7_architecture_materielle/C7_3/7_3_cours.md)  

* TP3 utilisation de la M999: télécharger le [dossier zippé](./Chapitres/c7_architecture_materielle/C7_3/M999.zip) de la machine puis dézipper-le. Sujet d'activité [en ligne](./Chapitres/c7_architecture_materielle/C7_3/C7_3_TP_langage_assembleur.md) --> Correction [multiplication](./Chapitres/c7_architecture_materielle/C7_3/M999.png)   

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-004.gif)  

__Chapitre 6 : la boucle bornée FOR__  
    * cours : cours [en ligne](./Chapitres/c6_boucle_bornee/C6_cours.md)   
    * TP [en ligne](./Chapitres/c6_boucle_bornee/tp_for/tp_for.md) ou [![](./img/pdf.jpg)](./Chapitres/c6_boucle_bornee/tp_for/tp_for.pdf)  
    
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-005.gif)  

__Chapitre 5 : Les conditions, booléens __
* cours [en ligne](./Chapitres/c5_booléen_conditions/cours_booleens_conditions.md) 
* TP [en ligne](./Chapitres/c5_booléen_conditions/tp_booleens_conditions/tp_booleens_conditions.md)


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-006.gif)  

__Chapitre 4 : La boucle non bornée TANT QUE__  
* cours : cours [en ligne](https://framagit.org/CecGhesq/1_nsi_23/-/blob/main/Chapitres/c4_while/c4_while_algo_2020_git.md) ou [![](./img/pdf.jpg)](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c4_while/C4_while_algo_2020.pdf)  
* TP [en ligne](./Chapitres/c4_while/tp/tp_boucles_while.md) ou [![](./img/pdf.jpg)](./Chapitres/c4_while/tp/tp_boucles_while_v1.pdf)  

---

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-007.gif)  
__Chapitre 3 : LES ENTIERS RELATIFS__  
 * cours :  [en ligne](./Chapitres/c3_entiers_relatifs/C3_complementadeux_git.md)
 * TP [en ligne](./Chapitres/c3_entiers_relatifs/C3_exercices_complement_a_deux.md)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)  

__Chapitre 2 : LES ENTIERS__

* cours [ en ligne](./Chapitres/c2_representation_entiers/C2_les_bases.md) ou [![](./img/pdf.jpg)](./Chapitres/c2_representation_entiers/C2_les_bases.pdf)
* TP [en ligne](./Chapitres/c2_representation_entiers/TP/tp1_bases.md) ou [![](./img/pdf.jpg)](./Chapitres/c2_representation_entiers/TP/tp1_bases.pdf)



---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif)  

__Chapitre 1 : DECOUVERTE DU PYTHON__  
* C1_1 Manipuler des nombres
    *  [ cours](./Chapitres/c1_decouverte_python/manipuler_nombres/C1_1.md) 
    *  [TP](./Chapitres/c1_decouverte_python/manipuler_nombres/tp1_nombres.md) 


* C1_2 Les fonctions :
    *   [cours](./Chapitres/c1_decouverte_python/python2_fonctions/cours2_fonctions.md) 
    *   [TP](./Chapitres/c1_decouverte_python/python2_fonctions/tp2_fonctions/tp2_fonctions.md)  

* C1_3 Le type chaine :
    * [cours](./Chapitres/c1_decouverte_python/python3_strings/cours3_strings.md) 
    * [TP](./Chapitres/c1_decouverte_python/python3_strings/tp3_strings/tp3_strings.md) et la [correction](https://framagit.org/CecGhesq/1_nsi_23/-/blob/main/Chapitres/c1_decouverte_python/python3_strings/C1.3.py)
---




![](./img/orientation.jpg)  
[Lien](https://padlet.com/cgenial/l-ves-mon-orientation-x6z1sir05s8k775w) vers les ressources orientation de la fondation CGENIAL.  

## DS  du 5 février
Cliquer sur votre nom afin de télécharger le fichier à compléter.
||||-||||
|---|---|---|---|---|---|---|
|[Ayleen](./DS/DS6/DS6_OS_A.odt)| [Léa](./DS/DS6/DS6_OS_B.odt)|[Vianney](./DS/DS6/DS6_OS_A.odt)|--|[Jules](./DS/DS6/DS6_OS_B.odt)|[Paul B](./DS/DS6/DS6_OS_A.odt)|[Aymeric](./DS/DS6/DS6_OS_B.odt)|
|[Hugo L](./DS/DS6/DS6_OS_B.odt)| [Hugo H](./DS/DS6/DS6_OS_A.odt)|[Louis](./DS/DS6/DS6_OS_B.odt)|--|[Lilian M](./DS/DS6/DS6_OS_A.odt)|[Timothée](./DS/DS6/DS6_OS_B.odt)|[Noah](./DS/DS6/DS6_OS_A.odt)|
|[Vincent](./DS/DS6/DS6_OS_B.odt)| [Lilian T](./DS/DS6/DS6_OS_A.odt)|[Paul G](./DS/DS6/DS6_OS_B.odt)|--|[Adam](./DS/DS6/DS6_OS_A.odt)|[Alexandre](./DS/DS6/DS6_OS_B.odt)|[Hugo S](./DS/DS6/DS6_OS_A.odt)|
|--|--|--|--|--||[Teeyah](./DS/DS6/DS6_OS_B.odt)|

## DS  du 18 mars
Cliquer sur votre nom afin de télécharger le fichier à compléter.
||||-||||
|---|---|---|---|---|---|---|
|[Ayleen](./DS/DS7/DS7_A_eleve.ipynb)| [Léa](./DS/DS7/DS7_B_eleve.ipynb)|[Vianney](./DS/DS7/DS7_A_eleve.ipynb)|--|[Jules](./DS/DS7/DS7_B_eleve.ipynb)|[Paul B](./DS/DS7/DS7_A_eleve.ipynb)|[Aymeric](./DS/DS7/DS7_B_eleve.ipynb)|
|[Hugo L](./DS/DS7/DS7_B_eleve.ipynb)| [Hugo H](./DS/DS7/DS7_A_eleve.ipynb)|[Louis](./DS/DS7/DS7_B_eleve.ipynb)|--|[Lilian M](./DS/DS7/DS7_A_eleve.ipynb)|[Timothée](./DS/DS7/DS7_B_eleve.ipynb)|[Noah](./DS/DS7/DS7_A_eleve.ipynb)|
|[Vincent](./DS/DS7/DS7_B_eleve.ipynb)| [Lilian T](./DS/DS7/DS7_A_eleve.ipynb)|[Paul G](./DS/DS7/DS7_B_eleve.ipynb)|--|[Adam](./DS/DS7/DS7_A_eleve.ipynb)|[Alexandre](./DS/DS7/DS7_B_eleve.ipynb)|[Hugo S](./DS/DS7/DS7_A_eleve.ipynb)|
|--|--|--|--|--||[Teeyah](./DS/DS7/DS7_B_eleve.ipynb)|

## DS  du 8 avril
Cliquer sur votre nom afin de télécharger le fichier à compléter.
||||-||||
|---|---|---|---|---|---|---|
|[Ayleen](./DS/DS8/DS8.md)| [Léa](./DS/DS8/DS8_B.md)|[Vianney](./DS/DS8/DS8.md)|--|[Jules](./DS/DS8/DS8_B.md)|[Paul B](./DS/DS8/DS8.md)|[Aymeric](./DS/DS8/DS8_B.md)|
|[Hugo L](./DS/DS8/DS8.md)| [Hugo H](./DS/DS8/DS8_B.md)|[Louis](./DS/DS8/DS8.md)|--|[Lilian M](./DS/DS8/DS8_B.md)|[Timothée](./DS/DS8/DS8.md)|[Noah](./DS/DS8/DS8_B.md)|
|[Vincent](./DS/DS8/DS8.md)| [Lilian T](./DS/DS8/DS8_B.md)|[Paul G](./DS/DS8/DS8.md)|--|[Adam](./DS/DS8/DS8_B.md)|[Alexandre](./DS/DS8/DS8.md)|[Hugo S](./DS/DS8/DS8_B.md)|
|--|--|--|--|--||[Teeyah](./DS/DS8/DS8_B.md)|

![](./img/53630_r.gif)Projet à rendre le 23/11 : [consignes ici](./Projets/projet_turtle.md)  

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Les  documents sont mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.
